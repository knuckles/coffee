This directory holds game assets: models, textures, etc.

Due to usage of GtkRadiant map editor, the project inherits Quake unit system 
which is scaled so that one meter is about 0.03 units (at least they say so).
This means that all objects must be scaled accordingly.
On the other hand we have .bvh files, which (it seems) are measured in inches.
To make skeleton animation more convinient we use .bvh units and scale all
.blend files to 0.025 while using metric system during modelling.
This results in good bones fitting and adequate world scale together with
Radiant maps.


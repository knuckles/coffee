#ifndef CREATE_TTF_H
#define CREATE_TTF_H

#include "../irrlicht/include/path.h"

namespace irr {

class ILogger;

namespace gui {
class IGUIFont;
}

namespace io {
class IFileSystem;
}

namespace video {
class IVideoDriver;
}

}

irr::gui::IGUIFont* createTTFont(irr::video::IVideoDriver* driver,
                                 irr::io::IFileSystem* fileSystem,
                                 const irr::io::path& filename,
                                 irr::u32 size,
                                 bool antialias = true,
                                 bool transparency = true,
                                 const wchar_t *invisibleChars = L" ",
                                 irr::ILogger* logger = 0);

#endif  // CREATE_TTF_H

#include "create_ttf.h"
#include "CGUITTFont.h"

irr::gui::IGUIFont* createTTFont(irr::video::IVideoDriver* driver,
                                 irr::io::IFileSystem* fileSystem,
                                 const irr::io::path& filename,
                                 irr::u32 size,
                                 bool antialias,
                                 bool transparency,
                                 const wchar_t *invisibleChars,
                                 irr::ILogger* logger) {
  return irr::gui::CGUITTFont::createTTFont(driver, fileSystem, filename, size, antialias,
                                            transparency, invisibleChars, logger);
}

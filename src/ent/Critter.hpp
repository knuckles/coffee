#ifndef CRITTER_HPP
#define CRITTER_HPP

#include <memory>
#include <string>
#include <vector>

#include <boost/signals2/signal.hpp>

#include "base/macros.hpp"
#include "ent/Object.hpp"

#include "3rdparty/irrlicht/include/ISceneNodeAnimator.h"
#include "3rdparty/irrlicht/include/vector3d.h"

class Inventory;
class CritterAI;
class CritterDescription;
class Item;
class PathNodeAnimator;
class Planner;
class PointsSceneNode;
class Weapon;

namespace irr {
namespace scene {
class IMeshSceneNode;
}
}

class Critter :
    public Object,
    protected irr::scene::ISceneNodeAnimator::IListener {
public:
  // Critter damaged signal. First argument is the critter damaged. Second - the source of damage.
  // NOTE: Second argument MAY BE NULLPTR.
  typedef boost::signals2::signal<void(Critter*, Critter*)> DamagedSignal;

  Critter(std::shared_ptr<CritterDescription> desc, irr::scene::IAnimatedMesh* mesh);
  ~Critter();

  const CritterDescription& critterDescription() const;
  int hp() const { return hp_; }
  bool dead() const { return hp_ <= 0; }
  const Weapon* weapon() const;
  Inventory* inventory() const { return inventory_.get(); }
  Planner* planner() const { return planner_.get(); }
  CritterAI* ai() const { return ai_.get(); }
  // Means: can pick or melee attack.
  // |point| must be in the same coordinate system (relative to parent node).
  bool isNear(const irr::core::vector3df& point) const;

  bool takeItem(std::shared_ptr<Item> item);
  void turnTo(const irr::core::vector3df& point);
  bool moveTo(const irr::core::vector3df& destination, Callback callback);
  void stopMoving();
  void takeDamage(int damage, Critter* attacker);
  void heal(int hp);
  void die();
  void wield(std::shared_ptr<Weapon> weapon);
  void dropWeapon();
  void setAI(std::unique_ptr<CritterAI> ai);

  boost::signals2::connection connectToDamaged(DamagedSignal::slot_type slot);

protected:
  // Entity.
  virtual void afterLocationChange() override;

  // irr::scene::ISceneNodeAnimator::IListener implementation.
  virtual void onFinished(irr::scene::ISceneNodeAnimator* animator) override;

private:
  void notifyStoppedMoving(bool reachedDestination);

  std::shared_ptr<CritterDescription> desc_;
  std::unique_ptr<Inventory> inventory_;
  int maxHP_;
  int hp_;
  std::unique_ptr<Planner> planner_;
  std::shared_ptr<Weapon> weapon_;
  Callback moveCallback_;
  std::unique_ptr<CritterAI> ai_;
  DamagedSignal damagedSignal_;

  // These members are only valid after entity is added to a Location (has own scene node).
  std::vector<irr::scene::IBoneSceneNode*> weaponMountNodes_;
  boost::intrusive_ptr<PathNodeAnimator> pathAnimator_;
  ScopedSceneNode<irr::scene::IMeshSceneNode> meleeSphere_;
  ScopedSceneNode<PointsSceneNode> path_;

  DISALLOW_COPY_AND_ASSIGN(Critter);
};

#endif  // CRITTER_HPP

#ifndef ITEM_DESCRIPTIONS_HPP
#define ITEM_DESCRIPTIONS_HPP

#include <map>
#include <string>
#include <vector>
#include <3rdparty/irrlicht/include/vector3d.h>

/**
 * These structs hold common item info, shared between all instances of the same type of item.
 * Inheritance structure should copy those of Entity hierarchy.
 */

// Maps action name to animation frame bounds.
typedef std::pair<int, int> FrameBounds;
typedef std::map<std::string, FrameBounds> FrameMap;

struct ObjectDescription {
  ObjectDescription(
      const std::wstring& name,
      const std::wstring& description,
      const std::string& modelPath,
      const FrameMap&& frameMap);

  const std::wstring name;
  const std::wstring description;
  const std::string modelPath;
  const FrameMap frameMap;
};

struct ItemDescription : public ObjectDescription {
  ItemDescription(
      const std::wstring& name,
      const std::wstring& description,
      const std::string& modelPath,
      const irr::core::vector3df& stablePosition,
      float weight);

  // Applied to item's scene node when it's lying on the ground.
  const irr::core::vector3df stablePosition;
  const float weight;  // in grams.
};

struct WeaponDescription : public ItemDescription {
  WeaponDescription(
      const std::wstring& name,
      const std::wstring& description,
      const std::string& modelPath,
      const irr::core::vector3df& stablePosition,
      const std::string& weaponClass,
      const std::string& ammo,
      int damage,
      float weight);

  const std::string weaponClass;  // knife, handgun, etc. Used to find animations of its usage.
  const std::string ammo;  // just the ItemDescription::name of the appropriate ammo item.
  const int damage;
};

struct CritterDescription : public ObjectDescription {
  CritterDescription(
      const std::wstring& name,
      const std::wstring& description,
      const std::string& modelPath,
      const FrameMap&& frameMap,
      const std::string& rigName,
      const std::vector<std::string>& weaponMountJoints,
      float occupyRadius,
      float meleeDistance,
      float walkSpeed,
      float runSpeed);

  const std::string rigName;
  const std::vector<std::string> weaponMountJoints;
  const float occupyRadius;
  const float meleeDistance;
  const float walkSpeed;
  const float runSpeed;
};

#endif  // ITEM_DESCRIPTIONS_HPP

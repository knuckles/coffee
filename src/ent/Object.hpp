#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <functional>
#include <string>
#include <vector>

#include "ent/Entity.hpp"

#include "3rdparty/irrlicht/include/IAnimatedMeshSceneNode.h"

class ObjectDescription;

namespace irr {

namespace scene {
class IAnimatedMesh;
}
}

/**
 * @brief The Object has a (animated) mesh and can perform "actions" (e.g. move).
 */
class Object : public Entity,
               public irr::scene::IAnimationEndCallBack {
public:
  typedef std::function<void(bool)> Callback;
  typedef std::function<void(int)> StepCallback;
  // Name of rest animation name in ObjectDescription::FrameMap
  static const std::string& kRestPoseName;

  Object(std::shared_ptr<ObjectDescription> desc,
         irr::scene::IAnimatedMesh* mesh);
  ~Object();

  const ObjectDescription& objectDescription() const;
  irr::scene::IAnimatedMesh* mesh() const;
  irr::scene::IAnimatedMeshSceneNode* animatedNode() const;

  float getAnimationDuration(const std::string& actionName) const;

  const std::string& currentAnimation() const { return currentAnimation_; }

  // Play named animation. Looped or not.
  // |callback| will be called after animation has finished. The only callback bool argument will be
  // true, iff animation was started looped and was fully played.
  // Returns, whether animation was started or not.
  // Note: previously set callback is called and replaced with new one, even if |actionName| equals
  // to currently played one.
  bool playAnimation(const std::string& actionName, bool loop, Callback callback);
  bool playAnimation(const std::string& actionName, bool loop);
  // Stops current animation, if any. The callback is called even if animation was started
  // non-looped. |false| is passed as an argument.
  void stopAnimation();
  // Plays a series of non-looped animations and calls back, when done.
  // An optional callback may be provided to get notifications after each animation. It gets the
  // index of animation, that finished playing. It is called before the final callback.
  typedef std::vector<std::string> AnimationChain;
  void chainAnimations(AnimationChain&& actionNames,
                       Callback callback, StepCallback stepCallback = StepCallback());

protected:
  virtual irr::scene::ISceneNode* createNode(irr::scene::ISceneManager* scene) override;

private:
  // IAnimationEndCallBack.
  virtual void OnAnimationEnd(irr::scene::IAnimatedMeshSceneNode* node) override;

  bool playingAnimation() const;
  void nextAnimationLink(int linkIndex, Callback stepCallback, Callback finalCallback,
                         bool lastLinkSucceeded);
  void fireCallbackAndClear(Callback& callback, bool fullyPlayed);

  std::shared_ptr<ObjectDescription> desc_;
  boost::intrusive_ptr<irr::scene::IAnimatedMesh> mesh_;

  AnimationChain animationChain_;
  std::string currentAnimation_;
  Callback animationCallback_;
};

#endif  // OBJECT_HPP

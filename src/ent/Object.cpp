#include "Object.hpp"

#include <assert.h>

#include "ent/entity_descriptions.hpp"

#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/IAnimatedMesh.h"

using namespace irr;
using namespace irr::core;
using namespace irr::scene;

namespace {
const u32 kGreenColor = 0x0000FF00;

const FrameBounds* const findActionBounds(const FrameMap& frameMap, const std::string& actionName) {
  const auto& animLoop(frameMap.find(actionName));
  if (animLoop == frameMap.end())
    return nullptr;
  return &animLoop->second;
}

}  // namespace

const std::string& Object::kRestPoseName = std::string();

Object::Object(std::shared_ptr<ObjectDescription> desc, IAnimatedMesh* mesh)
  : Entity(),
    desc_(desc),
    mesh_(mesh) {
  setEntityKind(kObjectEntity);
  setStubColor(video::SColor(kGreenColor));
}

Object::~Object() = default;

IAnimatedMesh* Object::mesh() const {
  return mesh_.get();
}

const ObjectDescription& Object::objectDescription() const {
  return *desc_.get();
}

IAnimatedMeshSceneNode* Object::animatedNode() const {
  assert(!node() || node()->getType() == ESNT_ANIMATED_MESH);
  return static_cast<IAnimatedMeshSceneNode*>(node());
}

float Object::getAnimationDuration(const std::string& actionName) const {
  const FrameBounds* const bounds = findActionBounds(desc_->frameMap, actionName);
  if (!bounds)
    return 0;
  const int frames = bounds->second - bounds->first;
  return mesh()->getAnimationSpeed() * frames;
}

ISceneNode* Object::createNode(ISceneManager* scene) {
  if (!mesh())
    return Entity::createNode(scene);
  IAnimatedMeshSceneNode* node = scene->addAnimatedMeshSceneNode(mesh());
  auto selector = scene->createTriangleSelector(node);
  node->setTriangleSelector(selector);
  selector->drop();
  node->setAnimationEndCallback(this);
  return node;
}

bool Object::playAnimation(const std::string& actionName, bool loop, Callback callback) {
  if (playingAnimation())
    fireCallbackAndClear(animationCallback_, false);
  const FrameBounds* const frames = findActionBounds(desc_->frameMap, actionName);
  if (!frames)
    return false;
  IAnimatedMeshSceneNode* const animNode = animatedNode();
  animNode->setFrameLoop(frames->first, frames->second);
  animNode->setLoopMode(loop);
  currentAnimation_ = actionName;
  animationCallback_ = callback;
  return true;
}

bool Object::playAnimation(const std::string& actionName, bool loop) {
  return playAnimation(actionName, loop, Callback());
}

void Object::stopAnimation() {
  playAnimation(kRestPoseName, false);
}

void Object::chainAnimations(std::vector<std::string>&& actionNames,
                             Object::Callback callback, Object::StepCallback stepCallback) {
  stopAnimation();
  std::swap(animationChain_, actionNames);
  nextAnimationLink(0, stepCallback, callback, true);
}

void Object::OnAnimationEnd(IAnimatedMeshSceneNode* n) {
  if (n != node())
    return;
  currentAnimation_ = kRestPoseName;
  fireCallbackAndClear(animationCallback_, true);
}

bool Object::playingAnimation() const {
  const IAnimatedMeshSceneNode* const animNode = animatedNode();
  return animNode->getLoopMode() || animNode->getFrameNr() < animNode->getEndFrame();
}

void Object::fireCallbackAndClear(Callback& callback, bool fullyPlayed) {
  if (!callback)
    return;
  Callback tempCallback;
  std::swap(tempCallback, callback);
  tempCallback(fullyPlayed);
}

void Object::nextAnimationLink(int linkIndex, Callback stepCallback, Callback finalCallback,
                               bool lastLinkSucceeded) {
  if (!lastLinkSucceeded || linkIndex == animationChain_.size()) {
    fireCallbackAndClear(finalCallback, lastLinkSucceeded);
    return;
  }

  playAnimation(animationChain_[linkIndex], false,
      [this, linkIndex, stepCallback, finalCallback](bool success) {
        if (stepCallback)
          stepCallback(linkIndex);
        nextAnimationLink(linkIndex + 1, stepCallback, finalCallback, success);
  });
}

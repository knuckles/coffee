#include "Item.hpp"

#include "ent/entity_descriptions.hpp"

#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/IMeshSceneNode.h"
#include "3rdparty/irrlicht/include/SColor.h"

using namespace irr;
using namespace irr::core;
using namespace irr::scene;

Item::Item(std::shared_ptr<ItemDescription> desc, IAnimatedMesh* mesh)
  : Object(desc, mesh),
    desc_(desc),
    // FIXME: this is never changed for non-weapon items!
    kind_(kUnknownItem) {
  setEntityKind(kItemEntity);
  setStubColor(irr::video::SColor(0, 0, 255, 0));
}

Item::~Item() = default;

ISceneNode* Item::createNode(ISceneManager* scene) {
  ISceneNode* objectNode = Object::createNode(scene);
  objectNode->setRotation(desc_->stablePosition);
  if (!mesh())
    return objectNode;

  // Create an invisible sphere around the item to simplify picking.
  IMeshSceneNode* pickNode =
      scene->addSphereSceneNode(mesh()->getBoundingBox().getExtent().getLength());
  auto selector = scene->createTriangleSelector(pickNode->getMesh(), pickNode);
  pickNode->setTriangleSelector(selector);
  selector->drop();
  video::SMaterial& material = pickNode->getMaterial(0);
  material.MaterialType = video::EMT_TRANSPARENT_ALPHA_CHANNEL;
  material.Lighting = false;
  material.ColorMask = video::ECP_NONE;
  pickNode->addChild(objectNode);
  return pickNode;
}

const ItemDescription& Item::itemDescription() const {
  return *desc_.get();
}

void Item::setQuantity(int newQuantity) {
  if (newQuantity < 1)
    throw std::range_error("Item quantity must be greater than 1");
  qty_ = newQuantity;
}

float Item::weight() const {
  return desc_->weight * quantity();
}

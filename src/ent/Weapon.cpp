#include "Weapon.hpp"

#include "ent/entity_descriptions.hpp"

Weapon::Weapon(std::shared_ptr<WeaponDescription> desc, irr::scene::IAnimatedMesh* mesh)
  : Item(desc, mesh),
    desc_(desc) {
  setItemKind(kWeaponItem);
  setStubColor(irr::video::SColor(0, 255, 255, 0));
}

Weapon::~Weapon() = default;

const WeaponDescription& Weapon::weaponDescription() const {
  return *desc_.get();
}

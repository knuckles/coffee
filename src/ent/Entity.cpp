#include "Entity.hpp"

#include <assert.h>

#include "map/Location.hpp"
#include "scene/NodeKinds.hpp"

#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/IMeshSceneNode.h"

using namespace irr;
using namespace irr::core;
using namespace irr::scene;

namespace {
const u32 kWhiteColor = 0x00FFFFFF;
}

Entity::Entity()
  : kind_(kAbstractEntity),
    stubColor_(kWhiteColor),
    location_(nullptr) {
}

Entity::~Entity() = default;

void Entity::addToLocation(Location* location, const vector3df& position) {
  assert(location);
  if (location_ == location)
    return;
  beforeLocationChange();

  node_.reset(createNode(location->scene()));
  node()->setID(static_cast<irr::u32>(kEntityNode) | static_cast<irr::u32>(entityKind()));
  location->structure()->addChild(node());
  node()->setPosition(position);

  location_ = location;

  afterLocationChange();
}

void Entity::removeFromLocation() {
  node_.reset();
  location_ = nullptr;
  afterLocationChange();
}

bool Entity::onLocation() const {
  return location() && node();
}

void Entity::setStubColor(irr::video::SColor color) {
  stubColor_ = color;
}

ISceneNode* Entity::createNode(ISceneManager* scene) {
  IMeshSceneNode* cubeNode = scene->addCubeSceneNode(10.0f);
  cubeNode->getMaterial(0).EmissiveColor = stubColor_;
  auto selector = scene->createTriangleSelector(cubeNode->getMesh(), cubeNode);
  cubeNode->setTriangleSelector(selector);
  selector->drop();
  return cubeNode;
}

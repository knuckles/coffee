#ifndef ITEM_HPP
#define ITEM_HPP

#include <memory>

#include "base/macros.hpp"
#include "Object.hpp"

class ItemDescription;

/**
 * @brief The Item can be picked up and put to Critter's inventory or used somehow.
 */
class Item : public Object {
public:
  enum Kind {
    kUnknownItem,
    kWeaponItem,
    kAmmoItem,
    kArmorItem,
    kMedicalItem,
    kBookItem,
    kKeyItem,
  };
  static_assert(kUnknownItem == 0, "Unknown item must be the default value");

  Item(std::shared_ptr<ItemDescription> desc, irr::scene::IAnimatedMesh* mesh);
  ~Item();

  const ItemDescription& itemDescription() const;
  Kind itemKind() const { return kind_; }
  int quantity() const { return qty_; }
  void setQuantity(int newQuantity);
  float weight() const;

protected:
  void setItemKind(Kind kind) { kind_ = kind; }

  // Entity
  virtual irr::scene::ISceneNode* createNode(irr::scene::ISceneManager* scene) override;

private:
  std::shared_ptr<ItemDescription> desc_;
  Kind kind_;
  int qty_;

  DISALLOW_COPY_AND_ASSIGN(Item);
};

#endif  // ITEM_HPP

#include "entity_descriptions.hpp"

#include <assert.h>

ObjectDescription::ObjectDescription(
    const std::wstring& name,
    const std::wstring& description,
    const std::string& modelPath,
    const FrameMap&& frameMap)
  : name(name),
    description(description),
    modelPath(modelPath),
    frameMap(frameMap) {
}

ItemDescription::ItemDescription(
    const std::wstring& name,
    const std::wstring& description,
    const std::string& modelPath,
    const irr::core::vector3df& stablePosition,
    float weight)
  : ObjectDescription {name, description, modelPath, FrameMap{}},
    stablePosition(stablePosition),
    weight(weight) {
  assert(weight >= 0.0f);
}

WeaponDescription::WeaponDescription(
    const std::wstring& name,
    const std::wstring& description,
    const std::string& modelPath,
    const irr::core::vector3df& stablePosition,
    const std::string& weaponClass,
    const std::string& ammo,
    int damage,
    float weight)
  : ItemDescription {name, description, modelPath, stablePosition, weight},
    weaponClass(weaponClass),
    ammo(ammo),
    damage(damage) {
}

CritterDescription::CritterDescription(
    const std::wstring& name,
    const std::wstring& description,
    const std::string& modelPath,
    const FrameMap&& frameMap,
    const std::string& rigName,
    const std::vector<std::string>& weaponMountJoints,
    float occupyRadius,
    float meleeDistance,
    float walkSpeed,
    float runSpeed)
  : ObjectDescription(name, description, modelPath, std::move(frameMap)),
    rigName(rigName),
    weaponMountJoints(weaponMountJoints),
    occupyRadius(occupyRadius),
    meleeDistance(meleeDistance),
    walkSpeed(walkSpeed),
    runSpeed(runSpeed) {
}

#ifndef ENTITYFACTORY_HPP
#define ENTITYFACTORY_HPP

#include <codecvt>
#include <locale>
#include <map>
#include <memory>

#include "base/macros.hpp"
#include "ent/Item.hpp"

#include "3rdparty/irrlicht/include/path.h"

class BSPEntity;
class Critter;
class CritterDescription;
class Item;
class ItemDescription;
class Weapon;
class WeaponDescription;

namespace irr {

namespace scene {
class ISceneManager;
}
}

namespace Json {
class Value;
}

class EntityFactory {
public:
  explicit EntityFactory(irr::scene::ISceneManager* scene);
  ~EntityFactory();

  std::shared_ptr<Entity> createEntity(const BSPEntity& bspEntity);
  std::shared_ptr<Weapon> createWeapon(const std::string& type);
  std::shared_ptr<Item> createAmmo(const std::string& type);
  std::shared_ptr<Critter> createCritter(const BSPEntity& bspEntity, bool npc = true);
  std::shared_ptr<Critter> createCritter(const std::string& name, bool npc = true);

private:
  std::shared_ptr<Item> createItem(const std::string& type, Item::Kind itemKind);
  Item::Kind findItemKind(const std::string& type) const;

  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> toWide_;
  std::unique_ptr<Json::Value> itemDefs_;
  Json::Value* weaponDefs_;
  Json::Value* ammoDefs_;
  std::map<std::string, std::shared_ptr<CritterDescription>> crittersCache_;
  std::map<std::string, std::shared_ptr<WeaponDescription>> weaponsCache_;
  std::map<std::string, std::shared_ptr<ItemDescription>> ammoCache_;

  irr::scene::ISceneManager* scene_;

  DISALLOW_COPY_AND_ASSIGN(EntityFactory);
};

#endif // ENTITYFACTORY_HPP

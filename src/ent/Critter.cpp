#include "Critter.hpp"

#include <assert.h>

#include "base/constants.h"
#include "ai/actions.hpp"
#include "ai/CritterAI.hpp"
#include "ai/Planner.hpp"
#include "ent/entity_descriptions.hpp"
#include "ent/Weapon.hpp"
#include "game/Inventory.hpp"
#include "map/Location.hpp"
#include "nav/Navigator.hpp"
#include "scene/PathNodeAnimator.hpp"
#include "scene/PointsSceneNode.hpp"

#include "3rdparty/irrlicht/include/IAnimatedMeshSceneNode.h"
#include "3rdparty/irrlicht/include/IBoneSceneNode.h"
#include "3rdparty/irrlicht/include/IMeshSceneNode.h"
#include "3rdparty/irrlicht/include/ISceneManager.h"

using namespace irr;
using namespace irr::core;
using namespace irr::scene;

namespace {
const bool kWieldOnPick = true;
const char kAnimationWalk[] = "walk";
}

Critter::Critter(std::shared_ptr<CritterDescription> desc, irr::scene::IAnimatedMesh* mesh)
  : Object(desc, mesh),
    desc_(desc),
    inventory_(new Inventory()),
    maxHP_(100),
    hp_(100),
    planner_(new Planner(this)),
    pathAnimator_(new PathNodeAnimator(), false) {
  setEntityKind(kCritterEntity);
  setStubColor(video::SColor(0, 255, 0, 0));
  pathAnimator_->setSpeed(desc_->walkSpeed);
  pathAnimator_->setListener(this);
}

Critter::~Critter() = default;

const CritterDescription& Critter::critterDescription() const {
  return *desc_;
}

const Weapon* Critter::weapon() const {
  return weapon_.get();
}

bool Critter::isNear(const vector3df& point) const {
  return node()->getPosition().getDistanceFrom(point) <= desc_->meleeDistance * kUnitsPerMeter;
}

bool Critter::takeItem(std::shared_ptr<Item> item) {
  if (kWieldOnPick) {
    std::shared_ptr<Weapon> weapon(
        item->itemKind() == Item::kWeaponItem ? std::static_pointer_cast<Weapon>(item)
                                              : nullptr);
    if (weapon) {
      wield(weapon);
      return true;
    }
  }

  inventory_->add(item);
  return true;
}

void Critter::turnTo(const vector3df& point) {
  assert(onLocation());
  node()->setRotation((node()->getPosition() - point).getHorizontalAngle());
}

bool Critter::moveTo(const vector3df& destination, Callback callback) {
  assert(onLocation());
  Navigator::Path route =
      location()->navigator()->route(node()->getPosition(), destination);
  if (!route.size())
    return false;
#if !defined(NDEBUG)
  core::array<core::vector3df> points;
  for (int i = 0; i < route.size(); ++i)
    points.push_back(route[i]);
  if (path_)
    path_->setPoints(points);
#endif
  pathAnimator_->setPath(std::move(route));
  moveCallback_ = callback;
  playAnimation(kAnimationWalk, true);
  return true;
}

void Critter::stopMoving() {
  pathAnimator_->setPath(Navigator::Path());
  stopAnimation();
  notifyStoppedMoving(false);
}

void Critter::takeDamage(int damage, Critter* attacker) {
  if (dead())
    return;
  hp_ -= damage;
  if (dead())
    die();
  damagedSignal_(this, attacker);
}

void Critter::heal(int hp) {
  if (!dead())
    hp_ += hp;
}

void Critter::die() {
  hp_ = 0;
  setAI(nullptr);
  planner()->abortPlan();
  planner_.reset();
  node()->setRotation(core::vector3df(90, 90, 0));
}

void Critter::afterLocationChange() {
  path_.reset();
  weaponMountNodes_.clear();

  if (!node())
    return;

#if !defined(NDEBUG)
  node()->setDebugDataVisible(EDS_SKELETON);
  meleeSphere_.reset(
      location()->scene()->addSphereSceneNode(desc_->meleeDistance * kUnitsPerMeter, 16, node()));
  meleeSphere_->setMaterialFlag(video::EMF_WIREFRAME, true);
  path_.reset(new PointsSceneNode(location()->structure(), node()->getSceneManager()));
  path_->setRenderType(EPT_LINE_STRIP);
#endif
  node()->addAnimator(pathAnimator_.get());
  node()->setMaterialType(video::EMT_LIGHTMAP);
  for (const std::string& jointName : desc_->weaponMountJoints) {
    weaponMountNodes_.push_back(animatedNode()->getJointNode(jointName.c_str()));
  }

  stopAnimation();

  if (ai_)
    ai_->makePlan();
}

void Critter::wield(std::shared_ptr<Weapon> weapon) {
  assert(weapon);
  if (!weaponMountNodes_.size())
    return;
  if (weapon_)
    inventory_->add(weapon_);
  if (!weapon->node())
    weapon->addToLocation(location(), vector3df());
  weaponMountNodes_[0]->addChild(weapon->node());
  weapon->node()->setRotation(vector3df(90, 90, 180));  // TODO: get from weapon def file
  weapon_ = weapon;
}

void Critter::dropWeapon() {
  assert(onLocation());
  if (!weapon_)
    return;
  location()->addEntity(std::move(weapon_), node()->getPosition());
}

void Critter::setAI(std::unique_ptr<CritterAI> ai) {
  if (ai_)
    ai_->setCritter(nullptr);
  ai_ = std::move(ai);
  if (ai_)
    ai_->setCritter(std::static_pointer_cast<Critter>(shared_from_this()));
}

boost::signals2::connection Critter::connectToDamaged(DamagedSignal::slot_type slot) {
  return damagedSignal_.connect(slot);
}

// irr::scene::ISceneNodeAnimator::IListener implementation.
void Critter::onFinished(ISceneNodeAnimator* animator) {
  stopAnimation();
  notifyStoppedMoving(true);
}

void Critter::notifyStoppedMoving(bool reachedDestination) {
  if (!moveCallback_)
    return;
  Callback callback;
  std::swap(callback, moveCallback_);
  callback(reachedDestination);
}

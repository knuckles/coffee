#include "EntityFactory.hpp"

#include <assert.h>
#include <boost/algorithm/string.hpp>
#include <map>
#include <stdexcept>
#include <string>

#include "ai/CritterAI.hpp"
#include "base/constants.h"
#include "base/irrlicht_ptrs.hpp"
#include "base/IrrlichtStreams.hpp"
#include "base/json_utils.hpp"
#include "ent/Critter.hpp"
#include "ent/entity_descriptions.hpp"
#include "ent/Weapon.hpp"
#include "game/Coffee.hpp"
#include "game/Inventory.hpp"
#include "map/BSPEntity.hpp"
#include "map/ItemListParser.hpp"

#include "3rdparty/irrlicht/include/IFileSystem.h"
#include "3rdparty/irrlicht/include/IrrlichtDevice.h"
#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/path.h"
#include "3rdparty/jsoncpp/include/json/json.h"

using namespace irr;
using namespace irr::core;
using namespace irr::io;
using namespace irr::scene;

namespace {
// Item definition config file path.
const char kItemDefsPath[] = "items.json";

// Weapon definition key names.
const char kAmmoKeyName[] = "ammo";
const char kClassKeyName[] = "class";
const char kDamageKeyName[] = "damage";
const char kDescriptionKeyName[] = "description";
const char kNameKeyName[] = "name";
const char kStablePosKeyName[] = "stablePosition";
const char kWeightKeyName[] = "weight";
const char kModelKeyName[] = "model";

const path kModelsPath("models/");
const path kAnimsPath("animations/");

const float kDefaultWalkSpeed = 5000.0 / 3600.0;  // 5km/h
const float kDefaultRunSpeed = 7000.0 / 3600.0;  // 7km/h

path getModelPath(const std::string& name) {
  return kModelsPath + name.c_str() + ".json";
}

path getAnimationsPath(const std::string& actionSetName) {
  return kAnimsPath + actionSetName.c_str() + ".b3d";
}

path getRigPath(const std::string& rigName) {
  return kAnimsPath + rigName.c_str() + ".json";
}

std::unique_ptr<Json::Value> parseJson(const path& filePath) {
  static Json::Reader reader;
  IFileSystem* fs = Coffee::getInstance()->device()->getFileSystem();
  std::unique_ptr<Json::Value> result;
  boost::intrusive_ptr<IReadFile> file(fs->createAndOpenFile(filePath), false);
  if (!file)
    return result;
  InputIrrlichtStream stream(file.get());
  result.reset(new Json::Value);
  if (!reader.parse(stream, *result))
    result.reset();
  return result;
}

struct EntityPair {
  Entity::Kind entityKind;
  Item::Kind itemKind;
};

const std::map<std::string, Entity::Kind> kEntityKindsMap {
  {"object", Entity::kObjectEntity},
  {"item",   Entity::kItemEntity},
  {"npc",    Entity::kCritterEntity},
};

const std::map<std::string, Item::Kind> kItemKindsMap {
  {"weapon", Item::kWeaponItem},
  {"ammo",   Item::kAmmoItem},
  {"armor",  Item::kArmorItem},
};

EntityPair parseKind(const BSPEntity& bspEntity) {
  EntityPair result {
    Entity::kAbstractEntity,
    Item::kUnknownItem
  };

  std::vector<std::string> nameParts;
  boost::split(nameParts, bspEntity.className(), boost::is_any_of("_"));
  if (!nameParts.size())
    return result;

  const auto& iEntityKind = kEntityKindsMap.find(nameParts[0]);
  if (iEntityKind != kEntityKindsMap.end())
    result.entityKind = iEntityKind->second;
  if (result.entityKind == Entity::kItemEntity && nameParts.size() > 1) {
    const auto& iItemKind = kItemKindsMap.find(nameParts[1]);
    if (iItemKind != kItemKindsMap.end())
      result.itemKind = iItemKind->second;
  }
  return result;
}

bool parseRigConfig(const std::string& rigName,
                    FrameMap& frameMap,
                    std::vector<std::string>& weaponMountJoints,
                    float* occupyRadius,
                    float* meleeDistance) {
  std::unique_ptr<Json::Value> rigConfig = parseJson(getRigPath(rigName));
  if (!rigConfig)
    return false;
  assert(rigConfig->isObject());
  *occupyRadius = rigConfig->get("occupyRadius", 0.5).asFloat();
  *meleeDistance = rigConfig->get("meleeDistance", 1).asFloat();
  const Json::Value& weaponMountJointsValue = (*rigConfig.get())["weaponMountJoints"];
  assert(weaponMountJointsValue.isArray());
  for (int i = 0; i < weaponMountJointsValue.size(); ++i) {
    weaponMountJoints.push_back(weaponMountJointsValue[i].asString());
  }
  const Json::Value& frameMapValue = (*rigConfig.get())["frameMap"];
  assert(frameMapValue.isObject());
  for (auto i = frameMapValue.begin(); i != frameMapValue.end(); ++i) {
    const Json::Value& framesValue = frameMapValue[i.memberName()];
    if (!framesValue.isArray() || framesValue.size() < 2)
      continue;
    const int f1 = framesValue[0].asInt();
    const int f2 = framesValue[1].asInt();
    frameMap.insert(FrameMap::value_type(i.memberName(), {f1, f2}));
  }
  // Ensure, there's a rest pose in the frame map.
  if (frameMap.find(Object::kRestPoseName) == frameMap.end())
    frameMap.insert({Object::kRestPoseName, {1, 1}});
  return true;
}

CritterDescription* parseCritter(const std::string& name, const std::wstring& wideName) {
  std::unique_ptr<Json::Value> critterConfig = parseJson(getModelPath(name));
  if (!critterConfig)
    return nullptr;
  assert(critterConfig && critterConfig->isObject());

  const std::string& modelPath = (*critterConfig.get())["model"].asString();
  assert(!modelPath.empty());
  const float walkSpeed =
      critterConfig->get("walkSpeed", kDefaultWalkSpeed).asFloat() * kUnitsPerMeter;
  assert(walkSpeed > 0.01);
  const float runSpeed =
      critterConfig->get("runSpeed", kDefaultRunSpeed).asFloat() * kUnitsPerMeter;
  assert(runSpeed > 0.01);
  const std::string& rigName = (*critterConfig.get())["rig"].asString();
  assert(!rigName.empty());

  FrameMap frameMap;
  std::vector<std::string> weaponMountJoints;
  float occupyRadius;
  float meleeDistance;
  if (!parseRigConfig(rigName, frameMap, weaponMountJoints, &occupyRadius, &meleeDistance))
    return nullptr;
  return new CritterDescription(wideName,
                                L"",
                                modelPath,
                                std::move(frameMap),
                                rigName,
                                weaponMountJoints,
                                occupyRadius,
                                meleeDistance,
                                walkSpeed,
                                runSpeed);
}

}  // namespace

EntityFactory::EntityFactory(ISceneManager* scene)
  : scene_(scene) {
  assert(scene_);
  itemDefs_ = parseJson(kItemDefsPath);
  if (!itemDefs_ || !itemDefs_->isObject())
    throw std::runtime_error("Could not parse item definitions JSON");
  weaponDefs_ = &(*itemDefs_.get())["weapons"];
  ammoDefs_ = &(*itemDefs_.get())["ammo"];
}

EntityFactory::~EntityFactory() = default;

std::shared_ptr<Entity> EntityFactory::createEntity(const BSPEntity& bspEntity) {
  if (!bspEntity.hasType())
    return nullptr;
  const EntityPair entityPair = parseKind(bspEntity);
  switch (entityPair.entityKind) {
    case Entity::kCritterEntity:
      return createCritter(bspEntity);
    case Entity::kItemEntity:
      return createItem(bspEntity.getType(), entityPair.itemKind);
#if !defined(NDEBUG)
    default:
      return std::shared_ptr<Entity>(new Entity());
#endif
  }
  return nullptr;
}

std::shared_ptr<Weapon> EntityFactory::createWeapon(const std::string& type) {
  std::shared_ptr<WeaponDescription> desc;
  const auto& iWeapon = weaponsCache_.find(type);
  if (iWeapon != weaponsCache_.end()) {
    desc = iWeapon->second;
  }
  else {
    Json::Value& weaponDef = (*weaponDefs_)[type];
    if (weaponDef.isNull())
      return nullptr;

    desc.reset(new WeaponDescription {
        toWide_.from_bytes(weaponDef[kNameKeyName].asString()),
        toWide_.from_bytes(weaponDef[kDescriptionKeyName].asString()),
        weaponDef[kModelKeyName].asString(),
        floatPointFromJsonArray(weaponDef[kStablePosKeyName]),
        weaponDef[kClassKeyName].asString(),
        weaponDef[kAmmoKeyName].asString(),
        weaponDef[kDamageKeyName].asInt(),
        weaponDef[kWeightKeyName].asFloat()
    });
    weaponsCache_[type] = desc;
  }

  return std::shared_ptr<Weapon>(new Weapon(desc, scene_->getMesh(path(desc->modelPath.c_str()))));
}

std::shared_ptr<Item> EntityFactory::createAmmo(const std::string& type) {
  std::shared_ptr<ItemDescription> desc;
  const auto& iAmmo = ammoCache_.find(type);
  if (iAmmo != ammoCache_.end()) {
    desc = iAmmo->second;
  }
  else {
    Json::Value& ammoDef = (*ammoDefs_)[type];
    if (ammoDef.isNull())
      return nullptr;

    desc.reset(new ItemDescription {
        toWide_.from_bytes(ammoDef[kNameKeyName].asString()),
        toWide_.from_bytes(ammoDef[kDescriptionKeyName].asString()),
        ammoDef[kModelKeyName].asString(),
        floatPointFromJsonArray(ammoDef[kStablePosKeyName]),
        ammoDef[kWeightKeyName].asFloat()
    });
    ammoCache_[type] = desc;
  }
  return std::shared_ptr<Item>(new Item(desc, scene_->getMesh(path(desc->modelPath.c_str()))));
}

std::shared_ptr<Critter> EntityFactory::createCritter(const BSPEntity& bspEntity, bool npc) {
  auto critter = createCritter(bspEntity.getType(), npc);
  if (!critter)
    return nullptr;
  const auto& inventoryString = bspEntity.properties().at("inventory").asString();
  for (const auto& iItem : ItemListParser::parse(inventoryString)) {
    std::shared_ptr<Item> item(createItem(iItem.first, findItemKind(iItem.first)));
    item->setQuantity(iItem.second);
    critter->inventory()->add(item);
  }
  return critter;
}

std::shared_ptr<Critter> EntityFactory::createCritter(const std::string& name, bool npc) {
  std::shared_ptr<CritterDescription> desc;
  const auto& iCritter = crittersCache_.find(name);
  if (iCritter != crittersCache_.end()) {
    desc = iCritter->second;
  }
  else {
    desc.reset(parseCritter(name, toWide_.from_bytes(name)));
    crittersCache_[name] = desc;
  }

  // Create animated mesh.
  IAnimatedMesh* critterMesh = scene_->getMesh(path(desc->modelPath.c_str()));
  assert(critterMesh);
  assert(critterMesh->getMeshType() == EAMT_SKINNED);
  ISkinnedMesh* critterSkin = static_cast<ISkinnedMesh*>(critterMesh);
  IAnimatedMesh* animations = scene_->getMesh(getAnimationsPath(desc->rigName));
  assert(animations);
  assert(animations->getMeshType() == EAMT_SKINNED);
  critterSkin->useAnimationFrom(static_cast<ISkinnedMesh*>(animations));

  std::shared_ptr<Critter> critter(new Critter(desc, critterSkin));
  if (npc)
    critter->setAI(std::unique_ptr<CritterAI>(new CritterAI));
  return critter;
}

std::shared_ptr<Item> EntityFactory::createItem(const std::string& type, Item::Kind itemKind)  {
  switch (itemKind) {
    case Item::kWeaponItem:
      return createWeapon(type);
    case Item::kAmmoItem:
      return createAmmo(type);
    default:
      return nullptr;
  }
}

Item::Kind EntityFactory::findItemKind(const std::string& type) const {
  if(weaponDefs_->isMember(type))
    return Item::kWeaponItem;
  if(ammoDefs_->isMember(type))
    return Item::kAmmoItem;
  return Item::kUnknownItem;
}

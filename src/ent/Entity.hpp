#ifndef ENTITY_HPP
#define ENTITY_HPP

#include <functional>
#include <map>
#include <memory>
#include <string>
#include <utility>

#include "base/macros.hpp"
#include "base/irrlicht_ptrs.hpp"
#include "scene/NodeKinds.hpp"

#include "3rdparty/irrlicht/include/vector3d.h"

class Location;

namespace irr {

namespace scene {
class ISceneNode;
}

}

/**
 * @brief The Entity class is a base for all other game entities.
 *
 * It may have some common properties, but it does not define any look (which is done by Object).
 * Such entities however may belong to some scene (Location) and have scene nodes with an optional
 * debug mesh (colored cube).
 */
class Entity : public std::enable_shared_from_this<Entity> {
public:
  enum Kind {
    kAbstractEntity = kNodeKindHighestBit << 1,
    kObjectEntity = kNodeKindHighestBit << 2,
    kItemEntity = kNodeKindHighestBit << 3,
    kCritterEntity = kNodeKindHighestBit << 4,
  };

  Entity();
  virtual ~Entity();

  irr::scene::ISceneNode* node() const { return node_.get(); }
  Location* location() const { return location_; }
  void addToLocation(Location* location, const irr::core::vector3df& position);
  void removeFromLocation();
  Kind entityKind() const { return kind_; }

protected:
  bool onLocation() const;

  void setEntityKind(Kind kind) { kind_ = kind; }
  void setStubColor(irr::video::SColor color);

  virtual irr::scene::ISceneNode* createNode(irr::scene::ISceneManager* scene);
  virtual void beforeLocationChange() {}
  virtual void afterLocationChange() {}

private:
  Kind kind_;
  irr::video::SColor stubColor_;
  Location* location_;

  ScopedSceneNode<irr::scene::ISceneNode> node_;

  DISALLOW_COPY_AND_ASSIGN(Entity);
};

#endif  // ENTITY_HPP

#ifndef WEAPON_HPP
#define WEAPON_HPP

#include <memory>

#include "base/macros.hpp"
#include "ent/Item.hpp"

class WeaponDescription;

class Weapon : public Item {
public:
  Weapon(std::shared_ptr<WeaponDescription> desc, irr::scene::IAnimatedMesh* mesh);
  ~Weapon();

  const WeaponDescription& weaponDescription() const;
private:
  std::shared_ptr<WeaponDescription> desc_;

  DISALLOW_COPY_AND_ASSIGN(Weapon);
};

#endif // WEAPON_HPP

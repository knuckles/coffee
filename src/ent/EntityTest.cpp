#include <vector>

#include "ent/Entity.hpp"
#include "ent/entity_descriptions.hpp"
#include "ent/Object.hpp"

#include "base/irrlicht_ptrs.hpp"
#include "test/irr/TestAnimatedMeshSceneNode.hpp"
#include "test/irr/TestMeshSceneNode.hpp"
#include "test/irr/TestSceneManager.hpp"
#include "test/LocationMock.hpp"

#include "3rdparty/irrlicht/include/SAnimatedMesh.h"
#include "gtest/gtest.h"

using namespace testing;

class EntityTest : public testing::Test {
protected:
  void SetUp() override {
    ON_CALL(location_, scene()).WillByDefault(Return(&scene_));
    ON_CALL(location_, structure()).WillByDefault(Return(&structure_));
  }

  NiceMock<LocationMock> location_;
  TestSceneManager scene_;
  TestMeshSceneNode structure_;
};

TEST_F(EntityTest, Entity) {
  Entity entity;
  ASSERT_EQ(Entity::kAbstractEntity, entity.entityKind());
  ASSERT_EQ(nullptr, entity.location());
  ASSERT_EQ(nullptr, entity.node());

  entity.addToLocation(&location_, irr::core::vector3df());
  ASSERT_NE(nullptr, entity.node());
  entity.removeFromLocation();
  ASSERT_EQ(nullptr, entity.node());
}

TEST_F(EntityTest, Object) {
  const int walkStartFrame = 5;
  const int walkEndFrame = 15;
  const int walkFrames = walkEndFrame - walkStartFrame;
  const int shootStartFrame = 20;
  const int shootEndFrame = 40;
  std::shared_ptr<ObjectDescription> desc(
      new ObjectDescription(L"name", L"descr", "model",
                            { {"walk", {walkStartFrame, walkEndFrame}},
                              {"shoot", {shootStartFrame, shootEndFrame}} }));
  boost::intrusive_ptr<irr::scene::IAnimatedMesh> mesh(new irr::scene::SAnimatedMesh());
  Object object(desc, mesh.get());
  ASSERT_EQ(desc.get(), &object.objectDescription());
  ASSERT_EQ(mesh.get(), object.mesh());
  ASSERT_EQ(walkFrames * 25, object.getAnimationDuration("walk"));
  ASSERT_EQ(0, object.getAnimationDuration("non existent action"));

  StrictMock<TestAnimatedMeshSceneNode> node;
  EXPECT_CALL(scene_, addAnimatedMeshSceneNode(mesh.get(), _, _, _, _, _, _)).
      WillOnce(Return(&node));
  object.addToLocation(&location_, irr::core::vector3df());
  ASSERT_NE(nullptr, object.node());

  // Set up some animation.
  bool callbackCalled = false;
  bool animSuccess = false;
  Object::Callback animCallback = [&callbackCalled, &animSuccess](bool success) {
    callbackCalled = true;
    animSuccess = success;
  };
  ASSERT_EQ(0, node.getStartFrame());
  ASSERT_EQ(0, node.getEndFrame());
  ASSERT_EQ(false, node.getLoopMode());

  // Start looped animation...
  object.playAnimation("walk", true, animCallback);
  EXPECT_EQ(walkStartFrame, node.getStartFrame());
  EXPECT_EQ(walkEndFrame, node.getEndFrame());
  EXPECT_EQ(true, node.getLoopMode());
  EXPECT_EQ(false, callbackCalled);

  // ... and abort it.
  object.stopAnimation();
  EXPECT_EQ(true, callbackCalled);
  EXPECT_EQ(false, animSuccess);

  // Try again with non-looped.
  callbackCalled = false;
  object.playAnimation("walk", false, animCallback);
  node.setFrameNr(walkEndFrame);
  EXPECT_EQ(true, callbackCalled);
  EXPECT_EQ(true, animSuccess);

  // Chain animations.
  callbackCalled = false;
  animSuccess = false;
  std::vector<int> steps;
  Object::StepCallback stepCallback = [&steps](int step) { steps.push_back(step); };
  object.chainAnimations({"walk", "shoot"}, animCallback, stepCallback);
  EXPECT_TRUE(steps.empty());
  node.setFrameNr(walkEndFrame);
  EXPECT_EQ(false, callbackCalled);
  EXPECT_EQ(false, animSuccess);
  EXPECT_TRUE(steps.size() == 1 && steps[0] == 0);
  node.setFrameNr(shootEndFrame);
  EXPECT_EQ(true, callbackCalled);
  EXPECT_EQ(true, animSuccess);
  EXPECT_TRUE(steps.size() == 2 && steps[1] == 1);

  // Remove object.
  object.removeFromLocation();
  ASSERT_EQ(nullptr, object.node());
}

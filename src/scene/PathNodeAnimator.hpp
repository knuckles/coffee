#ifndef PATHNODEANIMATOR_HPP
#define PATHNODEANIMATOR_HPP

#include "nav/Navigator.hpp"

#include "3rdparty/irrlicht/include/irrTypes.h"
#include "3rdparty/irrlicht/include/ISceneNodeAnimator.h"

/**
 * @brief Drives node along a given path.
 */
class PathNodeAnimator : public irr::scene::ISceneNodeAnimator {
public:
  PathNodeAnimator();
  /**
   * @brief PathNodeAnimator copy ctor.
   * THIS WON'T COPY NOR LISTENER NOR PATH AND CURRENT PROGRESS.
   */
  PathNodeAnimator(const PathNodeAnimator& other);
  ~PathNodeAnimator();

  void setPath(Navigator::Path&& path);
  void setSpeed(irr::f32 unitsPerSec) { unitsPerSec_ = unitsPerSec; }

  // ISceneNodeAnimator implementation
  virtual void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs) override;
  virtual ISceneNodeAnimator* createClone(irr::scene::ISceneNode* node,
                                          irr::scene::ISceneManager* newManager=0) override;
  virtual void setListener(IListener* listener) override;

private:
  void finish();
  void disable();

  ISceneNodeAnimator::IListener* listener_;
  irr::u32 lastAnimationTime_;
  irr::f32 unitsPerSec_;
  Navigator::Path path_;
  int nextPointIndex_;

  // Disallow assignment.
  void operator=(const PathNodeAnimator&);
};

#endif // PATHNODEANIMATOR_HPP

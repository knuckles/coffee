#include "PathNodeAnimator.hpp"

#include "3rdparty/irrlicht/include/ISceneNode.h"

using namespace irr;
using namespace irr::core;
using namespace irr::gui;
using namespace irr::scene;

PathNodeAnimator::PathNodeAnimator()
  : irr::IReferenceCounted(),
    listener_(nullptr),
    lastAnimationTime_(0),
    unitsPerSec_(0.0f),
    nextPointIndex_(-1) {
}

PathNodeAnimator::PathNodeAnimator(const PathNodeAnimator& other)
  : listener_(nullptr),
    lastAnimationTime_(0),
    unitsPerSec_(other.unitsPerSec_),
    nextPointIndex_(0) {
}

PathNodeAnimator::~PathNodeAnimator() = default;

void PathNodeAnimator::setPath(Navigator::Path&& path) {
  path_ = std::move(path);
  if (path_.size())
    nextPointIndex_ = 0;
  else
    disable();
}

void PathNodeAnimator::animateNode(ISceneNode* node, u32 timeMs) {
  if (nextPointIndex_ < 0 || !unitsPerSec_ || !node)
    return;

  if (!lastAnimationTime_) {
    lastAnimationTime_ = timeMs;
    return;
  }
  f32 timeDiff = timeMs - lastAnimationTime_;
  lastAnimationTime_ = timeMs;

  core::vector3df nodePos = node->getPosition();
  f32 l = unitsPerSec_ * timeDiff * 0.001f;  // Total distance the node travelled since last time.
  vector3df destPointer;
  do {  // Traverse intermediate path points.
    destPointer = path_[nextPointIndex_] - nodePos;
    l -= destPointer.getLength();
    if (l >= 0) {
      nodePos = path_[nextPointIndex_];
      nextPointIndex_++;
    }
  } while (l > 0 && nextPointIndex_ < path_.size());

  if (nextPointIndex_ == path_.size()) {
    // Went through all points.
    node->setPosition(nodePos);
    finish();
    return;
  }

  // Move a little more towards the next point.
  vector3df move = path_[nextPointIndex_] - nodePos;
  move.setLength(move.getLength() - fabs(l));
  node->setPosition(nodePos + move);

  // Orient node towards its destination.
  node->setRotation((nodePos - node->getPosition()).getHorizontalAngle());
}

ISceneNodeAnimator* PathNodeAnimator::createClone(ISceneNode* node, ISceneManager* newManager) {
  return new PathNodeAnimator(*this);
}

void PathNodeAnimator::setListener(ISceneNodeAnimator::IListener* listener) {
  listener_ = listener;
}

void PathNodeAnimator::finish() {
  disable();
  if (listener_)
    listener_->onFinished(this);
}

void PathNodeAnimator::disable() {
  nextPointIndex_ = -1;
  lastAnimationTime_ = 0;
}



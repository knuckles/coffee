#include "InventoryWindow.hpp"

#include "ent/Critter.hpp"
#include "ent/entity_descriptions.hpp"
#include "ent/Item.hpp"
#include "game/Coffee.hpp"
#include "game/Inventory.hpp"

#include "3rdparty/irrlicht/include/IGUIEnvironment.h"
#include "3rdparty/irrlicht/include/IGUIButton.h"
#include "3rdparty/irrlicht/include/IGUIListBox.h"
#include "3rdparty/irrlicht/include/IGUIWindow.h"
#include "3rdparty/irrlicht/include/IrrlichtDevice.h"
#include "3rdparty/irrlicht-guittf/create_ttf.h"

using namespace irr;
using namespace irr::gui;

InventoryWindow::InventoryWindow(Critter* critter)
  : device_(Coffee::getInstance()->device()),
    critter_(critter),
    lastReceiver_(nullptr),
    wnd_(nullptr) {
  init();
}

InventoryWindow::~InventoryWindow() {
  wnd_ = nullptr;
  critter_ = nullptr;
}

void InventoryWindow::show() {
  lastReceiver_ = device_->getEventReceiver();
  device_->setEventReceiver(this);
  updateControls();
  wnd_->setVisible(true);
}

void InventoryWindow::hide() {
  wnd_->setVisible(false);
  device_->setEventReceiver(lastReceiver_);
}

void InventoryWindow::init() {
  IGUIEnvironment* gui = device_->getGUIEnvironment();
  const io::path& fontPath = Coffee::getInstance()->config()->uiFontPath();
  if (!fontPath.empty())
    gui->getSkin()->setFont(
        createTTFont(device_->getVideoDriver(),
                     device_->getFileSystem(),
                     fontPath,
                     Coffee::getInstance()->config()->uiFontSize()));

  const core::dimension2d<u32> screenSize = device_->getVideoDriver()->getScreenSize();
  const core::dimension2d<u32> wndSize = screenSize * 2 / 3;
  const core::position2d<s32> wndPos((screenSize.Width - wndSize.Width) / 2,
                                     (screenSize.Height - wndSize.Height) / 2);
  const core::rect<s32> wndRect(wndPos, wndSize);
  wnd_ = gui->addWindow(wndRect, true, L"Inventory");
  wnd_->setVisible(false);
  wnd_->setAlignment(EGUIA_UPPERLEFT, EGUIA_LOWERRIGHT, EGUIA_UPPERLEFT, EGUIA_LOWERRIGHT);

  const core::rect<s32> clientRect = wnd_->getClientRect();

  core::rect<s32> listRect(10, 10, 160, clientRect.getHeight() - 20);
  listRect += wnd_->getClientRect().UpperLeftCorner;
  itemList_ = gui->addListBox(listRect, wnd_);
  itemList_->setAlignment(EGUIA_UPPERLEFT, EGUIA_UPPERLEFT, EGUIA_UPPERLEFT, EGUIA_LOWERRIGHT);
}

void InventoryWindow::updateControls() {
  itemList_->clear();
  for (auto i = critter_->inventory()->begin(); i != critter_->inventory()->end(); ++i) {
    itemList_->addItem((*i)->objectDescription().name.c_str());
  }
}

bool InventoryWindow::OnEvent(const SEvent& event) {
  if (event.EventType != EET_GUI_EVENT)
    return false;

//  if (event.GUIEvent.EventType != EGET_BUTTON_CLICKED)
//    return false;

  IGUIElement* caller = event.GUIEvent.Caller;
  if (event.GUIEvent.EventType == EGET_ELEMENT_CLOSED &&
      caller == wnd_) {
    hide();
    return true;  // Prevent window destruction.
  }

  return false;
}

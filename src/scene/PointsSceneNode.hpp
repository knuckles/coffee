#ifndef POINTSSCENENODE_HPP
#define POINTSSCENENODE_HPP

#include <memory>

#include "base/macros.hpp"

#include "3rdparty/irrlicht/include/aabbox3d.h"
#include "3rdparty/irrlicht/include/ISceneNode.h"
#include "3rdparty/irrlicht/include/SMeshBuffer.h"

class PointsSceneNode : public irr::scene::ISceneNode {
public:
  PointsSceneNode(ISceneNode* parent, irr::scene::ISceneManager* manager, irr::s32 id = -1);
  ~PointsSceneNode();

  // ISceneNode
  virtual void OnRegisterSceneNode() override;
  virtual void render() override;
  virtual const irr::core::aabbox3d<irr::f32>& getBoundingBox() const override;

  void setPoints(const irr::core::array<irr::core::vector3df>& points);
  void setRenderType(irr::scene::E_PRIMITIVE_TYPE type) { type_ = type; }

private:
  irr::scene::SMeshBuffer buffer_;
  irr::scene::E_PRIMITIVE_TYPE type_;

  DISALLOW_COPY_AND_ASSIGN(PointsSceneNode);
};

#endif  // POINTSSCENENODE_HPP

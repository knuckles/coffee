#include "PointsSceneNode.hpp"

#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/IVideoDriver.h"

using namespace irr;
using namespace irr::core;
using namespace irr::scene;

PointsSceneNode::PointsSceneNode(ISceneNode* parent, ISceneManager* manager, s32 id)
  : ISceneNode(parent, manager, id),
    type_(EPT_POINTS) {
  buffer_.Material.Lighting = false;  // Use vertex colors.
  buffer_.Material.Wireframe = true;
  buffer_.Material.Thickness = 2.0f;
  setMaterialFlag(video::EMF_WIREFRAME, true);
}

PointsSceneNode::~PointsSceneNode() = default;

void PointsSceneNode::OnRegisterSceneNode() {
  if (IsVisible) {
    getSceneManager()->registerNodeForRendering(this);
    ISceneNode::OnRegisterSceneNode();
  }
}

void PointsSceneNode::render() {
  auto* driver = getSceneManager()->getVideoDriver();

  core::matrix4 transformation;
  driver->setTransform(video::ETS_WORLD, transformation);
  driver->setMaterial(buffer_.Material);
  driver->drawVertexPrimitiveList(
      buffer_.getVertices(), buffer_.getVertexCount(),
      buffer_.getIndices(), buffer_.getIndexCount() -1 ,
      video::EVT_STANDARD, type_, buffer_.getIndexType());
}

const aabbox3d<f32>& PointsSceneNode::getBoundingBox() const {
  return buffer_.getBoundingBox();
}

void PointsSceneNode::setPoints(const array<vector3df>& points) {
  buffer_.Vertices.set_used(points.size());
  buffer_.Indices.set_used(points.size());

  video::S3DVertex vertex;
  vertex.Color.set(255, 255, 0, 0);
  for (u16 i = 0; i < points.size(); ++i) {
    vertex.Pos = points[i];
    buffer_.Vertices[i] = vertex;
    buffer_.Indices[i] = i;
  }
  buffer_.recalculateBoundingBox();
}



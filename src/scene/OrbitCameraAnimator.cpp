#include "OrbitCameraAnimator.hpp"

#include <assert.h>
#include <cmath>

#include "game/Coffee.hpp"
#include "map/Location.hpp"

#include "3rdparty/irrlicht/include/IAnimatedMeshSceneNode.h"
#include "3rdparty/irrlicht/include/ICameraSceneNode.h"
#include "3rdparty/irrlicht/include/ICursorControl.h"
#include "3rdparty/irrlicht/include/IrrlichtDevice.h"
#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/ISceneNode.h"
#include "3rdparty/irrlicht/include/Keycodes.h"

using namespace irr;
using namespace irr::core;
using namespace irr::gui;
using namespace irr::scene;

namespace {
const u32 kDefaultDistance = 200;
const f32 kDefaultRotationDegreesPerMS = 0.05;
const f32 kDefaultZoomSpeed = 10;
const f32 kMinDistance = 20;
const f32 kMaxDistance = 500;
const f32 kMinHeight = -50;
const f32 kMaxHeight = 89;
const u32 kPlayerHeight = 70;
}

OrbitCameraAnimator::OrbitCameraAnimator(IrrlichtDevice* device, ISceneNode* sunNode)
    : device_(device),
      node_(sunNode),
      cursorControl_(Coffee::getInstance()->device()->getCursorControl()),
      rotationSpeed_(kDefaultRotationDegreesPerMS),
      distance_(kDefaultDistance),
      zoomSpeed_(kDefaultZoomSpeed),
      height_(45),
      azimuth_(0),
      floorOffset_(0, kPlayerHeight, 0),
      registeredEvents_(CONTROL_EVENTS_COUNT),
      lastAnimationTime_(0) {
  assert(node_);
  assert(cursorControl_);
  initKeyMap();
  initPosition();
}

OrbitCameraAnimator::~OrbitCameraAnimator() = default;

void OrbitCameraAnimator::animateNode(ISceneNode* node, u32 timeMs) {
  u32 gameTime = device_->getTimer()->getRealTime();
  u32 timeDiff = gameTime - lastAnimationTime_;
  lastAnimationTime_ = gameTime;

  if (!node || node->getType() != ESNT_CAMERA)
    return;
  ICameraSceneNode* camera = static_cast<ICameraSceneNode*>(node);
  if (node->getSceneManager()->getActiveCamera() != camera)
    return;

  const f32 rotation = timeDiff * rotationSpeed_;
  if (registeredEvents_[ROTATE_LEFT])
    azimuth_ -= rotation;
  if (registeredEvents_[ROTATE_RIGHT])
    azimuth_ += rotation;
  azimuth_ = fmod(azimuth_, 360);
  if (registeredEvents_[ROTATE_UP])
    height_ += rotation;
  height_ = std::min(height_, kMaxHeight);
  if (registeredEvents_[ROTATE_DOWN])
    height_ -= rotation;
  height_ = std::max(height_, kMinHeight);

  const float sinA = sin(azimuth_ * core::DEGTORAD);
  const float cosA = cos(azimuth_ * core::DEGTORAD);
  const float sinH = sin(height_ * core::DEGTORAD);
  const float cosH = cos(height_ * core::DEGTORAD);

  vector3df position(distance_ * sinA * cosH,
                     distance_ * sinH,
                     distance_ * -cosA * cosH);
  camera->setPosition(node_->getAbsolutePosition() + position + floorOffset_);
  camera->setTarget(node_->getAbsolutePosition() + floorOffset_);
}

ISceneNodeAnimator* OrbitCameraAnimator::createClone(ISceneNode* node, ISceneManager* newManager) {
  return new OrbitCameraAnimator(device_, node_);
}

bool OrbitCameraAnimator::isEventReceiverEnabled() const {
  return true;
}

bool OrbitCameraAnimator::OnEvent(const SEvent& event) {
  switch(event.EventType) {
    case EET_KEY_INPUT_EVENT: {
      auto keyIter = keyMap_.find(event.KeyInput.Key);
      if (keyIter != keyMap_.end()) {
        registeredEvents_[(*keyIter).second] = event.KeyInput.PressedDown;
        return true;
      }
      break;
    }
    case EET_MOUSE_INPUT_EVENT:
      if (event.MouseInput.Event == EMIE_MOUSE_WHEEL) {
          distance_ = std::max(kMinDistance,
                               std::min(distance_ - event.MouseInput.Wheel * zoomSpeed_,
                                        kMaxDistance));
          return true;
      }
      break;
    default:
      break;
  }
  return false;
}

void OrbitCameraAnimator::initKeyMap() {
  keyMap_[KEY_LEFT] = ROTATE_LEFT;
  keyMap_[KEY_KEY_A] = ROTATE_LEFT;
  keyMap_[KEY_RIGHT] = ROTATE_RIGHT;
  keyMap_[KEY_KEY_D] = ROTATE_RIGHT;
  keyMap_[KEY_UP] = ROTATE_UP;
  keyMap_[KEY_KEY_W] = ROTATE_UP;
  keyMap_[KEY_DOWN] = ROTATE_DOWN;
  keyMap_[KEY_KEY_S] = ROTATE_DOWN;
}

void OrbitCameraAnimator::initPosition() {

}

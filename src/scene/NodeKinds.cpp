#include "NodeKinds.hpp"

NodeKind getNodeKind(irr::scene::ISceneNode* node) {
  const irr::u32 id = static_cast<irr::u32>(node->getID());
  if (id & kStructureNode)
    return kStructureNode;
  if (id & kNavMeshNode)
    return kNavMeshNode;
  if (id & kEntityNode)
    return kEntityNode;
  return kUnknownNode;
}

#ifndef ORBITCAMERAANIMATOR_HPP
#define ORBITCAMERAANIMATOR_HPP

#include <map>
#include <vector>

#include "base/macros.hpp"

#include "3rdparty/irrlicht/include/irrTypes.h"
#include "3rdparty/irrlicht/include/ISceneNodeAnimator.h"

class Location;

namespace irr {

class IrrlichtDevice;

class ILogger;
struct SEvent;

namespace gui {
class ICursorControl;
}

namespace scene {
class ISceneManager;
class ISceneNode;
}

}

/// This animator controls camera, so it's bound to player position and is able to rotate around it.
class OrbitCameraAnimator : public irr::scene::ISceneNodeAnimator {
 public:
  /**
   * @brief OrbitCameraAnimator constructor.
   * @param sunNode is the node, around which the animated node (typically camera) will revolve.
   */
  OrbitCameraAnimator(irr::IrrlichtDevice* device, irr::scene::ISceneNode* sunNode);
  ~OrbitCameraAnimator();

  // ISceneNodeAnimator implementation
  virtual void animateNode(irr::scene::ISceneNode* node, irr::u32 timeMs) override;
  virtual ISceneNodeAnimator* createClone(irr::scene::ISceneNode* node,
                                          irr::scene::ISceneManager* newManager=0) override;
  virtual bool isEventReceiverEnabled() const override;
  virtual bool OnEvent(const irr::SEvent& event) override;

 private:
  enum ControlEvent {
    ROTATE_LEFT,
    ROTATE_RIGHT,
    ROTATE_UP,
    ROTATE_DOWN,

    CONTROL_EVENTS_COUNT
  };

  void initKeyMap();
  void initPosition();

  irr::IrrlichtDevice* device_;
  irr::scene::ISceneNode* node_;
  irr::gui::ICursorControl* cursorControl_;

  irr::f32 rotationSpeed_;
  irr::u32 distance_;  // distance to player node.
  irr::f32 zoomSpeed_;  // distance change speed.
  irr::f32 height_;  // camera height angle, degrees [0..90).
  irr::f32 azimuth_;  // camera azimuth angle, degrees [0..360).
  irr::core::vector3df floorOffset_;
  std::map<irr::EKEY_CODE, ControlEvent> keyMap_;
  std::vector<bool> registeredEvents_;
  irr::u32 lastAnimationTime_;

  DISALLOW_COPY_AND_ASSIGN(OrbitCameraAnimator);
};

#endif  // ORBITCAMERAANIMATOR_HPP

#ifndef INVENTORYWINDOW_HPP
#define INVENTORYWINDOW_HPP

#include "base/macros.hpp"
#include "base/irrlicht_ptrs.hpp"

#include "3rdparty/irrlicht/include/IEventReceiver.h"

class Critter;

namespace irr {
class IrrlichtDevice;

namespace gui {
class IGUIListBox;
class IGUIWindow;
}
}

class InventoryWindow : public irr::IEventReceiver {
public:
  explicit InventoryWindow(Critter* critter);
  ~InventoryWindow();

  // Shows the window. Note that this will allso change current event receiver. It will be restored
  // after window is closed (hidden).
  void show();
  void hide();

protected:
  irr::gui::IGUIWindow* window() { return wnd_; }

private:
  void init();
  void updateControls();

  // IEventReceiver
  virtual bool OnEvent(const irr::SEvent& event) override;

  irr::IrrlichtDevice* device_;
  Critter* critter_;
  irr::IEventReceiver* lastReceiver_;
  irr::gui::IGUIWindow* wnd_;
  irr::gui::IGUIListBox* itemList_;

  DISALLOW_COPY_AND_ASSIGN(InventoryWindow);
};

#endif  // INVENTORYWINDOW_HPP

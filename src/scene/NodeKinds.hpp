#ifndef NODEKINDS_HPP
#define NODEKINDS_HPP

#include "3rdparty/irrlicht/include/irrTypes.h"
#include "3rdparty/irrlicht/include/ISceneNode.h"

/**
 * @brief The NodeKind enum is stored in scene node ID.
 */
enum NodeKind {
  kUnknownNode = 0,
  kStructureNode = 1 << 0,
  kNavMeshNode = 1 << 1,
  kEntityNode = 1 << 2,
};
const irr::u32 kNodeKindHighestBit = 1 << 15;

NodeKind getNodeKind(irr::scene::ISceneNode* node);

#endif // NODEKINDS_HPP

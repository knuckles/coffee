#include "BSPEntity.hpp"

#include <sstream>

#include "base/json_utils.hpp"
#include "base/string_utils.hpp"

#include "3rdparty/irrlicht/include/SColor.h"

using namespace irr;
using namespace irr::core;
using namespace irr::video;

const std::string& BSPEntity::CLASS_VAR_NAME("classname");

namespace {
const char kColorPropertyName[] = "_color";
const char kOriginPropertyName[] = "origin";
const char kTypePropertyName[] = "type";

template <typename T>
Json::Value parseVector(const std::string& str) {
  Json::Value result(Json::arrayValue);
  for (const auto& numStr: tokenizeString(str)) {
    result.append(Json::Value(parseString<T>(numStr)));
  }
  return result;
}
}

BSPEntity::BSPEntity(const std::string& className)
  : className_(className) {
}

BSPEntity::BSPEntity(const std::string& className, std::map<std::string, std::string> properties)
  : className_(className) {
  for (const auto& varPair: properties) {
    addProperty(varPair.first, varPair.second);
  }
}

BSPEntity::BSPEntity(const BSPEntity& other)
  : className_(other.className_),
    properties_(other.properties_) {
}

BSPEntity::~BSPEntity() = default;

BSPEntity& BSPEntity::operator=(const BSPEntity& other) {
  className_ = other.className_;
  properties_ = other.properties_;
  return *this;
}

bool BSPEntity::hasProperty(const std::string& name) const {
  return properties_.find(name) != properties_.end();
}

bool BSPEntity::hasOrigin() const {
  return hasProperty(kOriginPropertyName);
}

vector3df BSPEntity::getOrigin() const {
  vector3df origin = floatPointFromJsonArray(properties_.at(kOriginPropertyName));
  std::swap(origin.Y, origin.Z);  // BSP upward axis is Z, not Y
  return origin;
}

bool BSPEntity::hasColor() const {
  return hasProperty(kColorPropertyName);
}

SColorf BSPEntity::getColor() const {
  return fColorFromJsonArray(properties_.at(kColorPropertyName));
}

bool BSPEntity::hasType() const {
  return hasProperty(kTypePropertyName);
}

std::string BSPEntity::getType() const {
  return properties_.at(kTypePropertyName).asString();
}

void BSPEntity::addProperty(std::string name, std::string value) {
  Json::Value propValue;

  if (name == kOriginPropertyName)
    propValue = parseVector<int>(value);
  else if (name == kColorPropertyName)
    propValue = parseVector<float>(value);
  else
    propValue = Json::Value(value);

  properties_[name] = propValue;
}

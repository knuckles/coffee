#include "LocationImpl.hpp"

#include <algorithm>
#include <memory>
#include <string>
#include <sstream>

#include "base/perf_utils.hpp"
#include "base/IrrlichtStreams.hpp"
#include "base/irrlicht_ptrs.hpp"
#include "BSPEntity.hpp"
#include "ent/Critter.hpp"
#include "ent/EntityFactory.hpp"
#include "game/Coffee.hpp"
#include "nav/NavBuilder.hpp"
#include "scene/NodeKinds.hpp"

#include "3rdparty/jsoncpp/include/json/json.h"
#include "3rdparty/irrlicht/include/IAnimatedMesh.h"
#include "3rdparty/irrlicht/include/IAnimatedMeshSceneNode.h"
#include "3rdparty/irrlicht/include/IFileSystem.h"
#include "3rdparty/irrlicht/include/ILightSceneNode.h"
#include "3rdparty/irrlicht/include/ILogger.h"
#include "3rdparty/irrlicht/include/IMeshCache.h"
#include "3rdparty/irrlicht/include/IMeshSceneNode.h"
#include "3rdparty/irrlicht/include/IrrlichtDevice.h"
#include "3rdparty/irrlicht/include/irrString.h"
#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/IQ3LevelMesh.h"
#include "3rdparty/irrlicht/include/SMesh.h"

using namespace irr;
using namespace irr::io;
using namespace irr::scene;

namespace {

const char kBSPSpawnPosEntityName[] = "info_player_start";

std::ostream& operator<<(std::ostream& stream, const core::aabbox3d<f32>& box) {
  return stream << "[" << box.MinEdge << "," << box.MaxEdge << "]";
}

void printMeshBuffers(IMesh* mesh, ILogger* logger) {
  std::stringstream logStream;

  u32 numBuffs = mesh->getMeshBufferCount();
  logStream << "mesh->getMeshBufferCount(): " << numBuffs;
  logger->log(logStream.str().c_str());
  logStream.str(std::string());
  for (u32 i = 0; i < numBuffs; ++i) {
    logStream << "Mesh buffer: " << i;
    logger->log(logStream.str().c_str());
    logStream.str(std::string());

    IMeshBuffer* meshBuffer = mesh->getMeshBuffer(i);
    const video::SMaterial& material = meshBuffer->getMaterial();
    for(u32 j = 0; j < video::MATERIAL_MAX_TEXTURES; ++j) {
      video::ITexture* texture = material.getTexture(j);
      if (!texture)
        continue;
      logStream << "  Texture: " << j << "\n"
                << "    path: " << texture->getName().getPath().c_str() << "\n";
      logger->log(logStream.str().c_str());
      logStream.str(std::string());
    }
  }
}
}  // namespace

LocationImpl::LocationImpl(const path& name, ISceneManager* sceneManager)
  : entityFactory_(new EntityFactory(sceneManager)),
    sceneManager_(sceneManager),
    logger_(Coffee::getInstance()->device()->getLogger()),
    navBuilder_(new NavBuilder()) {
  auto start = getPerfTime();
#if 0
  auto fileSystem = sceneManager->getFileSystem();
  assert(fileSystem);
  path locationPath(path("locations/") + name + ".json");
  UniqueIrrObject<IReadFile> locationFile(fileSystem->createAndOpenFile(locationPath));
  InputIrrlichtStream inputStream(locationFile.get());
  Json::Reader jsonReader;
  Json::Value root;
  jsonReader.parse(inputStream, root);
  assert(root.isObject());
  name_ = root["name"].asString();
  std::string modelName = root["model"].asString();
  assert(!modelName.empty());
  mesh_ = scene()->getMesh(path("locations/") + modelName.c_str());
#endif

  structure_.reset(scene()->getMesh(path("locations/") + name));
  assert(structure_.get());
  buildNavMesh();
  if (structure_->getMeshType() == EAMT_BSP)
    processEntities(static_cast<IQ3LevelMesh*>(structure_.get()));

  std::stringstream logStream;
  logStream << "Took " << getPerfTimeTillNowMs(start) << "ms to find and load location";
  logger_->log(logStream.str().c_str());
  logStream.str(std::string());

  printMeshData();
}

LocationImpl::~LocationImpl() {
  player_.reset();
  entitiesMap_.clear();
  walkNode_.reset();
  worldNode_.reset();
  structure_.reset();
  scene()->getMeshCache()->clearUnusedMeshes();
}

ISceneManager* LocationImpl::scene() const {
  return sceneManager_;
}

ISceneNode* const LocationImpl::structure() const {
  return worldNode();
}

Critter* LocationImpl::player() const {
  return player_.get();
}

Navigator* LocationImpl::navigator() const {
  return navBuilder_->navigator();
}

void LocationImpl::makeScene(ISceneNode* parent) {
  // Add level structure.
  worldNode_.reset(
      scene()->addOctreeSceneNode(structure_->getMesh(0), parent, kStructureNode));
  auto selector = scene()->createOctreeTriangleSelector(structure_->getMesh(0), worldNode(), 128);
  worldNode()->setTriangleSelector(selector);
  selector->drop();
  selector = nullptr;

  // Add walkable surface mesh.
  walkNode_.reset(
      scene()->addMeshSceneNode(walkSurface_.get(), parent, kNavMeshNode));
  selector = scene()->createOctreeTriangleSelector(walkSurface_.get(), walkNode(), 128);
  walkNode()->setTriangleSelector(selector);
  selector->drop();
  selector = nullptr;

  // Add player.
  player_ = entityFactory_->createCritter(Coffee::getInstance()->config()->playerModel(), false);
  player_->addToLocation(this, spawnPosition());

  // Add entities.
  for (auto& classNEntity: entities_) {
    const BSPEntity* bspEntity = classNEntity.second.get();
    if (!bspEntity->hasOrigin())
      continue;

    auto entity(entityFactory_->createEntity(*bspEntity));
    if (!entity)
      continue;
    addEntity(entity, bspEntity->getOrigin());
  }

#if !defined(NDEBUG)
  scene()->addLightSceneNode();
#endif
}

void LocationImpl::addEntity(std::shared_ptr<Entity> entity, const core::vector3df& position) {
  if (!entity->node())
    entity->addToLocation(this, position);
  else
    structure()->addChild(entity->node());
  entitiesMap_.insert(EntitiesMap::value_type(entity->node(), entity));
}

std::shared_ptr<Entity> LocationImpl::removeEntity(ISceneNode* entityNode) {
  std::shared_ptr<Entity> entity;
  EntitiesMap::iterator iter = entitiesMap_.find(entityNode);
  if (iter == entitiesMap_.end())
    return entity;
  entity = iter->second;
  entity->removeFromLocation();
  entitiesMap_.erase(iter);
  return entity;
}

Entity* LocationImpl::findEntity(const ISceneNode* entityNode) const {
  const auto entIter = entitiesMap_.find(entityNode);
  return entIter != entitiesMap_.end() ? entIter->second.get() : nullptr ;
}

void LocationImpl::processEntities(IQ3LevelMesh* q3mesh) {
  const quake3::tQ3EntityList& entities = q3mesh->getEntityList();
  for (int ei = 0; ei < entities.size(); ++ei) {
    logger_->log((core::string<char>("Entity ") + core::string<char>(ei)).c_str());
    const quake3::SVarGroup* varGroup = entities[ei].getGroup(1);  // It's always just there.
    if (!varGroup)
      continue;
    std::map<std::string, std::string> vars;
    for (u32 vi = 0; vi < varGroup->Variable.size(); ++vi) {
      logger_->log((core::string<char>("  Var ") + core::string<char>(vi)).c_str());
      const quake3::SVariable& var = varGroup->Variable[vi];
      vars[std::string(var.name.c_str())] = std::string(var.content.c_str());
      logger_->log((core::string<char>("    Name ") + core::string<char>(var.name)).c_str());
      logger_->log((core::string<char>("    Value ") + core::string<char>(var.content)).c_str());
    }
    // Must copy, because erase() may change it.
    const std::string className = vars[BSPEntity::CLASS_VAR_NAME];
    vars.erase(BSPEntity::CLASS_VAR_NAME);
    std::unique_ptr<BSPEntity> entity(new BSPEntity(className, vars));
    entities_.insert(BSPEntities::value_type(className, std::move(entity)));
  }

  // Find common entities.
  BSPEntities::const_iterator spawnPosIter = entities_.find(kBSPSpawnPosEntityName);
  if (spawnPosIter != entities_.end())
    spawnPosition_ = spawnPosIter->second->getOrigin();
}

void LocationImpl::buildNavMesh() {
  if (!navBuilder_)
    return;

  std::stringstream logStream;
  auto start = getPerfTime();

  if (!navBuilder_->buildNavMesh(structure_->getMesh(0))) {
    logger_->log("Could not build navigation mesh.", ELL_ERROR);
    return;
  }

  walkSurface_ = std::move(navBuilder_->getNavMesh());
  if (!walkSurface_)
    logger_->log("Could not visualize walkable surface", ELL_ERROR);

  logStream << "Took " << getPerfTimeTillNowMs(start) << "ms to build nav mesh";
  logger_->log(logStream.str().c_str());
  logStream.str(std::string());
}

void LocationImpl::printMeshData() {
  std::stringstream logStream;

  u32 numOfMeshes = structure_->getFrameCount();
  logStream << "Location: " << name_ << "\n"
            << "mesh_->getFrameCount(): " << numOfMeshes << "\n";
  logger_->log(logStream.str().c_str());
  logStream.str(std::string());

  for (int m = 0; m < numOfMeshes; ++m) {
    IMesh* mesh = structure_->getMesh(m);

    logStream << "mesh #" << m << ":\n";
    logger_->log(logStream.str().c_str());
    logStream.str(std::string());

    printMeshBuffers(mesh, logger_);
  }
}

#include "ItemListParser.hpp"

#include <algorithm>
#include <sstream>
#include <stdexcept>

#include <boost/algorithm/string.hpp>

namespace {
struct parseItem {
  ItemListParser::InventoryItem operator()(const std::string& src) {
    const auto nameBegin = src.begin();
    auto nameEnd = src.end();
    int qty = 1;
    if (const auto& rBrace = boost::find_last(src, ")")) {
      if (const auto& lBrace = boost::find_last(src, "(")) {
        if (lBrace.begin() < rBrace.begin()) {
          typedef std::remove_reference<decltype(rBrace)>::type IterRangeType;
          const auto& braced = boost::copy_range<std::string>(
              IterRangeType(lBrace.end(), rBrace.begin()));
          std::istringstream converter(braced);
          if (!(converter >> qty))
            throw std::runtime_error("Could not parse item quantity: " + braced);
          nameEnd = lBrace.begin();
        }
      }
    }
    return {std::string(nameBegin, nameEnd), qty};
  }
};
}  // namespace

ItemListParser::ItemList ItemListParser::parse(const std::string& src) {
  ItemListParser::ItemList result;
  std::list<std::string> itemStrings;
  boost::split(itemStrings, src, boost::is_any_of(","));
  std::transform(itemStrings.begin(), itemStrings.end(), std::back_inserter(result), parseItem());
  return result;
}

#ifndef LOCATION_HPP
#define LOCATION_HPP

#include <memory>
#include <string>
#include <map>

#include "3rdparty/irrlicht/include/vector3d.h"

class Critter;
class Entity;
class Navigator;

namespace irr {

namespace scene {
class ISceneManager;
class ISceneNode;
}  // namespace scene

}  // namespace irr

class Location {
public:
  typedef std::map<const irr::scene::ISceneNode*, std::shared_ptr<Entity>> EntitiesMap;

  virtual ~Location() {}

  virtual irr::scene::ISceneManager* scene() const = 0;
  virtual irr::scene::ISceneNode* const structure() const = 0;
  virtual Critter* player() const = 0;
  virtual Navigator* navigator() const = 0;

  virtual void makeScene(irr::scene::ISceneNode* parent) = 0;
  virtual void addEntity(std::shared_ptr<Entity> entity, const irr::core::vector3df& position) = 0;
  virtual std::shared_ptr<Entity> removeEntity(irr::scene::ISceneNode* entityNode) = 0;
  virtual Entity* findEntity(const irr::scene::ISceneNode* entityNode) const = 0;
};

#endif  // LOCATION_HPP

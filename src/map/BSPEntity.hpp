#ifndef BSPENTITY_HPP
#define BSPENTITY_HPP

#include <string>
#include <map>

#include "base/macros.hpp"

#include "3rdparty/irrlicht/include/vector3d.h"
#include "3rdparty/jsoncpp/include/json/value.h"

namespace irr {

namespace video {
class SColorf;
}

}

class BSPEntity {
public:
  typedef std::map<std::string, Json::Value> Properties;

  static const std::string& CLASS_VAR_NAME;

  explicit BSPEntity(const std::string& className);
  BSPEntity(const std::string& className,
            std::map<std::string, std::string> properties);
  ~BSPEntity();

  BSPEntity(const BSPEntity& other);
  BSPEntity& operator=(const BSPEntity& other);

  const Properties& properties() const { return properties_; }
  bool hasProperty(const std::string& name) const;

  // Some frequently used properties.
  const std::string& className() const { return className_; }
  bool hasOrigin() const;
  irr::core::vector3df getOrigin() const;
  bool hasColor() const;
  irr::video::SColorf getColor() const;
  bool hasType() const;
  std::string getType() const;

  // Adds new or replaces existing property for this entity.
  // |value| will be parsed according to |name|.
  void addProperty(std::string name, std::string value);

private:
  std::string className_;
  Properties properties_;

  BSPEntity() {}
};

#endif // BSPENTITY_HPP

#ifndef ITEMLISTPARSER_HPP
#define ITEMLISTPARSER_HPP

#include <list>
#include <string>
#include <utility>

#include "base/macros.hpp"

class ItemListParser {
public:
  typedef std::pair<std::string, int> InventoryItem;
  typedef std::list<InventoryItem> ItemList;

  // |src| must be a comma separated list of items. Each item may be suffixed with an integer number
  // put in parentheses, thic defines the item quantity. By default each item is single.
  static ItemList parse(const std::string& src);

private:
  ItemListParser() = delete;
  DISALLOW_COPY_AND_ASSIGN(ItemListParser);
};

#endif // ITEMLISTPARSER_HPP

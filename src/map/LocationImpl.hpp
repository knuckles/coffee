#ifndef LOCATIONIMPL_HPP
#define LOCATIONIMPL_HPP

#include "map/Location.hpp"

#include "base/irrlicht_ptrs.hpp"
#include "base/macros.hpp"

#include "3rdparty/irrlicht/include/path.h"

class BSPEntity;
class EntityFactory;
class NavBuilder;

namespace irr {
class ILogger;

namespace scene {
class IAnimatedMesh;
class IMesh;
class IQ3LevelMesh;
class SMesh;
}  // namespace scene

}  // namespace irr


class LocationImpl : public Location {
public:
  LocationImpl(const irr::io::path& name, irr::scene::ISceneManager* sceneManager);
  ~LocationImpl() override;

  irr::scene::ISceneManager* scene() const override;
  irr::scene::ISceneNode* const structure() const override;
  Critter* player() const override;
  Navigator* navigator() const override;

  void makeScene(irr::scene::ISceneNode* parent) override;
  void addEntity(std::shared_ptr<Entity> entity, const irr::core::vector3df& position) override;
  std::shared_ptr<Entity> removeEntity(irr::scene::ISceneNode* entityNode) override;
  Entity* findEntity(const irr::scene::ISceneNode* entityNode) const override;

protected:
  irr::scene::ISceneNode* const worldNode() const { return worldNode_.get(); }
  irr::scene::ISceneNode* const walkNode() const { return walkNode_.get(); }
  const irr::core::vector3df& spawnPosition() const { return spawnPosition_; }

  void processEntities(irr::scene::IQ3LevelMesh* q3mesh);
  void buildNavMesh();
  void printMeshData();

private:
  typedef std::multimap<std::string, std::unique_ptr<BSPEntity>> BSPEntities;

  std::unique_ptr<EntityFactory> entityFactory_;
  BSPEntities entities_;

  std::string name_;
  irr::ILogger* logger_;
  irr::scene::ISceneManager* sceneManager_;
  std::unique_ptr<NavBuilder> navBuilder_;

  boost::intrusive_ptr<irr::scene::IAnimatedMesh> structure_;
  std::unique_ptr<irr::scene::SMesh> walkSurface_;

  ScopedSceneNode<irr::scene::ISceneNode> worldNode_;
  ScopedSceneNode<irr::scene::ISceneNode> walkNode_;
  EntitiesMap entitiesMap_;
  irr::core::vector3df spawnPosition_;
  std::shared_ptr<Critter> player_;

  DISALLOW_COPY_AND_ASSIGN(LocationImpl);
};

#endif // LOCATIONIMPL_HPP

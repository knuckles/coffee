#ifndef GAME_SCREEN_MANAGER_HPP
#define GAME_SCREEN_MANAGER_HPP

#include "base/macros.hpp"

class GameScreen;

namespace irr {
class IrrlichtDevice;
}

class GameScreenManager {
public:
  GameScreenManager();
  ~GameScreenManager();

  GameScreen* activeScreen() const { return activeScreen_; }

  void activateScreen(GameScreen* screen);

private:
  irr::IrrlichtDevice* device_;
  GameScreen* activeScreen_;

  DISALLOW_COPY_AND_ASSIGN(GameScreenManager);
};

#endif  // GAME_SCREEN_MANAGER_HPP

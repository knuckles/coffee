#include "Coffee.hpp"

#include <stdexcept>

#include "game/GameScreenManager.hpp"
#include "game/LocationGameScreen.hpp"

#include "3rdparty/irrlicht/include/irrlicht.h"  // Declares irr::createDevice
#include "3rdparty/irrlicht/include/IrrlichtDevice.h"

using namespace irr;
using namespace irr::core;
using namespace irr::scene;

Coffee* Coffee::getInstance() {
  static Coffee instance;
  return &instance;
}

boost::asio::io_service&Coffee::io() { return *io_service_; }

Coffee::Coffee()
  : config_() {
}

void Coffee::Init() {
  io_service_.reset(new boost::asio::io_service());
  io_work_.reset(new boost::asio::io_service::work(*io_service_));

  device_.reset(createDevice(config_.driver_type(), config_.resolution()));
  if (!device_)
    throw std::runtime_error("Could not create IrrlichtDevice");
  screenManager_.reset(new GameScreenManager());

  const GameConfig::ArchivePathsType& paths = config_.GetArchivePaths();
  for (GameConfig::ArchivePathsType::const_iterator i = paths.begin(); i != paths.end(); ++i) {
     device_->getFileSystem()->addFileArchive(*i);
  }

  locationScreen_.reset(new LocationGameScreen());
  locationScreen_->load(config_.testLocationName());

  screenManager_->activateScreen(locationScreen_.get());
}

Coffee::~Coffee() = default;

int Coffee::Run() {
  video::IVideoDriver* const driver = device_->getVideoDriver();

  int lastFPS = -1;
  while(device_->run()) {
    if (!device_->isWindowActive()) {
      device_->yield();
      continue;
    }

    assert(!io_service_->stopped());
    io_service_->poll();

    driver->beginScene(true, true, config_.bgColor());
    screenManager_->activeScreen()->draw();
    device_->getGUIEnvironment()->drawAll();
    driver->endScene();

    int fps = driver->getFPS();

    if (lastFPS != fps) {
      core::stringw str = L"Coffee [";
      str += driver->getName();
      str += "] FPS:";
      str += fps;

      device_->setWindowCaption(str.c_str());
      lastFPS = fps;
    }
  }

  return 0;
}

#ifndef GAMESCREEN_HPP
#define GAMESCREEN_HPP

#include "base/irrlicht_ptrs.hpp"
#include "base/macros.hpp"

#include "3rdparty/irrlicht/include/IEventReceiver.h"

namespace irr {
class IrrlichtDevice;
namespace scene {
class ICameraSceneNode;
class ISceneManager;
}
}

class GameScreen : public irr::IEventReceiver {
public:
  GameScreen(irr::IrrlichtDevice* device);
  ~GameScreen();

  void draw();

protected:
  irr::IrrlichtDevice* device() const { return device_; }
  irr::scene::ICameraSceneNode* camera() const;
  irr::scene::ISceneManager* scene() const;

  void setCamera(irr::scene::ICameraSceneNode* camera);

private:
  irr::IrrlichtDevice* device_;
  boost::intrusive_ptr<irr::scene::ISceneManager> scene_;
  boost::intrusive_ptr<irr::scene::ICameraSceneNode> camera_;

  DISALLOW_COPY_AND_ASSIGN(GameScreen);
};

#endif  // GAMESCREEN_HPP

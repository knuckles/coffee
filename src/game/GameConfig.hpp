#ifndef GAME_CONFIG_HPP
#define GAME_CONFIG_HPP

#include <string>
#include <vector>

#include "base/macros.hpp"

#include "3rdparty/irrlicht/include/EDriverTypes.h"
#include "3rdparty/irrlicht/include/dimension2d.h"
#include "3rdparty/irrlicht/include/irrTypes.h"
#include "3rdparty/irrlicht/include/path.h"
#include "3rdparty/irrlicht/include/SColor.h"

class GameConfig {
public:
  typedef irr::core::dimension2d<irr::u32> Resolution;

  GameConfig();
  ~GameConfig();

  irr::video::E_DRIVER_TYPE driver_type() const { return driverType_; }
  Resolution resolution() const;
  const irr::video::SColor& bgColor() const { return bgColor_; }

  typedef std::vector<irr::io::path> ArchivePathsType;
  const ArchivePathsType& GetArchivePaths() const { return archivePaths_; }
  const irr::io::path& uiFontPath() const { return uiFontPath_; }
  const irr::u32 uiFontSize() const { return uiFontSize_; }
  const std::string& testLocationName() const { return testLocationName_; }
  const std::string& playerModel() const { return playerModel_; }

private:
  irr::video::E_DRIVER_TYPE driverType_;
  irr::video::SColor bgColor_;
  ArchivePathsType archivePaths_;
  irr::io::path uiFontPath_;
  irr::u32 uiFontSize_;
  std::string testLocationName_;
  std::string playerModel_;

  DISALLOW_COPY_AND_ASSIGN(GameConfig);
};

#endif // GAME_CONFIG_HPP

#include "LocationGameScreen.hpp"

#include "ai/Planner.hpp"
#include "Coffee.hpp"
#include "ent/Critter.hpp"
#include "ent/Item.hpp"
#include "map/LocationImpl.hpp"
#include "scene/InventoryWindow.hpp"
#include "scene/NodeKinds.hpp"
#include "scene/OrbitCameraAnimator.hpp"

#include "3rdparty/irrlicht/include/IAnimatedMeshSceneNode.h"
#include "3rdparty/irrlicht/include/ICameraSceneNode.h"
#include "3rdparty/irrlicht/include/IGUIWindow.h"
#include "3rdparty/irrlicht/include/IrrlichtDevice.h"
#include "3rdparty/irrlicht/include/IMeshSceneNode.h"
#include "3rdparty/irrlicht/include/ISceneCollisionManager.h"
#include "3rdparty/irrlicht/include/ISceneManager.h"

using namespace irr;
using namespace irr::core;
using namespace irr::scene;

namespace {
const unsigned int kTurnDurationMs = 5000;

ISceneNode* pickNode(ISceneManager* scene, const irr::SEvent& event, vector3df& destination) {
  ISceneCollisionManager* collMan = scene->getSceneCollisionManager();
  position2d<s32> clickPos(event.MouseInput.X, event.MouseInput.Y);
  line3d<f32> ray = collMan->getRayFromScreenCoordinates(clickPos, scene->getActiveCamera());
  triangle3df hitTriangle;
  return collMan->getSceneNodeAndCollisionPointFromRay(ray, destination, hitTriangle,
                                                       kNavMeshNode | kEntityNode);
}

class ScopedPlanStarter {
public:
  ScopedPlanStarter(Planner* planner)
    : planner_(planner) {
    assert(planner_);
    planner_->abortPlan();
  }

  ~ScopedPlanStarter() {
    planner_->checkPlan();
  }

private:
  Planner* planner_;

  DISALLOW_COPY_AND_ASSIGN(ScopedPlanStarter);
};
}

LocationGameScreen::LocationGameScreen()
  : GameScreen(Coffee::getInstance()->device()),
    timer_(Coffee::getInstance()->io()),
    turnBasedMode_(false) {
}

LocationGameScreen::~LocationGameScreen() = default;

void LocationGameScreen::load(const std::string& path) {
  location_.reset(new LocationImpl(io::path(path.c_str()), scene()));
  makeScene();
}

bool LocationGameScreen::OnEvent(const SEvent& event) {
  if (event.EventType == EET_KEY_INPUT_EVENT) {
    if (!event.KeyInput.PressedDown) {
      switch (event.KeyInput.Key) {
        case KEY_ESCAPE: {
          Planner* planner = location_->player()->planner();
          if (planner && planner->hasPlan())
            planner->abortPlan();
          else
            device()->closeDevice();
          return true;
        }
        case KEY_TAB:
          toggleTurnBasedMode();
          return true;
        case KEY_SPACE:
          giveTurn();
          return true;
        case KEY_KEY_I:
          inventoryWnd_->show();
          return true;
        default:
          break;
      }
    }
  } else if (event.EventType == EET_MOUSE_INPUT_EVENT) {
    switch (event.MouseInput.Event) {
      case EMIE_LMOUSE_LEFT_UP:
        return handleLMBClick(event);
      default:
        break;
    }
  }

  return scene()->postEventFromUser(event);
}

bool LocationGameScreen::playerTurn() const {
  return worldStopped();
}

void LocationGameScreen::makeScene() {
  location_->makeScene(nullptr);
  buildGUI();
  setCamera(scene()->addCameraSceneNode());
  camera()->addAnimator(new OrbitCameraAnimator(device(), location_->player()->node()));
}

void LocationGameScreen::buildGUI() {
  inventoryWnd_.reset(new InventoryWindow(location_->player()));
}

bool LocationGameScreen::handleLMBClick(const irr::SEvent& event) {
  if (turnBasedMode_ && !playerTurn()) return false;

  vector3df destination;
  ISceneNode* selectedNode = pickNode(scene(), event, destination);
  if (!selectedNode)
    return false;

  auto nodeKind = getNodeKind(selectedNode);
  if (nodeKind == kNavMeshNode) {
    Planner* planner = location_->player()->planner();
    if (!planner)
      return false;
    ScopedPlanStarter planStarter(planner);
    planner->navigate(destination);
    return true;
  }

  Entity* entity = nullptr;
  if (nodeKind != kEntityNode || !(entity = location_->findEntity(selectedNode)))
    return false;

  switch (entity->entityKind()) {
    case Entity::kAbstractEntity:
    case Entity::kObjectEntity:
      return false;
    case Entity::kItemEntity:
      return handleItemClick(*entity);
    case Entity::kCritterEntity:
      return handleCritterClick(*entity);
  }
}

bool LocationGameScreen::handleItemClick(Entity& item) const {
  Planner* planner = location_->player()->planner();
  if (!planner)
    return false;
  ScopedPlanStarter planStarter(planner);
  planner->pick(std::static_pointer_cast<Item>(item.shared_from_this()));
  return true;
}

bool LocationGameScreen::handleCritterClick(Entity& critter) const {
  Planner* planner = location_->player()->planner();
  if (!planner)
    return false;
  ScopedPlanStarter planStarter(planner);
  planner->attack(std::static_pointer_cast<Critter>(critter.shared_from_this()));
  return true;
}

void LocationGameScreen::toggleTurnBasedMode() {
  turnBasedMode_ = ! turnBasedMode_;
  if (turnBasedMode_)
    stopWorld();
  else
    startWorld();
}

void LocationGameScreen::giveTurn() {
  if (!playerTurn())
    return;
  startWorld();
  timer_.expires_from_now(boost::posix_time::milliseconds(kTurnDurationMs));
  timer_.async_wait([this](const boost::system::error_code& error) {
     stopWorld();
  });
}

bool LocationGameScreen::worldStopped() const {
  return device()->getTimer()->isStopped();
}

void LocationGameScreen::startWorld() {
  device()->getTimer()->start();
}

void LocationGameScreen::stopWorld() {
  device()->getTimer()->stop();
}

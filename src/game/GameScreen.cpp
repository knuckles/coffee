#include "GameScreen.hpp"

#include "3rdparty/irrlicht/include/ICameraSceneNode.h"
#include "3rdparty/irrlicht/include/IrrlichtDevice.h"
#include "3rdparty/irrlicht/include/ISceneManager.h"

using namespace irr::scene;

GameScreen::GameScreen(irr::IrrlichtDevice* device)
  : device_(device),
    scene_(device_->getSceneManager()->createNewSceneManager(), false),
    camera_() {
}

GameScreen::~GameScreen() = default;

void GameScreen::draw() {
  scene()->drawAll();
}

ICameraSceneNode* GameScreen::camera() const {
  return camera_.get();
}

ISceneManager* GameScreen::scene() const {
  return scene_.get();
}

void GameScreen::setCamera(ICameraSceneNode* camera) {
  camera_.reset(camera);
}

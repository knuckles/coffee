#ifndef COFFEE_HPP
#define COFFEE_HPP

#include <memory>
#include <boost/asio/io_service.hpp>

#include "base/irrlicht_ptrs.hpp"
#include "base/macros.hpp"
#include "GameConfig.hpp"

class GameScreenManager;
class LocationGameScreen;

namespace irr {
class IrrlichtDevice;
}

class Coffee {
public:
  static Coffee* getInstance();

  const GameConfig* config() const { return &config_; }
  irr::IrrlichtDevice* device() const { return device_.get(); }
  boost::asio::io_service& io();

  void Init();
  int Run();

private:
  Coffee();
  ~Coffee();

  GameConfig config_;
  std::unique_ptr<boost::asio::io_service> io_service_;
  std::unique_ptr<boost::asio::io_service::work> io_work_;  // Prevents |io_service_| from stopping.
  boost::intrusive_ptr<irr::IrrlichtDevice> device_;
  std::unique_ptr<GameScreenManager> screenManager_;
  std::unique_ptr<LocationGameScreen> locationScreen_;

  DISALLOW_COPY_AND_ASSIGN(Coffee);
};

#endif  // COFFEE_HPP

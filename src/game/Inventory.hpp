#ifndef INVENTORY_HPP
#define INVENTORY_HPP

#include <memory>
#include <set>

#include "base/macros.hpp"

class Item;
class Weapon;

class Inventory {
public:
  typedef std::set<std::shared_ptr<Item>> ContainerType;
  typedef ContainerType::const_iterator ConstIterator;

  Inventory();
  ~Inventory();

  ConstIterator begin() const;
  ConstIterator end() const;

  std::shared_ptr<Weapon> getBestWeapon();

  void add(std::shared_ptr<Item> item);
#if 0
  std::unique_ptr<Item> remove(ConstIterator at);
#endif

private:
  ContainerType items_;

  DISALLOW_COPY_AND_ASSIGN(Inventory);
};

#endif  // INVENTORY_HPP

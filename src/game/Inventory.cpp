#include "Inventory.hpp"

#include <algorithm>

#include "ent/Item.hpp"
#include "ent/Weapon.hpp"

namespace {
bool isWeapon (std::shared_ptr<Item> item) {
  return item->itemKind() == Item::kWeaponItem;
}
}

Inventory::Inventory() = default;
Inventory::~Inventory() = default;

Inventory::ConstIterator Inventory::begin() const {
  return items_.begin();
}

Inventory::ConstIterator Inventory::end() const {
  return items_.end();
}

std::shared_ptr<Weapon> Inventory::getBestWeapon() {
  auto iWeapon = std::find_if(begin(), end(), isWeapon);
  return iWeapon != end() ? std::static_pointer_cast<Weapon>(*iWeapon) : std::shared_ptr<Weapon>();
}

void Inventory::add(std::shared_ptr<Item> item) {
  if (item)
    items_.insert(item);
}

#if 0
Item::ScopedItem Inventory::remove(ContainerType::const_iterator at) {
  Item::ScopedItem item((*at).release());
  items_.erase(at);
  return item;
}
#endif

#include "GameScreenManager.hpp"

#include <assert.h>

#include "game/Coffee.hpp"
#include "game/GameScreen.hpp"

#include "3rdparty/irrlicht/include/IrrlichtDevice.h"
#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/ISceneNode.h"

GameScreenManager::GameScreenManager()
  : device_(Coffee::getInstance()->device()),
    activeScreen_(nullptr) {
  assert(device_);
}

GameScreenManager::~GameScreenManager() = default;

void GameScreenManager::activateScreen(GameScreen* screen) {
  if (activeScreen_ == screen)
    return;

  // TODO: update per screen virtual time.
  activeScreen_ = screen;
  device_->setEventReceiver(activeScreen_);
}

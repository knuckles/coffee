#include "GameConfig.hpp"

#include <assert.h>
#include <fstream>

#include "3rdparty/jsoncpp/include/json/json.h"

using namespace irr;
using namespace irr::core;
using namespace irr::video;

GameConfig::GameConfig()
    : driverType_(EDT_NULL),
      bgColor_(255, 200, 200, 200),
      archivePaths_(),
      testLocationName_() {
  std::ifstream configStream("config.json");
  assert(configStream.is_open());

  Json::Reader jsonReader;
  Json::Value root;
  jsonReader.parse(configStream, root);
  assert(root.isObject());

  driverType_ = static_cast<E_DRIVER_TYPE>(root["driverType"].asInt());

  const Json::Value& pathList = root["resourcePaths"];
  if (!pathList.isNull()) {
    Json::ArrayIndex numPaths = pathList.size();
    for (Json::ArrayIndex i = 0; i < numPaths; ++i) {
      archivePaths_.push_back(irr::io::path(pathList[i].asString().c_str()));
    }
  }

  uiFontPath_ = io::path(root["uiFontPath"].asString().c_str());
  uiFontSize_ = root.get("uiFontSize", 12).asUInt();

  testLocationName_ = root["testLocation"].asString();
  playerModel_ = root["playerModel"].asString();
}

GameConfig::~GameConfig() = default;

GameConfig::Resolution GameConfig::resolution() const {
  return Resolution(640, 480);
}





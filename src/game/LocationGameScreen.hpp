#ifndef LOCATIONGAMESCREEN_HPP
#define LOCATIONGAMESCREEN_HPP

#include <memory>

#include <boost/asio/deadline_timer.hpp>

#include "base/irrlicht_ptrs.hpp"
#include "base/macros.hpp"
#include "game/GameScreen.hpp"

class Entity;
class InventoryWindow;
class Location;

class LocationGameScreen : public GameScreen {
public:
  LocationGameScreen();
  ~LocationGameScreen();

  void load(const std::string& path);

private:
  // IEventReceiver implementation
  virtual bool OnEvent(const irr::SEvent& event) override;

  bool playerTurn() const;

  void makeScene();
  void buildGUI();
  bool handleLMBClick(const irr::SEvent& event);
  bool handleItemClick(Entity& item) const;
  bool handleCritterClick(Entity& critter) const;
  void toggleTurnBasedMode();
  void giveTurn();

  bool worldStopped() const;
  void startWorld();
  void stopWorld();

  boost::asio::deadline_timer timer_;
  bool turnBasedMode_;

  // Following members are initialized in |makeScene|.

  std::unique_ptr<Location> location_;
  std::unique_ptr<InventoryWindow> inventoryWnd_;

  DISALLOW_COPY_AND_ASSIGN(LocationGameScreen);
};

#endif  // LOCATIONGAMESCREEN_HPP

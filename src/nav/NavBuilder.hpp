#ifndef NAV_BUILDER_H
#define NAV_BUILDER_H

#include <memory>
#include <vector>

#include "base/irrlicht_ptrs.hpp"
#include "base/macros.hpp"
#include "base/recast_ptrs.hpp"

// Forward declarations.
class Navigator;
struct rcConfig;
class rcContext;

namespace irr {

namespace scene {
class IMesh;
class IMeshBuffer;
class SMesh;
}  // namespace scene

}  // namespace irr

class NavBuilder {
 public:
  NavBuilder();
  virtual ~NavBuilder();

  Navigator* navigator() const { return navigator_.get(); }
  bool buildNavMesh(irr::scene::IMesh* mesh);
  std::unique_ptr<irr::scene::SMesh> getNavMesh();

 private:
  void resetCommonSettings();

  std::unique_ptr<rcConfig> rcConfig_;
  std::unique_ptr<rcContext> ctx_;
  ScopedContourSet contourSet_;
  ScopedPolyMesh polyMesh_;
  ScopedPolyMeshDetail navMeshDetail_;

  std::unique_ptr<Navigator> navigator_;

  bool monotonePartitioning_;
  float agentHeight_;
  float agentRadius_;
  float agentMaxClimb_;


  DISALLOW_COPY_AND_ASSIGN(NavBuilder);
};

#endif  // NAV_BUILDER

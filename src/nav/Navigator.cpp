#include "Navigator.hpp"

#include <cstring>
#include <stdexcept>

#include "3rdparty/RecastNavigation/Detour/Include/DetourNavMesh.h"
#include "3rdparty/RecastNavigation/Detour/Include/DetourNavMeshBuilder.h"
#include "3rdparty/RecastNavigation/Detour/Include/DetourNavMeshQuery.h"
#include "3rdparty/RecastNavigation/Recast/Include/Recast.h"

using namespace irr;
using namespace irr::core;

namespace {
const int MAX_POLYS = 256;

enum SamplePolyAreas {
  SAMPLE_POLYAREA_GROUND,
  SAMPLE_POLYAREA_WATER,
  SAMPLE_POLYAREA_ROAD,
  SAMPLE_POLYAREA_DOOR,
  SAMPLE_POLYAREA_GRASS,
  SAMPLE_POLYAREA_JUMP,
};

enum SamplePolyFlags {
  SAMPLE_POLYFLAGS_WALK = 0x01,      // Ability to walk (ground, grass, road)
  SAMPLE_POLYFLAGS_SWIM = 0x02,      // Ability to swim (water).
  SAMPLE_POLYFLAGS_DOOR = 0x04,      // Ability to move through doors.
  SAMPLE_POLYFLAGS_JUMP = 0x08,      // Ability to jump.
  SAMPLE_POLYFLAGS_DISABLED = 0x10,  // Disabled polygon
  SAMPLE_POLYFLAGS_ALL = 0xffff
};

#if 0
std::ostream& operator<<(std::ostream& stream, const irr::core::vector3df& point) {
  return stream << "(" << point.X << "," << point.Y << "," << point.Z << ")";
}

std::ostream& operator<<(std::ostream& stream, const Navigator::Path& path) {
  for (auto&& point : path)
    stream << point;
  return stream;
}
#endif

}  // namespace

Navigator::Navigator(rcConfig* config, rcPolyMesh* polyMesh, rcPolyMeshDetail* detailMesh) {
  init(config, polyMesh, detailMesh);
}

Navigator::~Navigator() = default;

Navigator::Path Navigator::route(vector3df from, vector3df to) const {
  std::vector<irr::core::vector3df> path;
  if (!navQuery_ || !navMesh_)
    return path;

  dtPolyRef pathBuffer[MAX_POLYS];
  float straightPath[MAX_POLYS * 3];
  float startCoords[3] = {from.X, from.Y, from.Z};
  float endCoords[3] = {to.X, to.Y, to.Z};
  float polyPickExt[3] = {2, 4, 2};

  dtQueryFilter filter;
  dtPolyRef startRef;
  dtPolyRef endRef;

  navQuery_->findNearestPoly(startCoords, polyPickExt, &filter, &startRef, 0);
  if (startRef == 0)
    return path;

  navQuery_->findNearestPoly(endCoords, polyPickExt, &filter, &endRef, 0);
  if (endRef == 0)
    return path;

  dtStatus findStatus = DT_FAILURE;
  int pathCount = 0;
  findStatus = navQuery_->findPath(startRef, endRef, startCoords, endCoords, &filter, pathBuffer,
                                   &pathCount, MAX_POLYS);
  if (pathCount <= 0)
    return path;

  int numPoints = 0;
  findStatus =  navQuery_->findStraightPath(startCoords, endCoords, pathBuffer, pathCount,
      straightPath, 0, 0, &numPoints, MAX_POLYS);

  for (int i = 0; i < numPoints; ++i) {
    path.push_back(
        vector3df(straightPath[i * 3],
                  straightPath[i * 3 + 1] + 0.25,
                  straightPath[i * 3 + 2]));
  }
  return path;
}

void Navigator::init(rcConfig* config, rcPolyMesh* polyMesh, rcPolyMeshDetail* detailMesh) {
  // The GUI may allow more max points per polygon than Detour can handle.
  // Only build the detour navmesh if we do not exceed the limit.
  if (config->maxVertsPerPoly > DT_VERTS_PER_POLYGON)
    throw std::range_error("Too much vertices per polygon!");

  // Update poly flags from areas.
  for (int i = 0; i < polyMesh->npolys; ++i) {
    if (polyMesh->areas[i] == RC_WALKABLE_AREA)
      polyMesh->areas[i] = SAMPLE_POLYAREA_GROUND;

    if (polyMesh->areas[i] == SAMPLE_POLYAREA_GROUND ||
        polyMesh->areas[i] == SAMPLE_POLYAREA_GRASS ||
        polyMesh->areas[i] == SAMPLE_POLYAREA_ROAD) {
      polyMesh->flags[i] = SAMPLE_POLYFLAGS_WALK;
    } else if (polyMesh->areas[i] == SAMPLE_POLYAREA_WATER) {
      polyMesh->flags[i] = SAMPLE_POLYFLAGS_SWIM;
    } else if (polyMesh->areas[i] == SAMPLE_POLYAREA_DOOR) {
      polyMesh->flags[i] = SAMPLE_POLYFLAGS_WALK | SAMPLE_POLYFLAGS_DOOR;
    }
  }

  dtNavMeshCreateParams params;
  memset(&params, 0, sizeof(params));
  params.verts = polyMesh->verts;
  params.vertCount = polyMesh->nverts;
  params.polys = polyMesh->polys;
  params.polyAreas = polyMesh->areas;
  params.polyFlags = polyMesh->flags;
  params.polyCount = polyMesh->npolys;
  params.nvp = polyMesh->nvp;
  params.detailMeshes = detailMesh->meshes;
  params.detailVerts = detailMesh->verts;
  params.detailVertsCount = detailMesh->nverts;
  params.detailTris = detailMesh->tris;
  params.detailTriCount = detailMesh->ntris;
#if 0
  params.offMeshConVerts = m_geom->getOffMeshConnectionVerts();
  params.offMeshConRad = m_geom->getOffMeshConnectionRads();
  params.offMeshConDir = m_geom->getOffMeshConnectionDirs();
  params.offMeshConAreas = m_geom->getOffMeshConnectionAreas();
  params.offMeshConFlags = m_geom->getOffMeshConnectionFlags();
  params.offMeshConUserID = m_geom->getOffMeshConnectionId();
  params.offMeshConCount = m_geom->getOffMeshConnectionCount();
#endif
  params.offMeshConAreas = 0;
  params.offMeshConCount = 0;
  params.offMeshConDir = 0;
  params.offMeshConFlags = 0;
  params.offMeshConRad = 0;
  params.offMeshConUserID = 0;
  params.offMeshConVerts = 0;

  params.walkableHeight = config->walkableHeight * config->ch;
  params.walkableRadius = config->walkableRadius * config->cs;
  params.walkableClimb = config->walkableClimb * config->ch;
  rcVcopy(params.bmin, polyMesh->bmin);
  rcVcopy(params.bmax, polyMesh->bmax);
  params.cs = config->cs;
  params.ch = config->ch;
  params.buildBvTree = true;

  std::unique_ptr<unsigned char*, ReleaseDetourObject> navData(new unsigned char*);  //leak
  int navDataSize = 0;

  if (!dtCreateNavMeshData(&params, &*navData, &navDataSize))
    throw std::runtime_error("Could not build Detour navmesh.");

  dtStatus status;
  navMesh_.reset(dtAllocNavMesh());
  if (!navMesh_)
    throw std::runtime_error("Could not alloc Detour NavMesh");
  status = navMesh_->init(*navData, navDataSize, DT_TILE_FREE_DATA);
  if (dtStatusFailed(status))
    throw std::runtime_error("Could not init Detour NavMesh");

  navQuery_.reset(dtAllocNavMeshQuery());
  status = navQuery_->init(navMesh_.get(), 2048);
  if (dtStatusFailed(status))
    throw std::runtime_error("Could not init Detour NavMeshQuery");
}

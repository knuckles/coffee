#ifndef NAVIGATOR_HPP
#define NAVIGATOR_HPP

#include <vector>

#include "base/macros.hpp"
#include "base/recast_ptrs.hpp"

#include "3rdparty/irrlicht/include/vector3d.h"

struct rcConfig;

class Navigator {
public:
  typedef std::vector<irr::core::vector3df> Path;

  Navigator(rcConfig* config, rcPolyMesh* polyMesh, rcPolyMeshDetail* detailMesh);
  ~Navigator();

  Path route(irr::core::vector3df from, irr::core::vector3df to) const;

private:
  void init(rcConfig* config, rcPolyMesh* polyMesh, rcPolyMeshDetail* detailMesh);

  ScopedNavMesh navMesh_;
  ScopedNavMeshQuery navQuery_;

  Navigator() {}
  DISALLOW_COPY_AND_ASSIGN(Navigator);
};

#endif // NAVIGATOR_HPP

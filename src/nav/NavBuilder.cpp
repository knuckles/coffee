#include "NavBuilder.hpp"

#include "base/irrlicht_ptrs.hpp"
#include "base/recast_ptrs.hpp"
#include "Navigator.hpp"

#include "3rdparty/irrlicht/include/IMeshBuffer.h"
#include "3rdparty/irrlicht/include/SMesh.h"
#include "3rdparty/irrlicht/include/SMeshBuffer.h"

#include "3rdparty/RecastNavigation/Recast/Include/Recast.h"

using namespace std;
using namespace irr;
using namespace irr::core;

namespace {

bool flattenMeshVertexCoordinates(const scene::IMeshBuffer& buffer, std::vector<float>& verts) {
  int nverts = buffer.getVertexCount();
  if (nverts <= 0)
    return false;

  verts.reserve(nverts * 3);
  for (u32 i = 0; i < nverts; ++i) {
    core::vector3df pos = buffer.getPosition(i);
    verts.push_back(pos.X);
    verts.push_back(pos.Y);
    verts.push_back(pos.Z);
  }

  return true;
}

bool processMeshBuffer(rcContext& ctx, const rcConfig& config,
                       const scene::IMeshBuffer& buffer,
                       rcHeightfield* heightField) {
  std::vector<float> vertices;
  if (!flattenMeshVertexCoordinates(buffer, vertices))
    return false;
  int triangles_count = buffer.getIndexCount() / 3;
  std::vector<unsigned char> areaTypes;
  areaTypes.reserve(triangles_count);
  rcMarkWalkableTriangles(vertices.data(), buffer.getIndices(), triangles_count,
                          config.walkableSlopeAngle, areaTypes.data());
  rcRasterizeTriangles(&ctx, vertices.data(), vertices.size() / 3,
                       buffer.getIndices(), areaTypes.data(),
                       triangles_count, *heightField,
                       config.walkableClimb);
  areaTypes.resize(0);
  vertices.resize(0);
  return true;
}

}  // namespace

NavBuilder::NavBuilder()
    : ctx_(new rcContext()),
      contourSet_(),
      polyMesh_(),
      navMeshDetail_(),
      monotonePartitioning_(true),
      agentHeight_(2.0f),
      agentRadius_(0.6f),
      agentMaxClimb_(0.9f){
  resetCommonSettings();
}

NavBuilder::~NavBuilder() {
}

void NavBuilder::resetCommonSettings() {
  rcConfig_.reset(new rcConfig);
  rcConfig_->cs = 2.5f;
  rcConfig_->ch = 1.5f;
  rcConfig_->walkableSlopeAngle = 45.0f;
  rcConfig_->walkableHeight = ceilf(agentHeight_ / rcConfig_->ch);
  rcConfig_->walkableClimb = floorf(agentMaxClimb_ / rcConfig_->ch);
  rcConfig_->walkableRadius = ceilf(agentRadius_ / rcConfig_->cs);
  rcConfig_->maxEdgeLen = (30.0f / rcConfig_->cs);
  rcConfig_->maxSimplificationError = 1.3f;
  rcConfig_->minRegionArea = 64;     // 8 * 8
  rcConfig_->mergeRegionArea = 100;  // 20 * 20
  rcConfig_->maxVertsPerPoly = 6;
  rcConfig_->detailSampleDist = 6 * rcConfig_->cs;
  rcConfig_->detailSampleMaxError = 0.2f * rcConfig_->ch;
}

bool NavBuilder::buildNavMesh(scene::IMesh* mesh) {
  // Step 1. Initialize build config.
  //
  // Set the area where the navigation will be build.
  // Here the bounds of the input mesh are used, but the
  // area could be specified by an user defined box, etc.
  const aabbox3df& boundingBox = mesh->getBoundingBox();
  boundingBox.MinEdge.getAs3Values(rcConfig_->bmin);
  boundingBox.MaxEdge.getAs3Values(rcConfig_->bmax);
  rcCalcGridSize(rcConfig_->bmin, rcConfig_->bmax, rcConfig_->cs,
                 &rcConfig_->width, &rcConfig_->height);

  // Reset build times gathering.
  ctx_->resetTimers();
  ctx_->startTimer(RC_TIMER_TOTAL);

  ctx_->log(RC_LOG_PROGRESS, "Building navigation:");
  ctx_->log(RC_LOG_PROGRESS, " - %d x %d cells", rcConfig_->width, rcConfig_->height);
//  ctx_->log(RC_LOG_PROGRESS, " - %.1fK verts, %.1fK tris",
//             buffer->getVertexCount() / 1000.0f, buffer->getIndexCount() / 3 / 1000.0f);

  //
  // Step 2. Rasterize input polygon soup.
  //
  ScopedHeightfield heightField(rcAllocHeightfield());
  if (!heightField) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Out of memory 'solid'.");
    return false;
  }
  if (!rcCreateHeightfield(ctx_.get(), *heightField, rcConfig_->width, rcConfig_->height,
                           rcConfig_->bmin, rcConfig_->bmax,
                           rcConfig_->cs, rcConfig_->ch)) {
    ctx_->log(RC_LOG_ERROR,
               "buildNavigation: Could not create solid heightfield.");
    return false;
  }

  // Find triangles which are walkable based on their slope and rasterize them.
  // If your input data is multiple meshes, you can transform them here,
  // calculate the area type for each of the meshes and rasterize them.
  for (int i = 0; i < mesh->getMeshBufferCount(); ++i) {
    processMeshBuffer(*ctx_, *rcConfig_, *mesh->getMeshBuffer(i), heightField.get());
  }

  //
  // Step 3. Filter walkables surfaces.
  //
  // Once all geoemtry is rasterized, we do initial pass of filtering to
  // remove unwanted overhangs caused by the conservative rasterization
  // as well as filter spans where the character cannot possibly stand.
  rcFilterLowHangingWalkableObstacles(ctx_.get(), rcConfig_->walkableClimb, *heightField);
  rcFilterLedgeSpans(ctx_.get(), rcConfig_->walkableHeight, rcConfig_->walkableClimb, *heightField);
  rcFilterWalkableLowHeightSpans(ctx_.get(), rcConfig_->walkableHeight, *heightField);

  //
  // Step 4. Partition walkable surface to simple regions.
  //
  // Compact the heightfield so that it is faster to handle from now on.
  // This will result more cache coherent data as well as the neighbours
  // between walkable cells will be calculated.
  ScopedCompactHeightfield compactHeightfield(rcAllocCompactHeightfield());
  if (!compactHeightfield) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Out of memory 'chf'.");
    return false;
  }
  if (!rcBuildCompactHeightfield(ctx_.get(), rcConfig_->walkableHeight,
                                 rcConfig_->walkableClimb, *heightField,
                                 *compactHeightfield)) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Could not build compact data.");
    return false;
  }
  heightField.reset();

  // Erode the walkable area by agent radius.
  if (!rcErodeWalkableArea(ctx_.get(), rcConfig_->walkableRadius, *compactHeightfield)) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Could not erode.");
    return false;
  }

  // (Optional) Mark areas.
#if 0
  const ConvexVolume* vols = m_geom->getConvexVolumes();
  for (int i  = 0; i < m_geom->getConvexVolumeCount(); ++i)
    rcMarkConvexPolyArea(m_ctx.get(), vols[i].verts, vols[i].nverts, vols[i].hmin, vols[i].hmax,
                         vols[i].area, *compactHeightfield);
#endif

  if (monotonePartitioning_) {
    // Partition the walkable surface into simple regions without holes.
    // Monotone partitioning does not need distancefield.
    if (!rcBuildRegionsMonotone(ctx_.get(), *compactHeightfield, 0,
                                rcConfig_->minRegionArea,
                                rcConfig_->mergeRegionArea)) {
      ctx_->log(RC_LOG_ERROR, "buildNavigation: Could not build regions.");
      return false;
    }
  } else {
    // Prepare for region partitioning, by calculating distance field along the
    // walkable surface.
    if (!rcBuildDistanceField(ctx_.get(), *compactHeightfield)) {
      ctx_->log(RC_LOG_ERROR,
                 "buildNavigation: Could not build distance field.");
      return false;
    }

    // Partition the walkable surface into simple regions without holes.
    if (!rcBuildRegions(ctx_.get(), *compactHeightfield, 0, rcConfig_->minRegionArea,
                        rcConfig_->mergeRegionArea)) {
      ctx_->log(RC_LOG_ERROR, "buildNavigation: Could not build regions.");
      return false;
    }
  }

  //
  // Step 5. Trace and simplify region contours.
  //
  contourSet_.reset(rcAllocContourSet());
  if (!contourSet_) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Out of memory 'cset'.");
    return false;
  }
  if (!rcBuildContours(ctx_.get(), *compactHeightfield,
                       rcConfig_->maxSimplificationError,
                       rcConfig_->maxEdgeLen, *contourSet_)) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Could not create contours.");
    return false;
  }
  printf("m_cset->nconts=%i\n", contourSet_->nconts);

  //
  // Step 6. Build polygons mesh from contours.
  //
  polyMesh_.reset(rcAllocPolyMesh());
  if (!polyMesh_) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Out of memory 'pmesh'.");
    return false;
  }
  if (!rcBuildPolyMesh(ctx_.get(), *contourSet_, rcConfig_->maxVertsPerPoly, *polyMesh_)) {
    ctx_->log(RC_LOG_ERROR,
               "buildNavigation: Could not triangulate contours.");
    return false;
  }

  //
  // Step 7. Create detail mesh which allows to access approximate height on
  // each polygon.
  //
  navMeshDetail_.reset(rcAllocPolyMeshDetail());
  if (!navMeshDetail_) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Out of memory 'pmdtl'.");
    return false;
  }
  if (!rcBuildPolyMeshDetail(ctx_.get(), *polyMesh_, *compactHeightfield,
                             rcConfig_->detailSampleDist,
                             rcConfig_->detailSampleMaxError, *navMeshDetail_)) {
    ctx_->log(RC_LOG_ERROR, "buildNavigation: Could not build detail mesh.");
    return false;
  }

  ctx_->stopTimer(RC_TIMER_TOTAL);

  // Show performance stats.
  ctx_->log(RC_LOG_PROGRESS,
            "Polymesh: %d vertices  %d polygons",
            polyMesh_->nverts, polyMesh_->npolys);
  ctx_->log(RC_LOG_PROGRESS,
            "Total build time: %d ms",
            ctx_->getAccumulatedTime(RC_TIMER_TOTAL) / 1000.0f);

  navigator_.reset(new Navigator(rcConfig_.get(), polyMesh_.get(), navMeshDetail_.get()));

  return true;
}

std::unique_ptr<scene::SMesh> polyMeshDetailToIrrlichMesh(rcPolyMeshDetail* meshDetail) {
  if (!meshDetail || !meshDetail->nmeshes)
    return std::unique_ptr<scene::SMesh>();

  std::unique_ptr<scene::SMesh> mesh(new scene::SMesh());
  mesh->setHardwareMappingHint(scene::EHM_STATIC, scene::EBT_VERTEX_AND_INDEX);
  scene::SMeshBuffer* buffer = nullptr;
  mesh->addMeshBuffer(buffer = new scene::SMeshBuffer());

  // Copy vertices.
  buffer->Vertices.set_used(meshDetail->nverts);
  for (int i = 0; i < meshDetail->nverts; ++i) {
    const float* vCoords = &meshDetail->verts[i * 3];
    buffer->Vertices[i] =
        video::S3DVertex(vCoords[0], vCoords[1], vCoords[2],  // Position
                         0.0f, 1.0f, 0.0f,  // Normal
                         0x80FF0000,  // Color
                         0.0f, 0.0f);  // Texture coordinates
  }

  unsigned int triOffset = 0;
  for (int m = 0; m < meshDetail->nmeshes; ++m) {
    unsigned int* meshData = &(meshDetail->meshes[m * 4]);
    //const unsigned int firstVertIdx = meshData[0];
    const unsigned int numVerts = meshData[1];
    const unsigned int firstTriIdx = meshData[2];
    const unsigned int numTris = meshData[3];
    //const float* verts = &(meshDetail->verts[firstVertIdx * 3]);
    const unsigned char* tris = &(meshDetail->tris[firstTriIdx * 4]);
    // printf("meshnum=%i, bverts=%i, btris=%i\n", p, bverts, btris);

    int bufferOffset = buffer->Indices.size();
    buffer->Indices.set_used(bufferOffset + numTris * 3);

    for (int t = 0; t < numTris; ++t) {
      for (int c = 0; c < 3; ++c) {
        buffer->Indices[bufferOffset + t * 3 + c] = tris[t * 4 + c] + triOffset;
      }
    }
    triOffset += numVerts;
  }

  video::SMaterial& material = buffer->getMaterial();
  material.MaterialType = video::EMT_TRANSPARENT_ALPHA_CHANNEL;
  material.Lighting = false;
  material.ColorMask = video::ECP_NONE;
#if 0
  material.BackfaceCulling = false;
  material.Wireframe = true;
  material.Thickness = 2.0f;
#endif

  buffer->recalculateBoundingBox();

  // this is important:
  // if not done, scene may disappear sometimes!
  for (u32 n = 0; n < buffer->getVertexCount(); ++n) {
    buffer->BoundingBox.addInternalPoint(buffer->Vertices[n].Pos);
  }

  mesh->recalculateBoundingBox();

  return mesh;
}

std::unique_ptr<scene::SMesh> NavBuilder::getNavMesh() {
  return polyMeshDetailToIrrlichMesh(navMeshDetail_.get());
}

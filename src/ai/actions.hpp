#ifndef ACTIONS_HPP
#define ACTIONS_HPP

#include <functional>
#include <memory>

#include <boost/asio/deadline_timer.hpp>

#include "base/macros.hpp"
#include "3rdparty/irrlicht/include/vector3d.h"

class Critter;
class Item;

class Action : public std::enable_shared_from_this<Action> {
public:
  typedef std::function<void(Action*, bool)> ActionCallback;

  Action(Critter* critter, ActionCallback callback);
  virtual ~Action();
  virtual bool start() = 0;

protected:
  template <typename T = Action>
  std::weak_ptr<T> getWeakThis() {
    return std::weak_ptr<T>(std::static_pointer_cast<T>(shared_from_this()));
  }
  std::function<void(bool)> getWeakComplete();

  void complete(bool success);

  Critter* critter_;
  ActionCallback callback_;
  bool complete_;

private:

  DISALLOW_COPY_AND_ASSIGN(Action);
};

class IdleAction : public Action {
public:
  IdleAction(Critter* critter, float seconds, ActionCallback callback);
  ~IdleAction();
  // Action
  virtual bool start() override;

private:
  void TimerFired(const boost::system::error_code& error);

  float duration_;
  boost::asio::deadline_timer timer_;
};

class NavAction : public Action {
public:
  NavAction(Critter* critter, const irr::core::vector3df& destination, ActionCallback callback);
  ~NavAction();
  // Action
  virtual bool start() override;

private:
  const irr::core::vector3df destination_;
};

class PickAction : public Action {
public:
  PickAction(Critter* critter, std::weak_ptr<Item> item, ActionCallback callback);
  ~PickAction();
  // Action
  virtual bool start() override;

private:
  std::weak_ptr<Item> item_;
};

class AttackAction : public Action {
public:
  AttackAction(Critter* critter, std::weak_ptr<Critter> target, ActionCallback callback);
  ~AttackAction();

  // Action
  virtual bool start() override;

private:
  std::weak_ptr<Critter> target_;
};

#endif // ACTIONS_HPP

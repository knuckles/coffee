#include "actions.hpp"

#include "base/weak_bind.hpp"
#include "ent/Critter.hpp"
#include "ent/entity_descriptions.hpp"
#include "ent/Weapon.hpp"
#include "game/Coffee.hpp"
#include "game/Inventory.hpp"
#include "map/Location.hpp"

using namespace irr::core;
using namespace irr::scene;

// Action

Action::Action(Critter* critter, ActionCallback callback)
  : critter_(critter),
    callback_(callback),
    complete_(false) {
  assert(critter_);
}

Action::~Action() {
}

std::function<void (bool)> Action::getWeakComplete() {
  return weak_bind(getWeakThis(), &Action::complete, this , std::placeholders::_1);
}

void Action::complete(bool success) {
  complete_ = true;
  if (!callback_) return;
  ActionCallback callback;
  std::swap(callback, callback_);
  callback(this, success);
}

// IdleAction

IdleAction::IdleAction(Critter* critter, float seconds, ActionCallback callback)
  : Action(critter, callback),
    duration_(seconds * 1000),
    timer_(Coffee::getInstance()->io()) {
}

bool IdleAction::start() {
  timer_.expires_from_now(boost::posix_time::milliseconds(duration_));
  timer_.async_wait(std::bind(&IdleAction::TimerFired, this, std::placeholders::_1 ));
  return true;
}

void IdleAction::TimerFired(const boost::system::error_code& error) {
  complete(true);
}

IdleAction::~IdleAction() = default;

// NavAction

NavAction::NavAction(Critter* critter, const vector3df& destination, ActionCallback callback)
  : Action(critter, callback),
    destination_(destination) {
}

NavAction::~NavAction() {
  if (!complete_)
    critter_->stopMoving();
}

bool NavAction::start() {
  return critter_->moveTo(destination_, getWeakComplete());
}

// PickAction

PickAction::PickAction(Critter* critter, std::weak_ptr<Item> item, ActionCallback callback)
  : Action(critter, callback),
    item_(item) {
}

PickAction::~PickAction() {
  if (!complete_)
    critter_->stopAnimation();
}

bool PickAction::start() {
  auto item = item_.lock();
  if (!item)
    return false;
  if (!critter_->isNear(item->node()->getPosition()))
    return false;

  bool took = critter_->takeItem(item);
  if (took)
    item->location()->removeEntity(item->node());
  complete(took);
  return true;
}

// AttackAction

namespace {

const char kAnimationWeaponRaise[] = "raise ";
const char kAnimationWeaponShoot[] = "shoot ";
const char kAnimationWeaponLower[] = "lower ";

std::string raiseWeaponAction(const Weapon* weapon) {
  return std::string(kAnimationWeaponRaise) + weapon->weaponDescription().weaponClass;
}

std::string shootWeaponAction(const Weapon* weapon) {
  return std::string(kAnimationWeaponShoot) + weapon->weaponDescription().weaponClass;
}

std::string lowerWeaponAction(const Weapon* weapon) {
  return std::string(kAnimationWeaponLower) + weapon->weaponDescription().weaponClass;
}

}  // namespace

AttackAction::AttackAction(Critter* critter, std::weak_ptr<Critter> target, ActionCallback callback)
  : Action(critter, callback),
    target_(target) {
}

AttackAction::~AttackAction() = default;

bool AttackAction::start() {
  auto target = target_.lock();
  if (!target || target.get() == critter_)
    return false;
  const Weapon* curr_weapon = critter_->weapon();
  if (!curr_weapon) {
    auto weapon = critter_->inventory()->getBestWeapon();
    if (!weapon)
      return false;
    critter_->wield(weapon);
  }
  curr_weapon = critter_->weapon();
  if (!curr_weapon)
    return false;

  critter_->turnTo(target->node()->getPosition());
  auto damage = curr_weapon->weaponDescription().damage;
  critter_->chainAnimations({
      raiseWeaponAction(curr_weapon),
      shootWeaponAction(curr_weapon),
      lowerWeaponAction(curr_weapon)},
      getWeakComplete(),
      weak_bind(getWeakThis<AttackAction>(), [this, damage](int animIndex) {
    if (animIndex == 1) {  // weapon was shot
      auto target = target_.lock();
      if (target)
        target->takeDamage(damage, critter_);
    }
  }, std::placeholders::_1));

  return true;
}

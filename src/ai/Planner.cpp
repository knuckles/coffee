#include "Planner.hpp"

#include <assert.h>

#include "ai/actions.hpp"
#include "ent/Critter.hpp"
#include "ent/Item.hpp"

Planner::Planner(Critter* critter)
  : actionCallback_(
        std::bind(&Planner::onActionComplete, this, std::placeholders::_1, std::placeholders::_2)),
    critter_(critter) {
  assert(critter_);
}

Planner::~Planner() = default;

boost::signals2::connection Planner::connectPlanExhausted(PlanExhaustedSignal::slot_type slot) {
  return planExhausted_.connect(slot);
}

bool Planner::hasPlan() const {
  return !plan_.empty();
}

void Planner::abortPlan() {
  PlanType().swap(plan_);
  planExhausted_(false);
}

void Planner::checkPlan() {
  if (hasPlan())
    checkNextAction(false);
}

// ACTIONS.

Planner& Planner::idle(float seconds) {
  addToPlan(new IdleAction(critter_, seconds, actionCallback_));
  return *this;
}

Planner& Planner::navigate(const irr::core::vector3df& destination) {
  addToPlan(new NavAction(critter_, destination, actionCallback_));
  return *this;
}

Planner& Planner::pick(std::weak_ptr<Item> item) {
  auto strongItem = item.lock();
  assert(strongItem);
  if (!critter_->isNear(strongItem->node()->getPosition()))
    navigate(strongItem->node()->getPosition());
  addToPlan(new PickAction(critter_, item, actionCallback_));
  return *this;
}

Planner& Planner::attack(std::weak_ptr<Critter> target) {
  addToPlan(new AttackAction(critter_, target, actionCallback_));
  return *this;
}

// PRIVATE.

void Planner::onActionComplete(Action* action, bool success) {
  if (hasPlan())  // If not, maybe the plan was aborted.
    plan_.pop();  // Remove finished action from queue.
  checkNextAction(success);
}

void Planner::addToPlan(Action* action) {
  assert(action);
  plan_.push(std::shared_ptr<Action>(action));
}

void Planner::checkNextAction(bool lastActionSucceeded) {
  // Try to start next action or throw it away.
  while (hasPlan() && !plan_.front()->start())
    plan_.pop();
  if (!hasPlan())
    planExhausted_(lastActionSucceeded);
}

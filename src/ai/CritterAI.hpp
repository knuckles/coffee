#ifndef CRITTERAI_HPP
#define CRITTERAI_HPP

#include <memory>
#include <set>

#include <boost/signals2/connection.hpp>

#include "base/irrlicht_ptrs.hpp"

class Critter;
class PointsSceneNode;

namespace irr {
class IrrlichtDevice;
}

class CritterAI {
public:
  CritterAI();
  ~CritterAI();

  void setCritter(std::shared_ptr<Critter> critter);

  void makePlan();

  typedef std::set<std::weak_ptr<Critter>, std::owner_less<std::weak_ptr<Critter>>> Hostiles;

private:
  bool bored() const;

  void decide();
  void onCritterDamaged(Critter* critter, Critter* attacker);
  void cleanHostiles();

  int idleSince_;
  std::shared_ptr<Critter> critter_;
  irr::IrrlichtDevice* device_;

  ScopedSceneNode<PointsSceneNode> debugNode_;

  boost::signals2::scoped_connection onNoPlanConnection_;
  boost::signals2::scoped_connection onDamageConnection_;

  Hostiles hostileCritters_;
};

#endif // CRITTERAI_HPP

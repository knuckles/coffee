#ifndef PLANNER_HPP
#define PLANNER_HPP

#include <queue>

#include <functional>
#include <memory>

#include <boost/signals2/signal.hpp>

#include "base/macros.hpp"
#include "3rdparty/irrlicht/include/vector3d.h"

class Action;
class Critter;
class Item;

class Planner {
public:
  explicit Planner(Critter* critter);
  ~Planner();

  typedef boost::signals2::signal<void(bool)> PlanExhaustedSignal;
  boost::signals2::connection connectPlanExhausted(PlanExhaustedSignal::slot_type slot);

  bool hasPlan() const;
  void abortPlan();  // Stop doing, what's in progress and clear plan.
  void checkPlan();  // Proceed with the plan, if any.

  Planner& idle(float seconds);
  Planner& navigate(const irr::core::vector3df& destination);
  Planner& pick(std::weak_ptr<Item> item);
  Planner& attack(std::weak_ptr<Critter> target);

private:
  typedef std::queue<std::shared_ptr<Action>> PlanType;
  typedef std::function<void(Action*, bool)> ActionCallback;

  void onActionComplete(Action* action, bool success);

  void addToPlan(Action* action);
  void checkNextAction(bool lastActionSucceeded);

  ActionCallback actionCallback_;
  Critter* critter_;
  PlanExhaustedSignal planExhausted_;
  PlanType plan_;  // Plan must be destroyed as early as possible.

  DISALLOW_COPY_AND_ASSIGN(Planner);
};

#endif // PLANNER_HPP

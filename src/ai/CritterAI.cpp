#include "CritterAI.hpp"

#include <assert.h>
#include <list>
#include <set>
#include <stdio.h>
#include <utility>

#include "ai/Planner.hpp"
#include "base/constants.h"
#include "ent/Critter.hpp"
#include "ent/entity_descriptions.hpp"
#include "game/Coffee.hpp"
#include "map/Location.hpp"
#include "nav/Navigator.hpp"
#include "scene/NodeKinds.hpp"
#include "scene/PointsSceneNode.hpp"

#include "3rdparty/irrlicht/include/position2d.h"
#include "3rdparty/irrlicht/include/IRandomizer.h"
#include "3rdparty/irrlicht/include/IrrlichtDevice.h"
#include "3rdparty/irrlicht/include/irrMath.h"
#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "3rdparty/irrlicht/include/ISceneCollisionManager.h"
#include "3rdparty/MathGeoLib/src/Geometry/Circle.h"
#include "3rdparty/MathGeoLib/src/Math/MathConstants.h"

using namespace irr;
using namespace irr::core;
using namespace irr::scene;

namespace {
const float kMaxWanderingDistance = 1.5 * kUnitsPerMeter;
const int kMinIdleTimeSec = 1;  // Rest at least 1 second after doing something.
const int kMaxIdleTimeSec = 5;  // Get bored after 5 seconds.
static_assert(kMaxIdleTimeSec > kMinIdleTimeSec, "kMaxIdleTimeSec > kMinIdleTimeSec");

irr::core::vector3df randomPointInRadius(irr::IRandomizer* randomizer, const irr::f32 radius) {
  irr::core::vector3df result;
  result.X = radius * randomizer->frand();
  result.rotateXZBy(360 * randomizer->frand());
  return result;
}

// First is the cover quality points (bigger is better). Second is the cover coordinates.
typedef std::pair<int, vector3df> Cover;
// Order Covers by their quality.
struct CoverCmp {
  bool operator()(const Cover& c1, const Cover& c2) {
    return c1.first < c2.first;
  }
};
typedef std::set<Cover, CoverCmp> CoversSet;
typedef std::list<vector3df> Positions;

Positions findCoveredPositions(const Critter& critter, const Critter& hostile,
                               array<vector3df>* debugPoints) {
  // Some constants.
  const auto searchRadius = 2 * kUnitsPerMeter;
  const auto searchStep = 2.0;  // Degrees to step around the threat in search for cover.
  const auto lift = 0.5 * kUnitsPerMeter;

  Positions result;
  auto* collisionManager = hostile.location()->scene()->getSceneCollisionManager();
  auto threatPos = hostile.node()->getAbsolutePosition();
  threatPos.Y += lift;
  Circle searchCircle({threatPos.X, threatPos.Y, threatPos.Z}, {0, 1, 0}, searchRadius);
  for (float a = 0.0; a < 360; a += searchStep) {
    float3 testPoint = searchCircle.GetPoint(pi * a / 180);
    line3d<f32> testRay({testPoint.x, testPoint.y, testPoint.z}, threatPos);
    vector3df hitPoint;
    triangle3df hitTriangle;
    ISceneNode* hitNode = collisionManager->getSceneNodeAndCollisionPointFromRay(
        testRay, hitPoint, hitTriangle, kStructureNode);
    if (!hitNode)
      continue;

    // Step a bit away from the wall.
    hitPoint += hitTriangle.getNormal().normalize() *
        critter.critterDescription().occupyRadius * kUnitsPerMeter;
    // Find position on the ground.
    vector3df position;
    testRay.start = testRay.end = hitPoint;
    testRay.end.Y = -1;  // TODO: this may not be good on uneven terrains!
    hitNode = collisionManager->getSceneNodeAndCollisionPointFromRay(
        testRay, position, hitTriangle, kNavMeshNode);
    if (!hitNode)
      continue;

    result.push_back(position);
    if (debugPoints)
      debugPoints->push_back(position);
  }
  return result;
}

float pathLength(const Navigator::Path& path) {
  if (path.size() < 2)
    return 0;
  float result = 0;
  for (auto first = path.begin(), second = first + 1;
       second != path.end(); ++first, ++second) {
    result += (*second).getDistanceFrom(*first);
  }
  return result;
}

Cover rankCover(const Critter& critter, const CritterAI::Hostiles& hostiles,
    const vector3df& position) {
  const Navigator* navigator = critter.location()->navigator();
  const float distanceToCover = pathLength(
      navigator->route(critter.node()->getAbsolutePosition(), position));
  return {1000 / distanceToCover, position};
}

CoversSet findCovers(const Critter& critter, const CritterAI::Hostiles& hostiles,
                     array<vector3df>* debugPoints) {
  Positions allPositions;
  for (const std::weak_ptr<Critter>& weakHostile : hostiles) {
    std::shared_ptr<Critter> hostile = weakHostile.lock();
    if (!hostile || hostile->dead())
      continue;
    auto&& positions = findCoveredPositions(critter, *hostile, debugPoints);
    allPositions.splice(allPositions.end(), positions);
  }
  CoversSet result;
  std::transform(allPositions.begin(), allPositions.end(), std::inserter(result, result.begin()),
      std::bind(&rankCover, std::cref(critter), std::cref(hostiles), std::placeholders::_1));
  return result;
}

bool critterIsDeadOrGone(const std::weak_ptr<Critter>& critter) {
  return critter.expired() || critter.lock()->dead();
}
}  // namespace

CritterAI::CritterAI()
  : idleSince_(0),
    device_(Coffee::getInstance()->device()) {
}

CritterAI::~CritterAI() = default;

void CritterAI::setCritter(std::shared_ptr<Critter> critter) {
  if (critter == critter_) return;
  onDamageConnection_.disconnect();
  onNoPlanConnection_.disconnect();
  critter_ = critter;
  if (!critter_) return;
  onDamageConnection_ = critter_->connectToDamaged(
      std::bind(&CritterAI::onCritterDamaged, this, std::placeholders::_1, std::placeholders::_2));
  onNoPlanConnection_ = critter_->planner()->connectPlanExhausted(
      std::bind(&CritterAI::makePlan, this));
}

void CritterAI::makePlan() {
  assert(critter_);

  if (!debugNode_) {
    debugNode_.reset(new PointsSceneNode(critter_->node(), critter_->location()->scene()));
  }

  decide();
  critter_->planner()->checkPlan();
}

bool CritterAI::bored() const {
  if (!idleSince_)
    return false;
  const long idle = device_->getTimer()->getTime() - idleSince_;
  return idle > kMaxIdleTimeSec * 1000;
}

void CritterAI::decide() {
  cleanHostiles();
  array<vector3df> debugPoints;
  CoversSet&& covers = findCovers(*critter_, hostileCritters_, &debugPoints);
  if (!hostileCritters_.empty())
    debugNode_->setPoints(debugPoints);
  if (!covers.empty())
    critter_->planner()->navigate(covers.begin()->second);

  if (!hostileCritters_.empty()) {
    std::shared_ptr<Critter> target;
    while (!target && !hostileCritters_.empty()) {
      target = hostileCritters_.begin()->lock();
      assert(target);
      if (!target || target->dead()) {
        hostileCritters_.erase(hostileCritters_.begin());
        target.reset();
      }
    }
    if (target) {
      critter_->planner()->attack(target);
      return;
    }
  }

  if (bored()) {
    critter_->planner()->navigate(
        critter_->node()->getPosition() +
          randomPointInRadius(device_->getRandomizer(), kMaxWanderingDistance));
    idleSince_ = 0;
    return;
  }

  critter_->planner()->idle(
      kMinIdleTimeSec + (kMaxIdleTimeSec - kMinIdleTimeSec) * device_->getRandomizer()->frand());
  if (!idleSince_)
    idleSince_ = device_->getTimer()->getTime();
}

void CritterAI::onCritterDamaged(Critter* critter, Critter* attacker) {
  if (!attacker) return;
  std::weak_ptr<Critter> weak(std::static_pointer_cast<Critter>(attacker->shared_from_this()));
  const bool alreadyHostile = hostileCritters_.find(weak) != hostileCritters_.end();
  if (alreadyHostile) return;
  hostileCritters_.insert(weak);
}

void CritterAI::cleanHostiles() {
  for (auto&& i = hostileCritters_.begin(); i != hostileCritters_.end(); ) {
    if (critterIsDeadOrGone(*i)) {
      i = hostileCritters_.erase(i);
    } else {
      ++i;
    }
  }
}

#include "game/Coffee.hpp"

int main() {
  Coffee* game = Coffee::getInstance();
  game->Init();
  return game->Run();
}


#include "irrlicht_ptrs.hpp"

#include "3rdparty/irrlicht/include/IReferenceCounted.h"
#include "3rdparty/irrlicht/include/ISceneNode.h"

namespace irr {
void intrusive_ptr_add_ref(irr::IReferenceCounted* node) {
  node->grab();
}

void intrusive_ptr_release(irr::IReferenceCounted* node) {
  node->drop();
}
}  // namespace irr

void RemoveSceneNode::operator()(irr::scene::ISceneNode* node) const {
  node->remove();
}

#ifndef MACROS_HPP
#define MACROS_HPP

#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName&); \
    void operator=(const TypeName&)

#endif // MACROS_HPP

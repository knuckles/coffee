#include "IrrlichtStreams.hpp"

#include "3rdparty/irrlicht/include/IReadFile.h"
#include "3rdparty/irrlicht/include/IWriteFile.h"

using namespace irr::io;
using namespace std;

IrrlichtInputStreamBuf::IrrlichtInputStreamBuf(IReadFile* file)
  : file_(file),
    buffer_() {
  assert(file_ != NULL);
}

#if 0
streambuf*IrrlichtInputStreamBuf::setbuf(basic_streambuf::char_type* s, streamsize n) {
  return nullptr;
}
#endif

IrrlichtInputStreamBuf::int_type IrrlichtInputStreamBuf::underflow() {
  if (gptr() && gptr() < egptr())
    return traits_type::to_int_type(*buffer_);

  irr::s32 read = file_->read(&buffer_, 1024);
  if (!read)
    return traits_type::eof();

  setg(buffer_, buffer_, buffer_ + read);
  return traits_type::to_int_type(*buffer_);
}

InputIrrlichtStream::InputIrrlichtStream(IReadFile* file)
  : IrrlichtInputStreamBuf(file),
    istream(this) {
}


IrrlichtOutputStreamBuf::IrrlichtOutputStreamBuf(IWriteFile* file)
  : file_(file) {
}

IrrlichtOutputStreamBuf::int_type IrrlichtOutputStreamBuf::overflow(basic_streambuf::int_type c) {
  if (traits_type::eq_int_type(c, traits_type::eof()))
    return sync() ? traits_type::eof() : traits_type::not_eof(c);
  // TODO: This is totally ineffective. Use buffer_.
  return file_->write(&c, 1) ? traits_type::not_eof(c) : traits_type::eof();
}

int IrrlichtOutputStreamBuf::sync() {
  return 1;
}

OutputIrrlichtStream::OutputIrrlichtStream(IWriteFile* file)
  : IrrlichtOutputStreamBuf(file),
    ostream(this) {
}

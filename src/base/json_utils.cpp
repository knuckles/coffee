#include "json_utils.hpp"

#include "3rdparty/irrlicht/include/SColor.h"
#include "3rdparty/jsoncpp/include/json/value.h"

using namespace irr::core;
using namespace irr::video;

vector3di intPointFromJsonArray(const Json::Value& array) {
  vector3di result;
  if (array.type() != Json::arrayValue)
    return result;
  Json::Value zero(0);
  result.X = array.get(0u, zero).asInt();
  result.Y = array.get(1u, zero).asInt();
  result.Z = array.get(2u, zero).asInt();
  return result;
}

vector3df floatPointFromJsonArray(const Json::Value& array) {
  vector3df result;
  if (array.type() != Json::arrayValue)
    return result;
  Json::Value zero(0.0f);
  result.X = array.get(0u, zero).asFloat();
  result.Y = array.get(1u, zero).asFloat();
  result.Z = array.get(2u, zero).asFloat();
  return result;
}

SColorf fColorFromJsonArray(const Json::Value& array) {
  SColorf result;
  if (array.type() != Json::arrayValue)
    return result;
  Json::Value one(1.0f);
  result.r = array.get(0u, one).asFloat();
  result.g = array.get(1u, one).asFloat();
  result.b = array.get(2u, one).asFloat();
  result.a = array.get(3u, one).asFloat();
  return result;
}

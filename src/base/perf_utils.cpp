#include "perf_utils.hpp"

#if defined(WIN32)
#include <windows.h>

TimeVal getPerfTime() {
  TimeVal count;
  QueryPerformanceCounter((LARGE_INTEGER*)&count);
  return count;
}

int getPerfDeltaTimeUsec(const TimeVal start, const TimeVal end) {
  static TimeVal freq = 0;
  if (freq == 0)
    QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
  TimeVal elapsed = end - start;
  return elapsed * 1000000 / freq;
}

#else
#include <sys/time.h>

TimeVal getPerfTime() {
  timeval now;
  gettimeofday(&now, 0);
  return (TimeVal)now.tv_sec * 1000000L + (TimeVal)now.tv_usec;
}

int getPerfDeltaTimeUsec(const TimeVal start, const TimeVal end) {
  return end - start;
}

#endif

int getPerfTimeTillNow(const TimeVal start) {
  return getPerfDeltaTimeUsec(start, getPerfTime());
}

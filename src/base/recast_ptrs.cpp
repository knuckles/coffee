#include "recast_ptrs.hpp"

#include "3rdparty/RecastNavigation/Detour/Include/DetourAlloc.h"
#include "3rdparty/RecastNavigation/Detour/Include/DetourNavMesh.h"
#include "3rdparty/RecastNavigation/Detour/Include/DetourNavMeshQuery.h"
#include "3rdparty/RecastNavigation/Recast/Include/Recast.h"

void ReleaseDetourObject::operator()(void* ptr) const {
  dtFree(ptr);
}

void ReleaseNavMesh::operator()(dtNavMesh* o) const {
  dtFreeNavMesh(o);
}

void ReleaseNavMeshQuery::operator()(dtNavMeshQuery* o) const {
  dtFreeNavMeshQuery(o);
}

void ReleaseCompactHeightfield::operator()(rcCompactHeightfield* o) const {
  rcFreeCompactHeightfield(o);
}

void ReleaseContourSet::operator()(rcContourSet* o) const {
  rcFreeContourSet(o);
}

void ReleaseHeightfield::operator()(rcHeightfield* o) const {
  rcFreeHeightField(o);
}

void ReleasercPolyMesh::operator()(rcPolyMesh* o) const {
  rcFreePolyMesh(o);
}

void ReleasercPolyMeshDetail::operator()(rcPolyMeshDetail* o) const {
  rcFreePolyMeshDetail(o);
}

#ifndef RECAST_PTRS_HPP
#define RECAST_PTRS_HPP

#include <functional>
#include <memory>

class dtNavMesh;
class dtNavMeshQuery;

struct rcCompactHeightfield;
struct rcContourSet;
struct rcHeightfield;
struct rcPolyMesh;
struct rcPolyMeshDetail;

#if 0
// Deletor "template"
struct Release_X{
  void operator()(X* o) const;
};
#endif

struct ReleaseDetourObject {
  void operator()(void* ptr) const;
};

struct ReleaseNavMesh{
  void operator()(dtNavMesh* o) const;
};

struct ReleaseNavMeshQuery{
  void operator()(dtNavMeshQuery* o) const;
};

struct ReleaseCompactHeightfield {
  void operator()(rcCompactHeightfield* o) const;
};

struct ReleaseContourSet {
  void operator()(rcContourSet* o) const;
};

struct ReleaseHeightfield {
  void operator()(rcHeightfield* o) const;
};

struct ReleasercPolyMesh{
  void operator()(rcPolyMesh* o) const;
};

struct ReleasercPolyMeshDetail{
  void operator()(rcPolyMeshDetail* o) const;
};

typedef std::unique_ptr<dtNavMesh, ReleaseNavMesh> ScopedNavMesh;
typedef std::unique_ptr<dtNavMeshQuery, ReleaseNavMeshQuery> ScopedNavMeshQuery;

typedef std::unique_ptr<rcCompactHeightfield, ReleaseCompactHeightfield> ScopedCompactHeightfield;
typedef std::unique_ptr<rcContourSet, ReleaseContourSet> ScopedContourSet;
typedef std::unique_ptr<rcHeightfield, ReleaseHeightfield> ScopedHeightfield;
typedef std::unique_ptr<rcPolyMesh, ReleasercPolyMesh> ScopedPolyMesh;
typedef std::unique_ptr<rcPolyMeshDetail, ReleasercPolyMeshDetail> ScopedPolyMeshDetail;

#endif // RECAST_PTRS_HPP

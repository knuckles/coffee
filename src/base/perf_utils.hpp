#ifndef PERF_TIMER_H
#define PERF_TIMER_H

#ifdef __GNUC__
#include <stdint.h>
typedef int64_t TimeVal;
#else
typedef __int64 TimeVal;
#endif

TimeVal getPerfTime();
int getPerfDeltaTimeUsec(const TimeVal start, const TimeVal end);
int getPerfTimeTillNow(const TimeVal start);
inline int getPerfTimeTillNowMs(const TimeVal start) {
  return getPerfTimeTillNow(start) / 1000;
}

#endif  // PERF_TIMER_H

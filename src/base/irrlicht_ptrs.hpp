#ifndef IRRLICHT_PTRS_H
#define IRRLICHT_PTRS_H

#include <memory>
#include <boost/intrusive_ptr.hpp>

namespace irr {
class IReferenceCounted;

namespace scene {
class ISceneNode;
}

void intrusive_ptr_add_ref(irr::IReferenceCounted* node);
void intrusive_ptr_release(irr::IReferenceCounted* node);

}  // namespace irr

struct RemoveSceneNode {
  void operator()(irr::scene::ISceneNode* node) const;
};

template<typename T>
class ScopedSceneNode : public std::unique_ptr<T, RemoveSceneNode> {
 public:
  ScopedSceneNode()
    : std::unique_ptr<T, RemoveSceneNode>() {
  }

  explicit ScopedSceneNode(T* object)
    : std::unique_ptr<T, RemoveSceneNode>(object) {
  }
};

#endif  // IRRLICHT_PTRS_H

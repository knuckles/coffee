#ifndef JSON_UTILS_HPP
#define JSON_UTILS_HPP

#include "3rdparty/irrlicht/include/vector3d.h"

namespace irr {
namespace video {
class SColorf;
}
}

namespace Json {
class Value;
}

irr::core::vector3di intPointFromJsonArray(const Json::Value& array);
irr::core::vector3df floatPointFromJsonArray(const Json::Value& array);
irr::video::SColorf fColorFromJsonArray(const Json::Value& array);

#endif // JSON_UTILS_HPP

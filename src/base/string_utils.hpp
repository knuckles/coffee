#ifndef STRING_UTILS_HPP
#define STRING_UTILS_HPP

#include <sstream>
#include <string>
#include <vector>

std::vector<std::string> tokenizeString(const std::string& string);

template <typename T>
T parseString(const std::string& string) {
  std::istringstream buffer(string);
  T result;
  buffer >> result;
  return result;
}

#endif // STRING_UTILS_HPP

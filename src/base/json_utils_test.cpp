#include "json_utils.hpp"

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "3rdparty/jsoncpp/include/json/value.h"

TEST(JsonUtilsTest, Bar) {
  Json::Value array(Json::arrayValue);
  array.append(Json::Value(1.1));
  array.append(Json::Value(2.2));
  array.append(Json::Value(3.3));
  array.append(Json::Value(4.4));
  EXPECT_EQ(irr::core::vector3di(1, 2, 3), intPointFromJsonArray(array));
}

#include "string_utils.hpp"

#include <iterator>
#include <sstream>

std::vector<std::string> tokenizeString(const std::string& string) {
  std::stringstream stream(string);
  std::vector<std::string> result((std::istream_iterator<std::string>(stream)),
                                  (std::istream_iterator<std::string>()));
  return result;
}

#ifndef IRRLICHTSTREAMBUF_HPP
#define IRRLICHTSTREAMBUF_HPP

#include <assert.h>
#include <iostream>
#include <streambuf>

namespace irr {
namespace io {
class IReadFile;
class IWriteFile;
}  // namespace io
}  // namespace irr

// Stream buffers bridging std streams to Irrlicht IO classes.
// Taken from http://stackoverflow.com/questions/1231461/inheriting-stdistream-or-equivalent

class IrrlichtInputStreamBuf : public std::streambuf {
public:
  explicit IrrlichtInputStreamBuf(irr::io::IReadFile* file);

protected:
  // std::streambuf implementation
  int_type underflow() override;

private:
  IrrlichtInputStreamBuf();

  irr::io::IReadFile* file_;
  char buffer_[1024];
};

class InputIrrlichtStream :
    private IrrlichtInputStreamBuf,
    public std::istream {
public:
  explicit InputIrrlichtStream(irr::io::IReadFile* file);
};

class IrrlichtOutputStreamBuf : public std::streambuf {
public:
  explicit IrrlichtOutputStreamBuf(irr::io::IWriteFile* file);

protected:
  // std::streambuf implementation
  int_type overflow(int_type c) override;
  int sync() override;

private:
  IrrlichtOutputStreamBuf();

  irr::io::IWriteFile* file_;
  char buffer_[1024];
};

class OutputIrrlichtStream :
    private IrrlichtOutputStreamBuf,
    public std::ostream {
public:
  explicit OutputIrrlichtStream(irr::io::IWriteFile* file);
};

#endif // IRRLICHTSTREAMBUF_HPP

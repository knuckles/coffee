#ifndef LOCATIONMOCK_HPP
#define LOCATIONMOCK_HPP

#include "map/Location.hpp"

#include "gmock/gmock.h"

class LocationMock : public Location {
public:
  MOCK_CONST_METHOD0(scene, irr::scene::ISceneManager*());
  MOCK_CONST_METHOD0(structure, irr::scene::ISceneNode* const());
  MOCK_CONST_METHOD0(player, Critter*());
  MOCK_CONST_METHOD0(navigator, Navigator*());

  MOCK_METHOD1(makeScene, void(irr::scene::ISceneNode*));
  MOCK_METHOD2(addEntity, void(std::shared_ptr<Entity>, const irr::core::vector3df&));
  MOCK_METHOD1(removeEntity, std::shared_ptr<Entity>(irr::scene::ISceneNode*));
  MOCK_CONST_METHOD1(findEntity, Entity*(const irr::scene::ISceneNode*));
};

#endif // LOCATIONMOCK_HPP

#include "TestSceneManager.hpp"

#include "test/irr/TestMeshSceneNode.hpp"
#include "test/irr/TestAnimatedMeshSceneNode.hpp"

using namespace testing;

TestSceneManager::TestSceneManager() : irr::scene::ISceneManager() {
  ON_CALL(*this, addAnimatedMeshSceneNode(_, _, _, _, _, _, _)).WillByDefault(
      Invoke(this, &TestSceneManager::createTestAnimatedMeshSceneNode));
}

TestSceneManager::~TestSceneManager() {}

irr::scene::IAnimatedMesh* TestSceneManager::getMesh(
    const irr::io::path& filename) {
  return nullptr;
}

irr::scene::IAnimatedMesh* TestSceneManager::getMesh(irr::io::IReadFile* file) {
  return nullptr;
}

irr::scene::IMeshCache* TestSceneManager::getMeshCache() { return nullptr; }

irr::video::IVideoDriver* TestSceneManager::getVideoDriver() { return nullptr; }

irr::gui::IGUIEnvironment* TestSceneManager::getGUIEnvironment() {
  return nullptr;
}

irr::io::IFileSystem* TestSceneManager::getFileSystem() { return nullptr; }

irr::scene::IVolumeLightSceneNode* TestSceneManager::addVolumeLightSceneNode(
    irr::scene::ISceneNode* parent, irr::s32 id, const irr::u32 subdivU,
    const irr::u32 subdivV, const irr::video::SColor foot,
    const irr::video::SColor tail, const irr::core::vector3df& position,
    const irr::core::vector3df& rotation, const irr::core::vector3df& scale) {
  return nullptr;
}

irr::scene::IMeshSceneNode* TestSceneManager::addCubeSceneNode(
    irr::f32 size, irr::scene::ISceneNode* parent, irr::s32 id,
    const irr::core::vector3df& position, const irr::core::vector3df& rotation,
    const irr::core::vector3df& scale) {
  return createTestSceneNode<TestMeshSceneNode>();
}

irr::scene::IMeshSceneNode* TestSceneManager::addSphereSceneNode(
    irr::f32 radius, irr::s32 polyCount, irr::scene::ISceneNode* parent,
    irr::s32 id, const irr::core::vector3df& position,
    const irr::core::vector3df& rotation, const irr::core::vector3df& scale) {
  return createTestSceneNode<TestMeshSceneNode>();
}

irr::scene::IMeshSceneNode* TestSceneManager::addMeshSceneNode(
    irr::scene::IMesh* mesh, irr::scene::ISceneNode* parent, irr::s32 id,
    const irr::core::vector3df& position, const irr::core::vector3df& rotation,
    const irr::core::vector3df& scale, bool alsoAddIfMeshPointerZero) {
  return createTestSceneNode<TestMeshSceneNode>();
}

irr::scene::ISceneNode* TestSceneManager::addWaterSurfaceSceneNode(
    irr::scene::IMesh* mesh, irr::f32 waveHeight, irr::f32 waveSpeed,
    irr::f32 waveLength, irr::scene::ISceneNode* parent, irr::s32 id,
    const irr::core::vector3df& position, const irr::core::vector3df& rotation,
    const irr::core::vector3df& scale) {
  return createTestSceneNode<TestMeshSceneNode>();
}

irr::scene::IMeshSceneNode* TestSceneManager::addOctreeSceneNode(
    irr::scene::IAnimatedMesh* mesh, irr::scene::ISceneNode* parent,
    irr::s32 id, irr::s32 minimalPolysPerNode, bool alsoAddIfMeshPointerZero) {
  return createTestSceneNode<TestMeshSceneNode>();
}

irr::scene::IMeshSceneNode* TestSceneManager::addOctreeSceneNode(
    irr::scene::IMesh* mesh, irr::scene::ISceneNode* parent, irr::s32 id,
    irr::s32 minimalPolysPerNode, bool alsoAddIfMeshPointerZero) {
  return createTestSceneNode<TestMeshSceneNode>();
}

irr::scene::ICameraSceneNode* TestSceneManager::addCameraSceneNode(
    irr::scene::ISceneNode* parent, const irr::core::vector3df& position,
    const irr::core::vector3df& lookat, irr::s32 id, bool makeActive) {
  return nullptr;
}

irr::scene::ICameraSceneNode* TestSceneManager::addCameraSceneNodeMaya(
    irr::scene::ISceneNode* parent, irr::f32 rotateSpeed, irr::f32 zoomSpeed,
    irr::f32 translationSpeed, irr::s32 id, irr::f32 distance,
    bool makeActive) {
  return nullptr;
}

irr::scene::ICameraSceneNode* TestSceneManager::addCameraSceneNodeFPS(
    irr::scene::ISceneNode* parent, irr::f32 rotateSpeed, irr::f32 moveSpeed,
    irr::s32 id, irr::SKeyMap* keyMapArray, irr::s32 keyMapSize,
    bool noVerticalMovement, irr::f32 jumpSpeed, bool invertMouse,
    bool makeActive) {
  return nullptr;
}

irr::scene::ILightSceneNode* TestSceneManager::addLightSceneNode(
    irr::scene::ISceneNode* parent, const irr::core::vector3df& position,
    irr::video::SColorf color, irr::f32 radius, irr::s32 id) {
  return nullptr;
}

irr::scene::IBillboardSceneNode* TestSceneManager::addBillboardSceneNode(
    irr::scene::ISceneNode* parent,
    const irr::core::dimension2d<irr::f32>& size,
    const irr::core::vector3df& position, irr::s32 id,
    irr::video::SColor colorTop, irr::video::SColor colorBottom) {
  return nullptr;
}

irr::scene::ISceneNode* TestSceneManager::addSkyBoxSceneNode(
    irr::video::ITexture* top, irr::video::ITexture* bottom,
    irr::video::ITexture* left, irr::video::ITexture* right,
    irr::video::ITexture* front, irr::video::ITexture* back,
    irr::scene::ISceneNode* parent, irr::s32 id) {
  return nullptr;
}

irr::scene::ISceneNode* TestSceneManager::addSkyDomeSceneNode(
    irr::video::ITexture* texture, irr::u32 horiRes, irr::u32 vertRes,
    irr::f32 texturePercentage, irr::f32 spherePercentage, irr::f32 radius,
    irr::scene::ISceneNode* parent, irr::s32 id) {
  return nullptr;
}

irr::scene::IParticleSystemSceneNode*
TestSceneManager::addParticleSystemSceneNode(
    bool withDefaultEmitter, irr::scene::ISceneNode* parent, irr::s32 id,
    const irr::core::vector3df& position, const irr::core::vector3df& rotation,
    const irr::core::vector3df& scale) {
  return nullptr;
}

irr::scene::ITerrainSceneNode* TestSceneManager::addTerrainSceneNode(
    const irr::io::path& heightMapFileName, irr::scene::ISceneNode* parent,
    irr::s32 id, const irr::core::vector3df& position,
    const irr::core::vector3df& rotation, const irr::core::vector3df& scale,
    irr::video::SColor vertexColor, irr::s32 maxLOD,
    irr::scene::E_TERRAIN_PATCH_SIZE patchSize, irr::s32 smoothFactor,
    bool addAlsoIfHeightmapEmpty) {
  return nullptr;
}

irr::scene::ITerrainSceneNode* TestSceneManager::addTerrainSceneNode(
    irr::io::IReadFile* heightMapFile, irr::scene::ISceneNode* parent,
    irr::s32 id, const irr::core::vector3df& position,
    const irr::core::vector3df& rotation, const irr::core::vector3df& scale,
    irr::video::SColor vertexColor, irr::s32 maxLOD,
    irr::scene::E_TERRAIN_PATCH_SIZE patchSize, irr::s32 smoothFactor,
    bool addAlsoIfHeightmapEmpty) {
  return nullptr;
}

irr::scene::IMeshSceneNode* TestSceneManager::addQuake3SceneNode(
    const irr::scene::IMeshBuffer* meshBuffer,
    const irr::scene::quake3::IShader* shader, irr::scene::ISceneNode* parent,
    irr::s32 id) {
  return nullptr;
}

irr::scene::ISceneNode* TestSceneManager::addEmptySceneNode(
    irr::scene::ISceneNode* parent, irr::s32 id) {
  return nullptr;
}

irr::scene::IDummyTransformationSceneNode*
TestSceneManager::addDummyTransformationSceneNode(
    irr::scene::ISceneNode* parent, irr::s32 id) {
  return nullptr;
}

irr::scene::ITextSceneNode* TestSceneManager::addTextSceneNode(
    irr::gui::IGUIFont* font, const wchar_t* text, irr::video::SColor color,
    irr::scene::ISceneNode* parent, const irr::core::vector3df& position,
    irr::s32 id) {
  return nullptr;
}

irr::scene::IBillboardTextSceneNode*
TestSceneManager::addBillboardTextSceneNode(
    irr::gui::IGUIFont* font, const wchar_t* text,
    irr::scene::ISceneNode* parent,
    const irr::core::dimension2d<irr::f32>& size,
    const irr::core::vector3df& position, irr::s32 id,
    irr::video::SColor colorTop, irr::video::SColor colorBottom) {
  return nullptr;
}

irr::scene::IAnimatedMesh* TestSceneManager::addHillPlaneMesh(
    const irr::io::path& name, const irr::core::dimension2d<irr::f32>& tileSize,
    const irr::core::dimension2d<irr::u32>& tileCount,
    irr::video::SMaterial* material, irr::f32 hillHeight,
    const irr::core::dimension2d<irr::f32>& countHills,
    const irr::core::dimension2d<irr::f32>& textureRepeatCount) {
  return nullptr;
}

irr::scene::IAnimatedMesh* TestSceneManager::addTerrainMesh(
    const irr::io::path& meshname, irr::video::IImage* texture,
    irr::video::IImage* heightmap,
    const irr::core::dimension2d<irr::f32>& stretchSize, irr::f32 maxHeight,
    const irr::core::dimension2d<irr::u32>& defaultVertexBlockSize) {
  return nullptr;
}

irr::scene::IAnimatedMesh* TestSceneManager::addArrowMesh(
    const irr::io::path& name, irr::video::SColor vtxColorCylinder,
    irr::video::SColor vtxColorCone, irr::u32 tesselationCylinder,
    irr::u32 tesselationCone, irr::f32 height, irr::f32 cylinderHeight,
    irr::f32 widthCylinder, irr::f32 widthCone) {
  return nullptr;
}

irr::scene::IAnimatedMesh* TestSceneManager::addSphereMesh(
    const irr::io::path& name, irr::f32 radius, irr::u32 polyCountX,
    irr::u32 polyCountY) {
  return nullptr;
}

irr::scene::IAnimatedMesh* TestSceneManager::addVolumeLightMesh(
    const irr::io::path& name, const irr::u32 SubdivideU,
    const irr::u32 SubdivideV, const irr::video::SColor FootColor,
    const irr::video::SColor TailColor) {
  return nullptr;
}

irr::scene::ISceneNode* TestSceneManager::getRootSceneNode() { return nullptr; }

irr::scene::ISceneNode* TestSceneManager::getSceneNodeFromId(
    irr::s32 id, irr::scene::ISceneNode* start) {
  return nullptr;
}

irr::scene::ISceneNode* TestSceneManager::getSceneNodeFromName(
    const irr::c8* name, irr::scene::ISceneNode* start) {
  return nullptr;
}

irr::scene::ISceneNode* TestSceneManager::getSceneNodeFromType(
    irr::scene::ESCENE_NODE_TYPE type, irr::scene::ISceneNode* start) {
  return nullptr;
}

void TestSceneManager::getSceneNodesFromType(
    irr::scene::ESCENE_NODE_TYPE type,
    irr::core::array<irr::scene::ISceneNode*>& outNodes,
    irr::scene::ISceneNode* start) {}

irr::scene::ICameraSceneNode* TestSceneManager::getActiveCamera() const {
  return nullptr;
}

void TestSceneManager::setActiveCamera(irr::scene::ICameraSceneNode* camera) {}

void TestSceneManager::setShadowColor(irr::video::SColor color) {}

irr::video::SColor TestSceneManager::getShadowColor() const {
  return irr::video::SColor();
}

irr::u32 TestSceneManager::registerNodeForRendering(
    irr::scene::ISceneNode* node, irr::scene::E_SCENE_NODE_RENDER_PASS pass) {
  return 0;
}

void TestSceneManager::drawAll() {}

irr::scene::ISceneNodeAnimator* TestSceneManager::createRotationAnimator(
    const irr::core::vector3df& rotationSpeed) {
  return nullptr;
}

irr::scene::ISceneNodeAnimator* TestSceneManager::createFlyCircleAnimator(
    const irr::core::vector3df& center, irr::f32 radius, irr::f32 speed,
    const irr::core::vector3df& direction, irr::f32 startPosition,
    irr::f32 radiusEllipsoid) {
  return nullptr;
}

irr::scene::ISceneNodeAnimator* TestSceneManager::createFlyStraightAnimator(
    const irr::core::vector3df& startPoint,
    const irr::core::vector3df& endPoint, irr::u32 timeForWay, bool loop,
    bool pingpong) {
  return nullptr;
}

irr::scene::ISceneNodeAnimator* TestSceneManager::createTextureAnimator(
    const irr::core::array<irr::video::ITexture*>& textures,
    irr::s32 timePerFrame, bool loop) {
  return nullptr;
}

irr::scene::ISceneNodeAnimator* TestSceneManager::createDeleteAnimator(
    irr::u32 timeMs) {
  return nullptr;
}

irr::scene::ISceneNodeAnimatorCollisionResponse*
TestSceneManager::createCollisionResponseAnimator(
    irr::scene::ITriangleSelector* world, irr::scene::ISceneNode* sceneNode,
    const irr::core::vector3df& ellipsoidRadius,
    const irr::core::vector3df& gravityPerSecond,
    const irr::core::vector3df& ellipsoidTranslation, irr::f32 slidingValue) {
  return nullptr;
}

irr::scene::ISceneNodeAnimator* TestSceneManager::createFollowSplineAnimator(
    const irr::core::array<irr::core::vector3df>& points, irr::f32 speed,
    irr::f32 tightness, bool loop, bool pingpong) {
  return nullptr;
}

irr::scene::ITriangleSelector* TestSceneManager::createTriangleSelector(
    irr::scene::IMesh* mesh, irr::scene::ISceneNode* node) {
  return nullptr;
}

irr::scene::ITriangleSelector* TestSceneManager::createTriangleSelector(
    irr::scene::IAnimatedMeshSceneNode* node) {
  return nullptr;
}

irr::scene::ITriangleSelector*
TestSceneManager::createTriangleSelectorFromBoundingBox(
    irr::scene::ISceneNode* node) {
  return nullptr;
}

irr::scene::ITriangleSelector* TestSceneManager::createOctreeTriangleSelector(
    irr::scene::IMesh* mesh, irr::scene::ISceneNode* node,
    irr::s32 minimalPolysPerNode) {
  return nullptr;
}

irr::scene::IMetaTriangleSelector*
TestSceneManager::createMetaTriangleSelector() {
  return nullptr;
}

irr::scene::ITriangleSelector* TestSceneManager::createTerrainTriangleSelector(
    irr::scene::ITerrainSceneNode* node, irr::s32 LOD) {
  return nullptr;
}

void TestSceneManager::addExternalMeshLoader(
    irr::scene::IMeshLoader* externalLoader) {}

irr::u32 TestSceneManager::getMeshLoaderCount() const { return 0; }

irr::scene::IMeshLoader* TestSceneManager::getMeshLoader(irr::u32 index) const {
  return nullptr;
}

void TestSceneManager::addExternalSceneLoader(
    irr::scene::ISceneLoader* externalLoader) {}

irr::u32 TestSceneManager::getSceneLoaderCount() const { return 0; }

irr::scene::ISceneLoader* TestSceneManager::getSceneLoader(
    irr::u32 index) const {
  return nullptr;
}

irr::scene::ISceneCollisionManager*
TestSceneManager::getSceneCollisionManager() {
  return nullptr;
}

irr::scene::IMeshManipulator* TestSceneManager::getMeshManipulator() {
  return nullptr;
}

void TestSceneManager::addToDeletionQueue(irr::scene::ISceneNode* node) {}

bool TestSceneManager::postEventFromUser(const irr::SEvent& event) {
  return false;
}

void TestSceneManager::clear() {}

irr::io::IAttributes* TestSceneManager::getParameters() { return nullptr; }

irr::scene::E_SCENE_NODE_RENDER_PASS TestSceneManager::getSceneNodeRenderPass()
    const {
  return irr::scene::ESNRP_NONE;
}

irr::scene::ISceneNodeFactory* TestSceneManager::getDefaultSceneNodeFactory() {
  return nullptr;
}

void TestSceneManager::registerSceneNodeFactory(
    irr::scene::ISceneNodeFactory* factoryToAdd) {}

irr::u32 TestSceneManager::getRegisteredSceneNodeFactoryCount() const {
  return 0;
}

irr::scene::ISceneNodeFactory* TestSceneManager::getSceneNodeFactory(
    irr::u32 index) {
  return nullptr;
}

irr::scene::ISceneNodeAnimatorFactory*
TestSceneManager::getDefaultSceneNodeAnimatorFactory() {
  return nullptr;
}

void TestSceneManager::registerSceneNodeAnimatorFactory(
    irr::scene::ISceneNodeAnimatorFactory* factoryToAdd) {}

irr::u32 TestSceneManager::getRegisteredSceneNodeAnimatorFactoryCount() const {
  return 0;
}

irr::scene::ISceneNodeAnimatorFactory*
TestSceneManager::getSceneNodeAnimatorFactory(irr::u32 index) {
  return nullptr;
}

const irr::c8* TestSceneManager::getSceneNodeTypeName(
    irr::scene::ESCENE_NODE_TYPE type) {
  return nullptr;
}

const irr::c8* TestSceneManager::getAnimatorTypeName(
    irr::scene::ESCENE_NODE_ANIMATOR_TYPE type) {
  return nullptr;
}

irr::scene::ISceneNode* TestSceneManager::addSceneNode(
    const char* sceneNodeTypeName, irr::scene::ISceneNode* parent) {
  return nullptr;
}

irr::scene::ISceneNodeAnimator* TestSceneManager::createSceneNodeAnimator(
    const char* typeName, irr::scene::ISceneNode* target) {
  return nullptr;
}

irr::scene::ISceneManager* TestSceneManager::createNewSceneManager(
    bool cloneContent) {
  return nullptr;
}

bool TestSceneManager::saveScene(
    const irr::io::path& filename,
    irr::scene::ISceneUserDataSerializer* userDataSerializer,
    irr::scene::ISceneNode* node) {
  return false;
}

bool TestSceneManager::saveScene(
    irr::io::IWriteFile* file,
    irr::scene::ISceneUserDataSerializer* userDataSerializer,
    irr::scene::ISceneNode* node) {
  return false;
}

bool TestSceneManager::saveScene(
    irr::io::IXMLWriter* writer, const irr::io::path& currentPath,
    irr::scene::ISceneUserDataSerializer* userDataSerializer,
    irr::scene::ISceneNode* node) {
  return false;
}

bool TestSceneManager::loadScene(
    const irr::io::path& filename,
    irr::scene::ISceneUserDataSerializer* userDataSerializer,
    irr::scene::ISceneNode* rootNode) {
  return false;
}

bool TestSceneManager::loadScene(
    irr::io::IReadFile* file,
    irr::scene::ISceneUserDataSerializer* userDataSerializer,
    irr::scene::ISceneNode* rootNode) {
  return false;
}

irr::scene::IMeshWriter* TestSceneManager::createMeshWriter(
    irr::scene::EMESH_WRITER_TYPE type) {
  return nullptr;
}

irr::scene::ISkinnedMesh* TestSceneManager::createSkinnedMesh() {
  return nullptr;
}

void TestSceneManager::setAmbientLight(
    const irr::video::SColorf& ambientColor) {}

const irr::video::SColorf& TestSceneManager::getAmbientLight() const {
  return ambientLight_;
}

void TestSceneManager::setLightManager(
    irr::scene::ILightManager* lightManager) {}

const irr::scene::IGeometryCreator* TestSceneManager::getGeometryCreator()
    const {
  return nullptr;
}

bool TestSceneManager::isCulled(const irr::scene::ISceneNode* node) const {
  return nullptr;
}

// PRIVATE

irr::scene::IAnimatedMeshSceneNode*
TestSceneManager::createTestAnimatedMeshSceneNode(
    irr::scene::IAnimatedMesh*, irr::scene::ISceneNode*, irr::s32, const irr::core::vector3df&,
    const irr::core::vector3df&, const irr::core::vector3df&, bool) {
  return createTestSceneNode<TestAnimatedMeshSceneNode>();
}

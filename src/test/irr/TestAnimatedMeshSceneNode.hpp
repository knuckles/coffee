#ifndef TESTANIMATEDMESHSCENENODE_HPP
#define TESTANIMATEDMESHSCENENODE_HPP

#include "3rdparty/irrlicht/include/IAnimatedMeshSceneNode.h"
#include "gmock/gmock.h"

class TestAnimatedMeshSceneNode : public irr::scene::IAnimatedMeshSceneNode {
 public:
  TestAnimatedMeshSceneNode();

  void setFrameNr(irr::f32 n);

  // ISceneNode interface
 public:
  irr::scene::ESCENE_NODE_TYPE getType() const override;
  void render() override;
  const irr::core::aabbox3d<irr::f32>& getBoundingBox() const;
  irr::scene::ISceneNode* clone(irr::scene::ISceneNode* newParent,
                                irr::scene::ISceneManager* newManager) override;

  // IAnimatedMeshSceneNode interface
 public:
  void setCurrentFrame(irr::f32 frame) override;
  bool setFrameLoop(irr::s32 start, irr::s32 end) override;
  void setAnimationSpeed(irr::f32 framesPerSecond) override;
  irr::f32 getAnimationSpeed() const;
  irr::scene::IShadowVolumeSceneNode* addShadowVolumeSceneNode(
      const irr::scene::IMesh* shadowMesh, irr::s32 id, bool zfailmethod,
      irr::f32 infinity) override;
  irr::scene::IBoneSceneNode* getJointNode(const irr::c8* jointName) override;
  irr::scene::IBoneSceneNode* getJointNode(irr::u32 jointID) override;
  irr::u32 getJointCount() const;
  bool setMD2Animation(irr::scene::EMD2_ANIMATION_TYPE anim) override;
  bool setMD2Animation(const irr::c8* animationName) override;
  irr::f32 getFrameNr() const;
  irr::s32 getStartFrame() const;
  irr::s32 getEndFrame() const;
  void setLoopMode(bool loop) override;
  bool getLoopMode() const;
  void setAnimationEndCallback(irr::scene::IAnimationEndCallBack* callback) override;
  void setReadOnlyMaterials(bool readonly) override;
  bool isReadOnlyMaterials() const;
  void setMesh(irr::scene::IAnimatedMesh* mesh) override;
  irr::scene::IAnimatedMesh* getMesh() override;
  const irr::scene::SMD3QuaternionTag* getMD3TagTransformation(
      const irr::core::stringc& tagname) override;
  void setJointMode(irr::scene::E_JOINT_UPDATE_ON_RENDER mode) override;
  void setTransitionTime(irr::f32 Time) override;
  void animateJoints(bool CalculateAbsolutePositions) override;
  void setRenderFromIdentity(bool On) override;

private:
  irr::f32 frame_;
  irr::s32 frame1_, frame2_;
  bool loop_;
  irr::core::aabbox3d<irr::f32> bbox_;
  irr::scene::IAnimationEndCallBack* animCallback_;
};

#endif  // TESTANIMATEDMESHSCENENODE_HPP

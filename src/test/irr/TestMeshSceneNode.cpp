#include "TestMeshSceneNode.hpp"

TestMeshSceneNode::TestMeshSceneNode()
  :irr::scene::IMeshSceneNode(nullptr, nullptr, -1) {
}

void TestMeshSceneNode::render() {}

const irr::core::aabbox3d<irr::f32>& TestMeshSceneNode::getBoundingBox() const {
  return bbox_;
}

void TestMeshSceneNode::setMesh(irr::scene::IMesh* mesh) {}

irr::scene::IMesh* TestMeshSceneNode::getMesh() { return nullptr; }

irr::scene::IShadowVolumeSceneNode* TestMeshSceneNode::addShadowVolumeSceneNode(
    const irr::scene::IMesh* shadowMesh, irr::s32 id, bool zfailmethod,
    irr::f32 infinity) {
  return nullptr;
}

void TestMeshSceneNode::setReadOnlyMaterials(bool readonly) {}

bool TestMeshSceneNode::isReadOnlyMaterials() const { return false; }

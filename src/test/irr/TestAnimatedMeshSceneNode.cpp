#include "TestAnimatedMeshSceneNode.hpp"

TestAnimatedMeshSceneNode::TestAnimatedMeshSceneNode()
  : irr::scene::IAnimatedMeshSceneNode(nullptr, nullptr, -1),
    frame_(),
    frame1_(),
    frame2_(),
    loop_() {
}

void TestAnimatedMeshSceneNode::setFrameNr(irr::f32 n) {
  frame_ = n;
  if (!loop_ && fabs(frame_ - frame2_) <= 0.0001 && animCallback_) {
    animCallback_->OnAnimationEnd(this);
  }
}

irr::scene::ESCENE_NODE_TYPE TestAnimatedMeshSceneNode::getType() const {
  return irr::scene::ESNT_ANIMATED_MESH;
}

void TestAnimatedMeshSceneNode::render() {}

const irr::core::aabbox3d<irr::f32>& TestAnimatedMeshSceneNode::getBoundingBox() const {
  return bbox_;
}

irr::scene::ISceneNode* TestAnimatedMeshSceneNode::clone(
    irr::scene::ISceneNode* newParent, irr::scene::ISceneManager* newManager) {
  return nullptr;
}

void TestAnimatedMeshSceneNode::setCurrentFrame(irr::f32 frame) {}

bool TestAnimatedMeshSceneNode::setFrameLoop(irr::s32 start, irr::s32 end) {
  frame1_ = start;
  frame2_ = end;
  return true;
}

void TestAnimatedMeshSceneNode::setAnimationSpeed(irr::f32 framesPerSecond) {}

irr::f32 TestAnimatedMeshSceneNode::getAnimationSpeed() const { return 0; }

irr::scene::IShadowVolumeSceneNode*
TestAnimatedMeshSceneNode::addShadowVolumeSceneNode(
    const irr::scene::IMesh* shadowMesh, irr::s32 id, bool zfailmethod,
    irr::f32 infinity) {
  return nullptr;
}

irr::scene::IBoneSceneNode* TestAnimatedMeshSceneNode::getJointNode(const irr::c8* jointName) {
  return nullptr;
}

irr::scene::IBoneSceneNode* TestAnimatedMeshSceneNode::getJointNode(irr::u32 jointID) {
  return nullptr;
}

irr::u32 TestAnimatedMeshSceneNode::getJointCount() const { return 0; }

bool TestAnimatedMeshSceneNode::setMD2Animation(irr::scene::EMD2_ANIMATION_TYPE anim) {
  return false;
}

bool TestAnimatedMeshSceneNode::setMD2Animation(const irr::c8* animationName) { return false; }

irr::f32 TestAnimatedMeshSceneNode::getFrameNr() const { return 0; }

irr::s32 TestAnimatedMeshSceneNode::getStartFrame() const { return frame1_; }

irr::s32 TestAnimatedMeshSceneNode::getEndFrame() const { return frame2_; }

void TestAnimatedMeshSceneNode::setLoopMode(bool loop) {
  loop_ = loop;
}

bool TestAnimatedMeshSceneNode::getLoopMode() const { return loop_; }

void TestAnimatedMeshSceneNode::setAnimationEndCallback(
    irr::scene::IAnimationEndCallBack* callback) {
  animCallback_ = callback;
}

void TestAnimatedMeshSceneNode::setReadOnlyMaterials(bool readonly) {}

bool TestAnimatedMeshSceneNode::isReadOnlyMaterials() const { return false; }

void TestAnimatedMeshSceneNode::setMesh(irr::scene::IAnimatedMesh* mesh) {}

irr::scene::IAnimatedMesh* TestAnimatedMeshSceneNode::getMesh() {
  return nullptr;
}

const irr::scene::SMD3QuaternionTag*
TestAnimatedMeshSceneNode::getMD3TagTransformation(const irr::core::stringc& tagname) {
  return nullptr;
}

void TestAnimatedMeshSceneNode::setJointMode(irr::scene::E_JOINT_UPDATE_ON_RENDER mode) {}

void TestAnimatedMeshSceneNode::setTransitionTime(irr::f32 Time) {}

void TestAnimatedMeshSceneNode::animateJoints(bool CalculateAbsolutePositions) {
}

void TestAnimatedMeshSceneNode::setRenderFromIdentity(bool On) {}

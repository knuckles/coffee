#ifndef TESTMESHSCENENODE_HPP
#define TESTMESHSCENENODE_HPP

#include "3rdparty/irrlicht/include/IMeshSceneNode.h"

class TestMeshSceneNode : public irr::scene::IMeshSceneNode {
 public:
  TestMeshSceneNode();

  // ISceneNode interface
  void render() override;
  const irr::core::aabbox3d<irr::f32>& getBoundingBox() const;

  // IMeshSceneNode interface
  void setMesh(irr::scene::IMesh* mesh) override;
  irr::scene::IMesh* getMesh() override;
  irr::scene::IShadowVolumeSceneNode* addShadowVolumeSceneNode(
      const irr::scene::IMesh* shadowMesh, irr::s32 id, bool zfailmethod,
      irr::f32 infinity) override;
  void setReadOnlyMaterials(bool readonly) override;
  bool isReadOnlyMaterials() const override;

private:
  irr::core::aabbox3d<irr::f32> bbox_;
};

#endif  // TESTMESHSCENENODE_HPP

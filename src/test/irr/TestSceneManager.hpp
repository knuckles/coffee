#ifndef TESTSCENEMANAGER_HPP
#define TESTSCENEMANAGER_HPP

#include <vector>
#include "base/irrlicht_ptrs.hpp"
#include "3rdparty/irrlicht/include/ISceneManager.h"
#include "gmock/gmock.h"

class TestSceneManager : public irr::scene::ISceneManager {
 public:
  TestSceneManager();
  ~TestSceneManager();

  // ISceneManager interface
  irr::scene::IAnimatedMesh* getMesh(const irr::io::path& filename) override;
  irr::scene::IAnimatedMesh* getMesh(irr::io::IReadFile* file) override;
  irr::scene::IMeshCache* getMeshCache() override;
  irr::video::IVideoDriver* getVideoDriver() override;
  irr::gui::IGUIEnvironment* getGUIEnvironment() override;
  irr::io::IFileSystem* getFileSystem() override;
  irr::scene::IVolumeLightSceneNode* addVolumeLightSceneNode(
      irr::scene::ISceneNode* parent = 0, irr::s32 id = -1,
      const irr::u32 subdivU = 32, const irr::u32 subdivV = 32,
      const irr::video::SColor foot = irr::video::SColor(51, 0, 230, 180),
      const irr::video::SColor tail = irr::video::SColor(0, 0, 0, 0),
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& rotation = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f,
                                                               1.0f)) override;
  irr::scene::IMeshSceneNode* addCubeSceneNode(
      irr::f32 size = 10.0f, irr::scene::ISceneNode* parent = 0,
      irr::s32 id = -1,
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& rotation = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f,
                                                               1.0f)) override;
  irr::scene::IMeshSceneNode* addSphereSceneNode(
      irr::f32 radius = 5.0f, irr::s32 polyCount = 16,
      irr::scene::ISceneNode* parent = 0, irr::s32 id = -1,
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& rotation = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f,
                                                               1.0f)) override;
  MOCK_METHOD7(addAnimatedMeshSceneNode,
      irr::scene::IAnimatedMeshSceneNode*(
        irr::scene::IAnimatedMesh*,
        irr::scene::ISceneNode*,
        irr::s32,
        const irr::core::vector3df&,
        const irr::core::vector3df&,
        const irr::core::vector3df&,
        bool alsoAddIfMeshPointerZero));
  irr::scene::IMeshSceneNode* addMeshSceneNode(
      irr::scene::IMesh* mesh, irr::scene::ISceneNode* parent = 0,
      irr::s32 id = -1,
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& rotation = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f,
                                                               1.0f),
      bool alsoAddIfMeshPointerZero = false) override;
  irr::scene::ISceneNode* addWaterSurfaceSceneNode(
      irr::scene::IMesh* mesh, irr::f32 waveHeight = 2.0f,
      irr::f32 waveSpeed = 300.0f, irr::f32 waveLength = 10.0f,
      irr::scene::ISceneNode* parent = 0, irr::s32 id = -1,
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& rotation = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f,
                                                               1.0f)) override;
  irr::scene::IMeshSceneNode* addOctreeSceneNode(
      irr::scene::IAnimatedMesh* mesh, irr::scene::ISceneNode* parent = 0,
      irr::s32 id = -1, irr::s32 minimalPolysPerNode = 512,
      bool alsoAddIfMeshPointerZero = false) override;
  irr::scene::IMeshSceneNode* addOctreeSceneNode(
      irr::scene::IMesh* mesh, irr::scene::ISceneNode* parent = 0,
      irr::s32 id = -1, irr::s32 minimalPolysPerNode = 256,
      bool alsoAddIfMeshPointerZero = false) override;
  irr::scene::ICameraSceneNode* addCameraSceneNode(
      irr::scene::ISceneNode* parent = 0,
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& lookat = irr::core::vector3df(0, 0, 100),
      irr::s32 id = -1, bool makeActive = true) override;
  irr::scene::ICameraSceneNode* addCameraSceneNodeMaya(
      irr::scene::ISceneNode* parent = 0, irr::f32 rotateSpeed = -1500.f,
      irr::f32 zoomSpeed = 200.f, irr::f32 translationSpeed = 1500.f,
      irr::s32 id = -1, irr::f32 distance = 70.f,
      bool makeActive = true) override;
  irr::scene::ICameraSceneNode* addCameraSceneNodeFPS(
      irr::scene::ISceneNode* parent = 0, irr::f32 rotateSpeed = 100.0f,
      irr::f32 moveSpeed = 0.5f, irr::s32 id = -1,
      irr::SKeyMap* keyMapArray = 0, irr::s32 keyMapSize = 0,
      bool noVerticalMovement = false, irr::f32 jumpSpeed = 0.f,
      bool invertMouse = false, bool makeActive = true) override;
  irr::scene::ILightSceneNode* addLightSceneNode(
      irr::scene::ISceneNode* parent = 0,
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      irr::video::SColorf color = irr::video::SColorf(1.0f, 1.0f, 1.0f),
      irr::f32 radius = 100.0f, irr::s32 id = -1) override;
  irr::scene::IBillboardSceneNode* addBillboardSceneNode(
      irr::scene::ISceneNode* parent = 0,
      const irr::core::dimension2d<irr::f32>& size =
          irr::core::dimension2d<irr::f32>(10.0f, 10.0f),
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      irr::s32 id = -1, irr::video::SColor colorTop = 0xFFFFFFFF,
      irr::video::SColor colorBottom = 0xFFFFFFFF) override;
  irr::scene::ISceneNode* addSkyBoxSceneNode(
      irr::video::ITexture* top, irr::video::ITexture* bottom,
      irr::video::ITexture* left, irr::video::ITexture* right,
      irr::video::ITexture* front, irr::video::ITexture* back,
      irr::scene::ISceneNode* parent = 0, irr::s32 id = -1) override;
  irr::scene::ISceneNode* addSkyDomeSceneNode(
      irr::video::ITexture* texture, irr::u32 horiRes = 16,
      irr::u32 vertRes = 8, irr::f32 texturePercentage = 0.9,
      irr::f32 spherePercentage = 2.0, irr::f32 radius = 1000.f,
      irr::scene::ISceneNode* parent = 0, irr::s32 id = -1) override;
  irr::scene::IParticleSystemSceneNode* addParticleSystemSceneNode(
      bool withDefaultEmitter = true, irr::scene::ISceneNode* parent = 0,
      irr::s32 id = -1,
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& rotation = irr::core::vector3df(0, 0, 0),
      const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f,
                                                               1.0f)) override;
  irr::scene::ITerrainSceneNode* addTerrainSceneNode(
      const irr::io::path& heightMapFileName,
      irr::scene::ISceneNode* parent = 0, irr::s32 id = -1,
      const irr::core::vector3df& position = irr::core::vector3df(0.0f, 0.0f,
                                                                  0.0f),
      const irr::core::vector3df& rotation = irr::core::vector3df(0.0f, 0.0f,
                                                                  0.0f),
      const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f,
                                                               1.0f),
      irr::video::SColor vertexColor = irr::video::SColor(255, 255, 255, 255),
      irr::s32 maxLOD = 5,
      irr::scene::E_TERRAIN_PATCH_SIZE patchSize = irr::scene::ETPS_17,
      irr::s32 smoothFactor = 0, bool addAlsoIfHeightmapEmpty = false) override;
  irr::scene::ITerrainSceneNode* addTerrainSceneNode(
      irr::io::IReadFile* heightMapFile, irr::scene::ISceneNode* parent = 0,
      irr::s32 id = -1,
      const irr::core::vector3df& position = irr::core::vector3df(0.0f, 0.0f,
                                                                  0.0f),
      const irr::core::vector3df& rotation = irr::core::vector3df(0.0f, 0.0f,
                                                                  0.0f),
      const irr::core::vector3df& scale = irr::core::vector3df(1.0f, 1.0f,
                                                               1.0f),
      irr::video::SColor vertexColor = irr::video::SColor(255, 255, 255, 255),
      irr::s32 maxLOD = 5,
      irr::scene::E_TERRAIN_PATCH_SIZE patchSize = irr::scene::ETPS_17,
      irr::s32 smoothFactor = 0, bool addAlsoIfHeightmapEmpty = false) override;
  irr::scene::IMeshSceneNode* addQuake3SceneNode(
      const irr::scene::IMeshBuffer* meshBuffer,
      const irr::scene::quake3::IShader* shader,
      irr::scene::ISceneNode* parent = 0, irr::s32 id = -1) override;
  irr::scene::ISceneNode* addEmptySceneNode(irr::scene::ISceneNode* parent = 0,
                                            irr::s32 id = -1) override;
  irr::scene::IDummyTransformationSceneNode* addDummyTransformationSceneNode(
      irr::scene::ISceneNode* parent = 0, irr::s32 id = -1) override;
  irr::scene::ITextSceneNode* addTextSceneNode(
      irr::gui::IGUIFont* font, const wchar_t* text,
      irr::video::SColor color = irr::video::SColor(100, 255, 255, 255),
      irr::scene::ISceneNode* parent = 0,
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      irr::s32 id = -1) override;
  irr::scene::IBillboardTextSceneNode* addBillboardTextSceneNode(
      irr::gui::IGUIFont* font, const wchar_t* text,
      irr::scene::ISceneNode* parent = 0,
      const irr::core::dimension2d<irr::f32>& size =
          irr::core::dimension2d<irr::f32>(10.0f, 10.0f),
      const irr::core::vector3df& position = irr::core::vector3df(0, 0, 0),
      irr::s32 id = -1, irr::video::SColor colorTop = 0xFFFFFFFF,
      irr::video::SColor colorBottom = 0xFFFFFFFF) override;
  irr::scene::IAnimatedMesh* addHillPlaneMesh(
      const irr::io::path& name,
      const irr::core::dimension2d<irr::f32>& tileSize,
      const irr::core::dimension2d<irr::u32>& tileCount,
      irr::video::SMaterial* material = 0, irr::f32 hillHeight = 0.0f,
      const irr::core::dimension2d<irr::f32>& countHills =
          irr::core::dimension2d<irr::f32>(0.0f, 0.0f),
      const irr::core::dimension2d<irr::f32>& textureRepeatCount =
          irr::core::dimension2d<irr::f32>(1.0f, 1.0f)) override;
  irr::scene::IAnimatedMesh* addTerrainMesh(
      const irr::io::path& meshname, irr::video::IImage* texture,
      irr::video::IImage* heightmap,
      const irr::core::dimension2d<irr::f32>& stretchSize =
          irr::core::dimension2d<irr::f32>(10.0f, 10.0f),
      irr::f32 maxHeight = 200.0f,
      const irr::core::dimension2d<irr::u32>& defaultVertexBlockSize =
          irr::core::dimension2d<irr::u32>(64, 64)) override;
  irr::scene::IAnimatedMesh* addArrowMesh(
      const irr::io::path& name,
      irr::video::SColor vtxColorCylinder = 0xFFFFFFFF,
      irr::video::SColor vtxColorCone = 0xFFFFFFFF,
      irr::u32 tesselationCylinder = 4, irr::u32 tesselationCone = 8,
      irr::f32 height = 1.f, irr::f32 cylinderHeight = 0.6f,
      irr::f32 widthCylinder = 0.05f, irr::f32 widthCone = 0.3f) override;
  irr::scene::IAnimatedMesh* addSphereMesh(const irr::io::path& name,
                                           irr::f32 radius = 5.f,
                                           irr::u32 polyCountX = 16,
                                           irr::u32 polyCountY = 16) override;
  irr::scene::IAnimatedMesh* addVolumeLightMesh(
      const irr::io::path& name, const irr::u32 SubdivideU = 32,
      const irr::u32 SubdivideV = 32,
      const irr::video::SColor FootColor = irr::video::SColor(51, 0, 230, 180),
      const irr::video::SColor TailColor = irr::video::SColor(0, 0, 0,
                                                              0)) override;
  irr::scene::ISceneNode* getRootSceneNode() override;
  irr::scene::ISceneNode* getSceneNodeFromId(
      irr::s32 id, irr::scene::ISceneNode* start = 0) override;
  irr::scene::ISceneNode* getSceneNodeFromName(
      const irr::c8* name, irr::scene::ISceneNode* start = 0) override;
  irr::scene::ISceneNode* getSceneNodeFromType(
      irr::scene::ESCENE_NODE_TYPE type,
      irr::scene::ISceneNode* start = 0) override;
  void getSceneNodesFromType(
      irr::scene::ESCENE_NODE_TYPE type,
      irr::core::array<irr::scene::ISceneNode*>& outNodes,
      irr::scene::ISceneNode* start = 0) override;
  irr::scene::ICameraSceneNode* getActiveCamera() const override;
  void setActiveCamera(irr::scene::ICameraSceneNode* camera) override;
  void setShadowColor(
      irr::video::SColor color = irr::video::SColor(150, 0, 0, 0)) override;
  irr::video::SColor getShadowColor() const override;
  irr::u32 registerNodeForRendering(irr::scene::ISceneNode* node,
                                    irr::scene::E_SCENE_NODE_RENDER_PASS pass =
                                        irr::scene::ESNRP_AUTOMATIC) override;
  void drawAll() override;
  irr::scene::ISceneNodeAnimator* createRotationAnimator(
      const irr::core::vector3df& rotationSpeed) override;
  irr::scene::ISceneNodeAnimator* createFlyCircleAnimator(
      const irr::core::vector3df& center = irr::core::vector3df(0.f, 0.f, 0.f),
      irr::f32 radius = 100.f, irr::f32 speed = 0.001f,
      const irr::core::vector3df& direction = irr::core::vector3df(0.f, 1.f,
                                                                   0.f),
      irr::f32 startPosition = 0.f, irr::f32 radiusEllipsoid = 0.f) override;
  irr::scene::ISceneNodeAnimator* createFlyStraightAnimator(
      const irr::core::vector3df& startPoint,
      const irr::core::vector3df& endPoint, irr::u32 timeForWay,
      bool loop = false, bool pingpong = false) override;
  irr::scene::ISceneNodeAnimator* createTextureAnimator(
      const irr::core::array<irr::video::ITexture*>& textures,
      irr::s32 timePerFrame, bool loop = true) override;
  irr::scene::ISceneNodeAnimator* createDeleteAnimator(
      irr::u32 timeMs) override;
  irr::scene::ISceneNodeAnimatorCollisionResponse*
  createCollisionResponseAnimator(
      irr::scene::ITriangleSelector* world, irr::scene::ISceneNode* sceneNode,
      const irr::core::vector3df& ellipsoidRadius = irr::core::vector3df(30, 60,
                                                                         30),
      const irr::core::vector3df& gravityPerSecond =
          irr::core::vector3df(0, -10.0f, 0),
      const irr::core::vector3df& ellipsoidTranslation =
          irr::core::vector3df(0, 0, 0),
      irr::f32 slidingValue = 0.0005f) override;
  irr::scene::ISceneNodeAnimator* createFollowSplineAnimator(
      const irr::core::array<irr::core::vector3df>& points,
      irr::f32 speed = 1.0f, irr::f32 tightness = 0.5f, bool loop = true,
      bool pingpong = false) override;
  irr::scene::ITriangleSelector* createTriangleSelector(
      irr::scene::IMesh* mesh, irr::scene::ISceneNode* node) override;
  irr::scene::ITriangleSelector* createTriangleSelector(
      irr::scene::IAnimatedMeshSceneNode* node) override;
  irr::scene::ITriangleSelector* createTriangleSelectorFromBoundingBox(
      irr::scene::ISceneNode* node) override;
  irr::scene::ITriangleSelector* createOctreeTriangleSelector(
      irr::scene::IMesh* mesh, irr::scene::ISceneNode* node,
      irr::s32 minimalPolysPerNode = 32) override;
  irr::scene::IMetaTriangleSelector* createMetaTriangleSelector() override;
  irr::scene::ITriangleSelector* createTerrainTriangleSelector(
      irr::scene::ITerrainSceneNode* node, irr::s32 LOD = 0) override;
  void addExternalMeshLoader(irr::scene::IMeshLoader* externalLoader) override;
  irr::u32 getMeshLoaderCount() const override;
  irr::scene::IMeshLoader* getMeshLoader(irr::u32 index) const override;
  void addExternalSceneLoader(
      irr::scene::ISceneLoader* externalLoader) override;

  irr::u32 getSceneLoaderCount() const override;
  irr::scene::ISceneLoader* getSceneLoader(irr::u32 index) const override;
  irr::scene::ISceneCollisionManager* getSceneCollisionManager() override;
  irr::scene::IMeshManipulator* getMeshManipulator() override;

  void addToDeletionQueue(irr::scene::ISceneNode* node) override;

  bool postEventFromUser(const irr::SEvent& event) override;

  void clear() override;

  irr::io::IAttributes* getParameters() override;
  irr::scene::E_SCENE_NODE_RENDER_PASS getSceneNodeRenderPass() const override;

  irr::scene::ISceneNodeFactory* getDefaultSceneNodeFactory() override;
  void registerSceneNodeFactory(
      irr::scene::ISceneNodeFactory* factoryToAdd) override;
  irr::u32 getRegisteredSceneNodeFactoryCount() const override;
  irr::scene::ISceneNodeFactory* getSceneNodeFactory(irr::u32 index) override;
  irr::scene::ISceneNodeAnimatorFactory* getDefaultSceneNodeAnimatorFactory()
      override;
  void registerSceneNodeAnimatorFactory(
      irr::scene::ISceneNodeAnimatorFactory* factoryToAdd) override;
  irr::u32 getRegisteredSceneNodeAnimatorFactoryCount() const override;

  irr::scene::ISceneNodeAnimatorFactory* getSceneNodeAnimatorFactory(
      irr::u32 index) override;
  const irr::c8* getSceneNodeTypeName(
      irr::scene::ESCENE_NODE_TYPE type) override;
  const irr::c8* getAnimatorTypeName(
      irr::scene::ESCENE_NODE_ANIMATOR_TYPE type) override;

  irr::scene::ISceneNode* addSceneNode(
      const char* sceneNodeTypeName,
      irr::scene::ISceneNode* parent = 0) override;

  irr::scene::ISceneNodeAnimator* createSceneNodeAnimator(
      const char* typeName, irr::scene::ISceneNode* target = 0) override;
  ISceneManager* createNewSceneManager(bool cloneContent = false) override;

  bool saveScene(const irr::io::path& filename,
                 irr::scene::ISceneUserDataSerializer* userDataSerializer = 0,
                 irr::scene::ISceneNode* node = 0) override;
  bool saveScene(irr::io::IWriteFile* file,
                 irr::scene::ISceneUserDataSerializer* userDataSerializer = 0,
                 irr::scene::ISceneNode* node = 0) override;
  bool saveScene(irr::io::IXMLWriter* writer, const irr::io::path& currentPath,
                 irr::scene::ISceneUserDataSerializer* userDataSerializer = 0,
                 irr::scene::ISceneNode* node = 0) override;

  bool loadScene(const irr::io::path& filename,
                 irr::scene::ISceneUserDataSerializer* userDataSerializer = 0,
                 irr::scene::ISceneNode* rootNode = 0) override;
  bool loadScene(irr::io::IReadFile* file,
                 irr::scene::ISceneUserDataSerializer* userDataSerializer = 0,
                 irr::scene::ISceneNode* rootNode = 0) override;

  irr::scene::IMeshWriter* createMeshWriter(
      irr::scene::EMESH_WRITER_TYPE type) override;
  irr::scene::ISkinnedMesh* createSkinnedMesh() override;
  void setAmbientLight(const irr::video::SColorf& ambientColor) override;
  const irr::video::SColorf& getAmbientLight() const override;
  void setLightManager(irr::scene::ILightManager* lightManager) override;
  const irr::scene::IGeometryCreator* getGeometryCreator(void) const override;
  bool isCulled(const irr::scene::ISceneNode* node) const override;

protected:
  template<typename T>
  T* createTestSceneNode() {
    boost::intrusive_ptr<T> node(new T());
    nodes_.push_back(node);
    return node.get();
  }

  irr::scene::IAnimatedMeshSceneNode* createTestAnimatedMeshSceneNode(
      irr::scene::IAnimatedMesh*, irr::scene::ISceneNode*, irr::s32, const irr::core::vector3df&,
      const irr::core::vector3df&, const irr::core::vector3df&, bool);

private:
  irr::video::SColorf ambientLight_;
  std::vector<boost::intrusive_ptr<irr::scene::ISceneNode>> nodes_;
};

#endif  // TESTSCENEMANAGER_HPP

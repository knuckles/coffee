#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

replacements = {
    "lCollar": "collar.L",
    "rCollar": "collar.R",
    "lShldr": "shldr.L",
    "rShldr": "shldr.R",
    "lForeArm": "forearm.L",
    "rForeArm": "forearm.R",
    "lHand": "hand.L",
    "rHand": "hand.R",
    "lThigh": "thigh.L",
    "rThigh": "thigh.R",
    "lShin": "shin.L",
    "rShin": "shin.R",
    "lFoot": "foot.L",
    "rFoot": "foot.R"
}

def processFile(bvhFileName):
    prefix = "JOINT "
    with open(bvhFileName, "r+") as f:
        text = f.read()
        for find, replace in replacements.iteritems():
            text = text.replace(prefix + find, prefix + replace)
        f.seek(0)
        f.write(text)
        f.truncate()

def main():
    for bvhFileName in sys.argv[1:]:
        processFile(bvhFileName)

if __name__ == "__main__":
    main()
